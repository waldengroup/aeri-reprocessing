# -*- coding: utf-8 -*-
"""
This is a general class definition for AERI instruments used for research.

There are two main classes in this module:
    REPROCESS
        for reprocessing data taken by the Atmospheric Emitted Radiance Interferometer (AERI). 
    AERI
        general routines for reading in various AERI data and parameter files.

Created on Wed Aug 22 22:33:40 2012

Updated on Fri Mar 11 12:34:00 2016

@author: Von P. Walden, Washington State University
"""

class REPROCESS:
    """This class defines attributes and methods for reprocessing data taken
        by the Atmospheric Emitted Radiance Interferometer (AERI).
        
        Example usage #1:
            
            # Retrieve a month's worth of data from the NOAA ICECAPS archive.
            import aeri
            paeri = aeri.REPROCESS('130901','130930')
            paeri.retrieveDataFromNOAA()

        Example usage #2:
        
            1) First check that the proper parameter files are in the prms 
               directory !!
               
            2) Reprocess the AERI data, then perform QC and
                recalibrate a single day of data.
            
            import aeri
            paeri = aeri.REPROCESS('130901','130901')
            # Runs AERI reprocessing, plus paeri.qc and paeri.rm.
            paeri.reprocess()   # Check status using "qstat" command on aeolus.
            paeri.qcCheck()     # Creates plots to check BEFORE PROCEEDING WITH PC.
            
            3) After the data are re-calibrated and QC'd, perform PC filtering and 
            zero-filling (which requires many days).
            
            paeri = aeri.REPROCESS('130901','130930')
            paeri.pc()          # Check status using "qstat" command on aeolus.
            paeri.zfli()
        or 
            paeri = aeri.REPROCESS('130901','130930')
            paeri.zfliHPC()     # Performs both PC and zfli using cluster.
                                # Check status using "qstat" command on aeolus.

            4) Final check; creates plots to look at to double-check processing.
            
            paeri.finalCheck()

        Example usage #3:
            1) To correct data that have bad spectra:
            
            import aeri
            paeri = aeri.REPROCESS('141203','141203')
            paeri.qcCorrectBadSpectra(range(2888,2896))  # Last element is NOT included.
    """
    def __init__(self, beginningDate, endingDate):
        """Initializes the AERI reprocessing object with the necessary variables
                    and directory names.
                    
                    Written by Von P. Walden
                                24 Sep 2014
        """
        import sys
        from socket import gethostname
        from datetime import datetime, timedelta
        
        # Wavenumber cutoffs ...
        self.nuloCh1            = 492.
        self.nuhiCh1            = 1800.
        self.nuloCh2            = 1780.
        self.nuhiCh2            = 3000.
        # Laser nu ... Penny Rowe's values from 5 May 2010.
        self.LASERwavenumberCh1 = 15799.31
        self.LASERwavenumberCh2 = 15799.39
        # cavfac. Set to nu-indie 39 (was 12.79) as recommended by D. Turner ... 
        self.cavfactor          = 39.
        # FFOV 1/2 angle ...
        self.FFOVhalfAngle      = 0.0230
        
        # Specify the beginning and ending dates to process.
#        beginningDate = datetime.strptime(sys.argv[1],'%y%m%d')
#       endingDate    = datetime.strptime(sys.argv[2],'%y%m%d')
        self.beginningDate = datetime.strptime(beginningDate,'%y%m%d')
        self.endingDate    = datetime.strptime(endingDate,   '%y%m%d')
        self.delta         = timedelta(days=1)
        if (self.beginningDate > self.endingDate):
            print('Beginning date is greater than the ending date.')
            sys.exit()
        
        # Takes a "snap shot" of the current software archive, which is 
        # included in the netCDF files.
        self.version()

        # Set up the necessary directories.
        self.hostname = gethostname()
        if self.hostname.rfind('aeolus')>=0 or self.hostname.rfind('compute')>=0:
            # For aeolus.wsu.edu and aeolus compute nodes  (Linux)
            self.directory = {'in':    '/data/lar/users/vonw/reprocessed_tmp/',
                              'proc':  '/data/lar/users/vonw/fieldExperiments/summit/data/paeri/reprocessed/',
                              'QC':    '/data/lar/users/vonw/fieldExperiments/summit/data/paeri/QC/',
                              'log':   '/data/lar/users/vonw/fieldExperiments/summit/data/paeri/logs/',
                              'RM':    '/data/lar/users/vonw/fieldExperiments/summit/data/paeri/QC/',
                              'PC':    '/data/lar/users/vonw/fieldExperiments/summit/data/paeri/PC/',
                              'jpgs':  '/data/lar/users/vonw/fieldExperiments/summit/data/paeri/jpgs/',
                              'final': '/data/lar/users/vonw/fieldExperiments/summit/data/paeri/final/',
                              'prms':  '/home/vonw/work/projects/paeri/software/parameter_files/paramsSmt/'}     # THIS MUST BE CHECKED FOR CHANGES TO THE PARAMETER FILES.
        elif self.hostname.rfind('sila')>=0 or self.hostname.rfind('nuia')>=0:
            # For sila.cee.wsu.edu  (iMac)
            self.directory = {'in':    '/Volumes/vonw/reprocessed_tmp/',
                              'proc':  '/Volumes/vonw/fieldExperiments/summit/data/paeri/reprocessed/',
                              'QC':    '/Volumes/vonw/fieldExperiments/summit/data/paeri/QC/',
                              'log':   '/Volumes/vonw/fieldExperiments/summit/data/paeri/logs/',
                              'RM':    '/Volumes/vonw/fieldExperiments/summit/data/paeri/QC/',
                              'PC':    '/Volumes/vonw/fieldExperiments/summit/data/paeri/PC/',
                              'jpgs':  '/Volumes/vonw/fieldExperiments/summit/data/paeri/jpgs/',
                              'final': '/Volumes/vonw/fieldExperiments/summit/data/paeri/final/',
                              'prms':  '/Users/vonw/work/projects/paeri/software/parameter_files/paramsSmt/'}     # THIS MUST BE CHECKED FOR CHANGES TO THE PARAMETER FILES.
        else:
            print('Your local computer is unrecognized by aeri.REPROCESS.  Talk to Von!')
            sys.exit()
        
        return
    
    def version(self):
        """Sets the software version of all the routines used for AERI
            reprocessing.  This output is generated by 'git ls-files -s'
            on aeolus.
                        
                    Written by Von P. Walden
                                14 Dec 2014
                    Updated on  16 Mar 2016
        """
        import os
        from subprocess import check_output

        #cwd = os.getcwd()
        #os.chdir('/home/vonw/work/software/ICECAPS/AERI-reprocessing/')
        #self.version = ('Tag: v1.1\n' + check_output(['git', 'ls-files', '-s']).decode('utf-8'))
        #os.chdir(cwd)
        self.version = ('Tag: v1.2\n'
                        '100644 a1c7208b0ce187b63a3ef1661ebaaf933367cedf 0	.gitignore\n'
                        '100755 cba4d965880e2b0ab323f4c67d7a47ae4047c3f0 0	aeri.py\n'
                        '100644 b94f599f5b9af094dd9b72b3ede9d91cbab941fd 0	octave-workspace\n'
                        '100755 ed8b41d9f74e39424b0ee5d6bd82a3f184f2f603 0	octave/PC/paeriPC.m\n'
                        '100755 4665a353254152ec2d5bed761365b3f881d23e4a 0	octave/PC/paeri_pcfilter.m\n'
                        '100755 9494c4e32da994fd2f61b001c737a644b96dcb54 0	octave/PC/paeri_pcfilter_driver.m\n'
                        '100755 d6454351d37da104309f472f7fcbe5acf61cd613 0	octave/QC/apodizer.m\n'
                        '100755 9eaaa273c1785c53997f22d925fd0d0057bd904d 0	octave/QC/attribute_list.m\n'
                        '100755 80e2ef3085b89f347849f384cb4ab5d1339ef738 0	octave/QC/cdfNewDim.m\n'
                        '100755 6ddb400273c9f4262f90dc34a3cba9d7fdb8a8b0 0	octave/QC/cdfNewDimSum.m\n'
                        '100755 e5e57b245c0439dc0e1eb15c409157bf6939bace 0	octave/QC/paeriQC.m\n'
                        '100755 47d6f0d483e9c8d4a94cb05edb3194e89c94ddfe 0	octave/QC/paeriQC_Flags.m\n'
                        '100755 81e55b4f481b0118f7dcb6ff8d3cf56f9b1e1146 0	octave/RM/AERI_process_amun_short.m\n'
                        '100755 853afe955ebb86e8826faf71c04441f7f6820f13 0	octave/RM/BBemissCorr.m\n'
                        '100755 c40ca6bceeb50f5b3495852114454997f3fd17c1 0	octave/RM/applyRM.m\n'
                        '100755 a31fc55832bc8c7ed034634cdc7567d9eb60a251 0	octave/RM/applyRMch2.m\n'
                        '100755 f4b6d047e6b6fcd8693834f7cdc0117312af0711 0	octave/RM/brightnessT.m\n'
                        '100755 8ada97431b69f27cda5d49341edf616a3c24796f 0	octave/RM/doAericalv.m\n'
                        '100755 b24a49a6a2c320a96d0f6052ef36bb2afef89fc3 0	octave/RM/ffovcmr_v2.m\n'
                        '100755 e781151beb41e312b8f883dc0a5ea3f9ee1096d8 0	octave/RM/ft.m\n'
                        '100755 3931010a2cdd149376157017a1773716dd7a6ba5 0	octave/RM/getWN_ELW.m\n'
                        '100755 0bb5011dd63f3be38c934cb9fbfa2290abcd906d 0	octave/RM/get_aeri_bb_emis.m\n'
                        '100755 08c4b9da4de413a39dfa02c4009e7c15a77c37a0 0	octave/RM/ift.m\n'
                        '100755 89da55d5d20b40c004a98f16af7782a97c8b6d09 0	octave/RM/paeriRM.m\n'
                        '100755 7aacfff44299213f00274884df222bc44db39484 0	octave/RM/plancknu.m\n'
                        '100755 d1096e2c682ed4cfc7524804518b54ccb5618a89 0	octave/dmvtocdf_any.m\n'
                        '100755 0e9a10204fce058323a81d6fdecdbd0d5a4815ac 0	octave/rd_netcdf.m\n'
                        '100755 98b8cc9eaf75b9e050ae7ac95196716a82057aaf 0	octave/zfli/cdfNewSingleDataType.m\n'
                        '100755 7434f4180bcd632c3dd3e4b9d6c42abe8091e039 0	octave/zfli/paeriZfli.m\n'
                        '100755 5e91dfa83ec182d430708dca41272d610f58c92d 0	octave/zfli/zfliDriver_amun1.m\n'
                        '100755 e6ad264fe530b4fcd0dd72f4eb44c3dcf6939add 0	octave/zfli/zfli_mat.m\n')
        return
    
    def setDirectories(self, date):
        """This functions simply sets up directories that depend on the actual
            date.
            
                    Written by Von P. Walden
                                24 Sep 2014
        """
        self.directory['aeri'] = 'AE' + date.strftime('%y%m%d')
        self.directory['data'] = self.directory['in'] + self.directory['aeri']
        self.directory['cal']  = self.directory['in'] + self.directory['aeri'] + '/cal/'
        self.directory['ftp']  = self.directory['in'] + 'ftp/' + self.directory['aeri'] + '/'

        return

    def retrieveDataFromNOAA(self):
        """STEP 1:  Retrieve all the necessary
                    data files from the archive at NOAA ESRL
                    (ftp://ftp1.esrl.noaa.gov/psd3/arctic/summit/aeri/)
        
                    Written by Von P. Walden
                                24 Sep 2014
        """
        import os
        from subprocess import call
        
        date  = self.beginningDate 
        while (date <= self.endingDate):
            self.setDirectories(date)
    
            # Get data from NOAA
            print('Copying data from NOAA: ' + self.directory['aeri'])
            # Create a data directory structure, if necessary
            if not os.path.exists(self.directory['data']):
                os.makedirs(self.directory['data'])
                os.makedirs(self.directory['cal'])
                os.makedirs(self.directory['ftp'])
            
            self.directory['ESRL'] = 'ftp://ftp1.esrl.noaa.gov/psd3/arctic/summit/aeri/raw/' + self.directory['aeri'][-8:] + '/'
            
            # Copy the files from NOAA.
            os.chdir(self.directory['ftp'])
            filesToTransfer = ['*.CXS', '*.UVS', '*.SUM', '*.QC', '*.PAR']
            for f in filesToTransfer:
                call(['wget', self.directory['ESRL']+f])
            
            filesToCopy = ['AERICALV.SIP', 'AERIEMIS.ASC', 'AERISUM.SIP', 'FFOVCMR.SI1', 'FFOVCMR.SI2', 'MIRROR.BEG', 'NLAPP.SIP', 'RWASPC.SIP', 'ZFLI.SI1', 'ZFLI.SI1']
            for f in filesToCopy:
                call(['cp', '-f', self.directory['prms']+f, self.directory['cal']])
                
            date+=self.delta
        
        return

    def reprocess(self, hpc=True, hpcType='amd'):
        """STEP 2:  Reprocess the AERI data in a series of sub-steps:
        				1) Use stPAERI_reprocess.cmd to run the original AERI software 
        					routines to create scan-direction average spectra.
        				2) Run the qc method to perform Quality Control on the spectra.
        				3) Run the aerical method to re-calibrate the spectra using
        					Penny Rowe's Matlab code.
                    
                    NOTE that stPAERI_reprocess.cmd can be edited to include any level of reprocessing.
                    [NLAPP -> AERICALV -> SCNDIRAV] -> FFOVC -> ZFLI    [] = current

                    Written by Von P. Walden
                                24 Sep 2014
                    Updated     11 Oct 2014 - Added hpc capability.
                    			 4 Dec 2014 - Included qc and aerical to take advantage
                    			 				of the hpc capability.
        """
        import os
        from subprocess import call, check_output, STDOUT

        def qsubScript(yymmdd):
            f = open(yymmdd+'.sh','w')
            f.write('#!/bin/bash\n')
            f.write('#PBS -k o\n')
            f.write('#PBS -l nodes=1:'+hpcType+':ppn=1,walltime=04:00:00,mem=2gb\n')
            f.write('#PBS -M v.walden@wsu.edu\n')
            f.write('#PBS -m abe\n')
            f.write('#PBS -N AE'+yymmdd+'\n')
            f.write('#PBS -j oe\n')
            f.write('#PBS -d /data/lar/users/vonw/fieldExperiments/summit/data/paeri/logs\n')
            f.write('#PBS -o /data/lar/users/vonw/fieldExperiments/summit/data/paeri/logs\n')
            f.write('date\n')
            f.write('cd /data/lar/users/vonw/reprocessed_tmp/AE'+yymmdd+'\n')
            f.write('cp /home/vonw/work/software/ICECAPS/AERI-reprocessing/ssec/aeri_sw/bin/nlapp.exe .\n')
            f.write('cp /home/vonw/work/software/ICECAPS/AERI-reprocessing/ssec/aeri_sw/bin/aericalv.exe .\n')
            f.write('cp /home/vonw/work/software/ICECAPS/AERI-reprocessing/ssec/aeri_sw/bin/scndirav.exe .\n')
            f.write('cp /home/vonw/work/software/ICECAPS/AERI-reprocessing/ssec/aeri_sw/bin/dmv-ncdf .\n')
            f.write('cp /home/vonw/work/projects/paeri/software/parameter_files/paramsSmt/NLAPP.SIP .\n')
            f.write('cp /home/vonw/work/projects/paeri/software/parameter_files/paramsSmt/AERICALV.SIP .\n')
            f.write('cp /home/vonw/work/projects/paeri/software/parameter_files/paramsSmt/MIRROR.BEG .\n')
            f.write('cp /home/vonw/work/projects/paeri/software/parameter_files/paramsSmt/AERIEMIS.ASC .\n')
            f.write('sed -e "s/yymmdddc/'+yymmdd+'F1/g" /home/vonw/work/projects/paeri/software/parameter_files/paramsSmt/nlapp.template > nlapp_if1\n')
            f.write('sed -e "s/yymmdddc/'+yymmdd+'B1/g" /home/vonw/work/projects/paeri/software/parameter_files/paramsSmt/nlapp.template > nlapp_ib1\n')
            f.write('./nlapp.exe < nlapp_if1 >  /data/lar/users/vonw/fieldExperiments/summit/data/paeri/logs/ssec_'+yymmdd+'.log\n')
            f.write('./nlapp.exe < nlapp_ib1 >> /data/lar/users/vonw/fieldExperiments/summit/data/paeri/logs/ssec_'+yymmdd+'.log\n')
            f.write('sed -e "s/yymmdddc/'+yymmdd+'F1/g" /home/vonw/work/projects/paeri/software/parameter_files/paramsSmt/aericalv1.template > aericalv_if1\n')
            f.write('sed -e "s/yymmdddc/'+yymmdd+'B1/g" /home/vonw/work/projects/paeri/software/parameter_files/paramsSmt/aericalv1.template > aericalv_ib1\n')
            f.write('sed -e "s/yymmdddc/'+yymmdd+'F2/g" /home/vonw/work/projects/paeri/software/parameter_files/paramsSmt/aericalv2.template > aericalv_if2\n')
            f.write('sed -e "s/yymmdddc/'+yymmdd+'B2/g" /home/vonw/work/projects/paeri/software/parameter_files/paramsSmt/aericalv2.template > aericalv_ib2\n')
            f.write('./aericalv.exe < aericalv_if1 >>  /data/lar/users/vonw/fieldExperiments/summit/data/paeri/logs/ssec_'+yymmdd+'.log\n')
            f.write('./aericalv.exe < aericalv_ib1 >>  /data/lar/users/vonw/fieldExperiments/summit/data/paeri/logs/ssec_'+yymmdd+'.log\n')
            f.write('./aericalv.exe < aericalv_if2 >>  /data/lar/users/vonw/fieldExperiments/summit/data/paeri/logs/ssec_'+yymmdd+'.log\n')
            f.write('./aericalv.exe < aericalv_ib2 >>  /data/lar/users/vonw/fieldExperiments/summit/data/paeri/logs/ssec_'+yymmdd+'.log\n')
            f.write('sed -e "s/yymmdd/'+yymmdd+'/g" /home/vonw/work/projects/paeri/software/parameter_files/paramsSmt/scndirav1.template > scndirav1\n')
            f.write('sed -e "s/yymmdd/'+yymmdd+'/g" /home/vonw/work/projects/paeri/software/parameter_files/paramsSmt/scndirav2.template > scndirav2\n')
            f.write('./scndirav.exe < scndirav1 >>  /data/lar/users/vonw/fieldExperiments/summit/data/paeri/logs/ssec_'+yymmdd+'.log\n')
            f.write('./scndirav.exe < scndirav2 >>  /data/lar/users/vonw/fieldExperiments/summit/data/paeri/logs/ssec_'+yymmdd+'.log\n')
            f.write('sed -e "s/yymmdddc/'+yymmdd+'C1/g" /home/vonw/work/projects/paeri/software/parameter_files/paramsSmt/dmv-ncdf-RLC.template > dmv-ncdf_C1\n')
            f.write('sed -e "s/yymmdddc/'+yymmdd+'C2/g" /home/vonw/work/projects/paeri/software/parameter_files/paramsSmt/dmv-ncdf-RLC.template > dmv-ncdf_C2\n')
       	    f.write('sed -e "s/yymmdd/'+yymmdd+'/g" /home/vonw/work/projects/paeri/software/parameter_files/paramsSmt/dmv-ncdf-SUM.template > dmv-ncdf_SUM\n')
       	    f.write('sed -e "s/yymmdddc/'+yymmdd+'F1/g" /home/vonw/work/projects/paeri/software/parameter_files/paramsSmt/dmv-ncdf-CXV.template > dmv-ncdf_F1\n')
       	    f.write('sed -e "s/yymmdddc/'+yymmdd+'B1/g" /home/vonw/work/projects/paeri/software/parameter_files/paramsSmt/dmv-ncdf-CXV.template > dmv-ncdf_B1\n')
       	    f.write('sed -e "s/yymmdddc/'+yymmdd+'F2/g" /home/vonw/work/projects/paeri/software/parameter_files/paramsSmt/dmv-ncdf-CXS.template > dmv-ncdf_F2\n')
       	    f.write('sed -e "s/yymmdddc/'+yymmdd+'B2/g" /home/vonw/work/projects/paeri/software/parameter_files/paramsSmt/dmv-ncdf-CXS.template > dmv-ncdf_B2\n')
       	    f.write('sed -e "s/yymmdddc/'+yymmdd+'F1/g" /home/vonw/work/projects/paeri/software/parameter_files/paramsSmt/dmv-ncdf-UVS.template > dmv-ncdf_F1_UVS\n')
       	    f.write('sed -e "s/yymmdddc/'+yymmdd+'B1/g" /home/vonw/work/projects/paeri/software/parameter_files/paramsSmt/dmv-ncdf-UVS.template > dmv-ncdf_B1_UVS\n')
       	    f.write('sed -e "s/yymmdddc/'+yymmdd+'F2/g" /home/vonw/work/projects/paeri/software/parameter_files/paramsSmt/dmv-ncdf-UVS.template > dmv-ncdf_F2_UVS\n')
       	    f.write('sed -e "s/yymmdddc/'+yymmdd+'B2/g" /home/vonw/work/projects/paeri/software/parameter_files/paramsSmt/dmv-ncdf-UVS.template > dmv-ncdf_B2_UVS\n')
            f.write('./dmv-ncdf < dmv-ncdf_C1 >>  /data/lar/users/vonw/fieldExperiments/summit/data/paeri/logs/ssec_'+yymmdd+'.log\n')
       	    f.write('./dmv-ncdf < dmv-ncdf_C2 >>  /data/lar/users/vonw/fieldExperiments/summit/data/paeri/logs/ssec_'+yymmdd+'.log\n')
       	    f.write('./dmv-ncdf < dmv-ncdf_SUM >>  /data/lar/users/vonw/fieldExperiments/summit/data/paeri/logs/ssec_'+yymmdd+'.log\n')
       	    f.write('./dmv-ncdf < dmv-ncdf_F1 >>  /data/lar/users/vonw/fieldExperiments/summit/data/paeri/logs/ssec_'+yymmdd+'.log\n')
       	    f.write('./dmv-ncdf < dmv-ncdf_B1 >>  /data/lar/users/vonw/fieldExperiments/summit/data/paeri/logs/ssec_'+yymmdd+'.log\n')
       	    f.write('./dmv-ncdf < dmv-ncdf_F2 >>  /data/lar/users/vonw/fieldExperiments/summit/data/paeri/logs/ssec_'+yymmdd+'.log\n')
       	    f.write('./dmv-ncdf < dmv-ncdf_B2 >>  /data/lar/users/vonw/fieldExperiments/summit/data/paeri/logs/ssec_'+yymmdd+'.log\n')
       	    f.write('./dmv-ncdf < dmv-ncdf_F1_UVS >>  /data/lar/users/vonw/fieldExperiments/summit/data/paeri/logs/ssec_'+yymmdd+'.log\n')
       	    f.write('./dmv-ncdf < dmv-ncdf_B1_UVS >>  /data/lar/users/vonw/fieldExperiments/summit/data/paeri/logs/ssec_'+yymmdd+'.log\n')
       	    f.write('./dmv-ncdf < dmv-ncdf_F2_UVS >>  /data/lar/users/vonw/fieldExperiments/summit/data/paeri/logs/ssec_'+yymmdd+'.log\n')
       	    f.write('./dmv-ncdf < dmv-ncdf_B2_UVS >>  /data/lar/users/vonw/fieldExperiments/summit/data/paeri/logs/ssec_'+yymmdd+'.log\n')
            f.write('rm -f aericalv*\n')
       	    f.write('rm	-f AERI*\n')
       	    f.write('rm	-f dmv-ncdf*\n')
       	    f.write('rm	-f MIRROR.BEG\n')
       	    f.write('rm	-f nlapp*\n')
       	    f.write('rm	-f NLAPP*\n')
       	    f.write('rm	-f scndirav*\n')
       	    f.write('rm	-f log.dat\n')
            f.write('date\n')
            f.close()
            
            return
        
        def qsubComplete(ids):
            from subprocess import check_output
            from time import sleep
            # Legacy code...
            #while any(id in check_output(['qstat']) for id in ids):
            #    print('Checking if qsub jobs are complete. Waiting 20 seconds...')
            #    sleep(20)
            # 
            #return
            l = 69    # position of status byte in qstat string.
            while True:
                status = []
                qstat_out = check_output(['qstat']).decode('utf-8')
                for id in ids:
                    b = qstat_out.find(id.decode('utf-8'))
                    if b == -1: 
                        continue
                    else:
                        status.append(qstat_out[b+l])   # Stores status byte
                if ('Q' in status or 'R' in status):    # Checks is processes are either queued or running.
                    print('Jobs are still running... Try again in 10 seconds.')
                    sleep(10)
                else:
                    break
            return
        
        # Reprocess the AERI data using SSEC software.
        os.chdir('/home/vonw/work/software/ICECAPS/AERI-reprocessing/ssec/')
        ids  = []
        date = self.beginningDate
        while (date <= self.endingDate):
            self.setDirectories(date)
            yymmdd = self.directory['aeri'][2:8]            
            if hpc:
                print('Submitting reprocessing job: ' + yymmdd)
                qsubScript(yymmdd)
                idstr  = check_output(['qsub', yymmdd+'.sh'])
                ids.append(idstr[0:6])
            else:
                print('Performing reprocessing job: ' + yymmdd)
                call(['./stPAERI_reprocess.cmd', 'AE'+yymmdd])
            
            date+=self.delta
        
        if hpc:
            qsubComplete(ids)
        
        date = self.beginningDate
        while (date <= self.endingDate):
            self.setDirectories(date)
            yymmdd = self.directory['aeri'][2:8]
            if hpc:
                os.remove('/home/vonw/work/software/ICECAPS/AERI-reprocessing/ssec/'+yymmdd+'.sh')
            
            date+=self.delta
        
        return

    def qc(self):
        """STEP 3:  Perform quality control on the AERI data.

                    Written by Von P. Walden
                                24 Sep 2014
        """
        import oct2py
        octave = oct2py.Oct2Py()
        # Add path to octave; python is using sh, NOT bash.
        import sys
        sys.path.append('/opt/local/bin')
        
        def attributeNum2Str(filename):
            import numpy
            from netCDF4 import Dataset
            
            f = Dataset(filename,'a')
            
            for v in f.variables:
                for a in f.variables[v].ncattrs():
                    val = f.variables[v].getncattr(a)
                    #print v, a, val, type(val)           # Used for testing
                    if isinstance(val, numpy.float32):
                        #print v, a, val, type(val)
                        f.variables[v].setncattr(a,str(val))
            f.close()
            return
        
        # Adds directories containing Matlab M-files to the octave path.
        if self.hostname.rfind('aeolus')>=0 or self.hostname.rfind('compute')>=0:
            octave.addpath('/home/vonw/work/software/ICECAPS/AERI-reprocessing/octave/');
            octave.addpath('/home/vonw/work/software/ICECAPS/AERI-reprocessing/octave/RM/');
            octave.addpath('/home/vonw/work/software/ICECAPS/AERI-reprocessing/octave/QC/');
            octave.addpath('/home/vonw/work/software/ICECAPS/AERI-reprocessing/octave/PC/');
            octave.addpath('/home/vonw/work/software/ICECAPS/AERI-reprocessing/octave/zfli/');
        elif self.hostname.rfind('sila')>=0 or self.hostname.rfind('nuia')>=0:
            octave.addpath('/Users/vonw/work/software/icecaps/AERI-reprocessing/octave/');
            octave.addpath('/Users/vonw/work/software/icecaps/AERI-reprocessing/octave/RM/');
            octave.addpath('/Users/vonw/work/software/icecaps/AERI-reprocessing/octave/QC/');
            octave.addpath('/Users/vonw/work/software/icecaps/AERI-reprocessing/octave/PC/');
            octave.addpath('/Users/vonw/work/software/icecaps/AERI-reprocessing/octave/zfli/');
        
        # Sets QC processing flags.
        BBEflag = 0
        apodflag = 0
        
        # Perform the QC on each date.
        date = self.beginningDate
        while (date <= self.endingDate):
            self.setDirectories(date)
            print('Performing Quality Control: ' + self.directory['aeri'])
            yymmdd = self.directory['aeri'][2:8]

            # Pre-processes the attributes in the RLC and SUM files from numbers to strings.
            #      (This was necessary due to a strange error in the octave netcdf package...)
            attributeNum2Str(self.directory['proc']+'AE'+yymmdd+'/'+yymmdd+'C1.RLC.nc')
            attributeNum2Str(self.directory['proc']+'AE'+yymmdd+'/'+yymmdd+'C2.RLC.nc')
            attributeNum2Str(self.directory['proc']+'AE'+yymmdd+'/'+yymmdd+'.SUM.nc')
            
            good = octave.paeriQC(yymmdd,self.directory['proc']+'AE'+yymmdd,self.directory['QC'],self.directory['final'],self.directory['log'],BBEflag,apodflag,self.nuloCh1,self.nuhiCh1,self.nuloCh2,self.nuhiCh2,self.FFOVhalfAngle,self.version);
                        
            date+=self.delta
        
        return
    
    def rm(self):
        """STEP 4:  Perform the AERI calibration using the Responsivity Method (RM).  
                    This consists of several steps in the AERI processing:
                    
                        aericalv - AERI calibration
                        scndirav - Scan-direction average
                        ffovcmr  - Finite field-of-view correction
                                    
                    These routines were mostly written by Dr. Penny Rowe.
                    
                    Written by Von P. Walden
                                24 Sep 2014
        """
        # Add path to octave; python is using sh, NOT bash.
        import sys
        sys.path.append('/opt/local/bin')
        import oct2py
        octave = oct2py.Oct2Py()

        # Adds directories containing Matlab M-files to the octave path.
        if self.hostname.rfind('aeolus')>=0 or self.hostname.rfind('compute')>=0:
            octave.addpath('/home/vonw/work/software/ICECAPS/AERI-reprocessing/octave/');
            octave.addpath('/home/vonw/work/software/ICECAPS/AERI-reprocessing/octave/RM/');
            octave.addpath('/home/vonw/work/software/ICECAPS/AERI-reprocessing/octave/QC/');
            octave.addpath('/home/vonw/work/software/ICECAPS/AERI-reprocessing/octave/PC/');
            octave.addpath('/home/vonw/work/software/ICECAPS/AERI-reprocessing/octave/zfli/');
        elif self.hostname.rfind('sila')>=0 or self.hostname.rfind('nuia')>=0:
            octave.addpath('/Users/vonw/work/software/icecaps/AERI-reprocessing/octave/');
            octave.addpath('/Users/vonw/work/software/icecaps/AERI-reprocessing/octave/RM/');
            octave.addpath('/Users/vonw/work/software/icecaps/AERI-reprocessing/octave/QC/');
            octave.addpath('/Users/vonw/work/software/icecaps/AERI-reprocessing/octave/PC/');
            octave.addpath('/Users/vonw/work/software/icecaps/AERI-reprocessing/octave/zfli/');

        
        # Perform the AERI calibration on each date.
        date = self.beginningDate
        while (date <= self.endingDate):
            self.setDirectories(date)
            print('Performing AERI calibration: ' + self.directory['aeri'])
            
            yymmdd = self.directory['aeri'][2:8]
            for ch in [1,2]:
                octave.paeriRM(self.directory['log'],int('20'+yymmdd),ch,eval('self.nuloCh'+str(ch)),eval('self.nuhiCh'+str(ch)),self.LASERwavenumberCh1,self.LASERwavenumberCh2,self.cavfactor,self.FFOVhalfAngle);
            
            date+=self.delta
        
        return

    def qcCheck(self):
        """STEP 5:  Creates plots of the spectra at this point in the reprocessing to
					check for bad spectra.  If any bad spectra are found, they must be
					dealt with before performing the PC noise filtering.  This is 
					typically done by viewing the netCDF files manually then using 
					the qcManual method.
					
					Written by Von P. Walden
								4 December 2014
        """
        import matplotlib
        # Force matplotlib to not use any Xwindows backend.
        matplotlib.use('AGG')
        from glob import glob
        import scipy.io.netcdf as netcdf
        import matplotlib.pyplot as plt
        from numpy import transpose, where
        
        date = self.beginningDate
        while (date <= self.endingDate):
            yymmdd          = date.strftime('%Y%m%d')
            print('Creating QC check figure: AE' + yymmdd[2:])
            f               = glob(self.directory['RM']+'smtaerich1.rm_qc.'+yymmdd+'*.cdf')[0]
            c1              = netcdf.netcdf_file(f)
            missingDataFlag = c1.variables['missingDataFlag'][:]
            wnum            = c1.variables['wnum'][:]
            mean_rad        = transpose(c1.variables['mean_rad'][:])
            
            good            = where(missingDataFlag==0)[0]
            bad             = where(missingDataFlag==1)[0]

            plt.figure()
            if good.any():
                plt.subplot(211)
                plt.plot(wnum,mean_rad[:,good])
                plt.axis([400, 2000, -20, 150])
                plt.text(1500,50,'GOOD')
                plt.ylabel('Radiance [mW m-2 st-1 (cm-1)-1]')
                plt.title(f[-36:])
            
            if bad.any():
                plt.subplot(212)
                plt.plot(wnum,mean_rad[:,bad])
                plt.xlabel('Wavenumber (cm-1)')
                plt.ylabel('Radiance [mW m-2 st-1 (cm-1)-1]')
                
            plt.savefig(self.directory['jpgs']+f[-36:]+'.png')
            plt.close('all')
            
            date+=self.delta
        
        return
    
    def qcBadSpectra(self):
        """Creates a plot of window radiance at 962 cm-1 versus record number.
            Used to determine bad spectra that were missed by the normal
            quality control in function qc().
					
					Written by Von P. Walden
								22 Dec 2014
        """
        from glob import glob
        import scipy.io.netcdf as netcdf
        import matplotlib.pyplot as plt
        from numpy import transpose
        
        date = self.beginningDate
        while (date <= self.endingDate):
            yymmdd          = date.strftime('%Y%m%d')
            f               = glob(self.directory['RM']+'smtaerich1.rm_qc.'+yymmdd+'*.cdf')[0]
            c1              = netcdf.netcdf_file(f)
            mean_rad        = transpose(c1.variables['mean_rad'][:])
            
            plt.figure()
            plt.plot(mean_rad[976,:],'.')
            plt.title(f[-36:])
        
        return
        
    def qcCorrectBadSpectra(self, inds):
        """Fills bad spectra with missing values; -9999.

        Inputs:
            inds   - range(2888,2896)   # inds is a Python list of indices to fill.
					
					Written by Von P. Walden
								22 Dec 2014
                    Updated on  05 Nov 2015
        """
        from glob import glob
        from netCDF4 import Dataset
        from numpy import ones, shape
        
        # 1 Oct 2013
        #yymmdd = '20131001'
        #inds = range(3039,3040)
        #  2 Jan 2014
        #yymmdd = '20140102'
        #inds = range(2104,2112)    
        #  5 Jan 2014
        #yymmdd = '20140105'
        #inds = range(3080,3088)    
        #  16 May 2014
        #yymmdd = '20140516'
        #inds = range(3008,3016)
        #  28 Jun 2014
        #yymmdd = '20140628'
        #inds = range(2400,2408)
        #   3 Dec 2014
        #yymmdd = '20141203'
        #inds = range(2888,2896)
        #   6 Dec 2014
        #yymmdd = '20141203'
        #inds = range(1904,1912)   # 1
        #inds = range(1944,1952)   # 2
        #  12 Jun 2015
        #yymmdd = '20150612'
        #inds = range(3192,3200)

        date = self.beginningDate
        while (date <= self.endingDate):
            yymmdd          = date.strftime('%Y%m%d')
            fs              = glob(self.directory['RM']+'smtaeri*qc.'+yymmdd+'*.cdf')
            for f in fs:
                qc = Dataset(f,'a')    
                for ind in inds:
                    qc.variables['mean_rad'][ind] = -9999. * ones(shape(qc.variables['mean_rad'][ind]))
                qc.close()
        
        return
        
    def pc(self):
        """STEP 5:  Perform PCA noise filtering on the AERI data.

                    Written by Von P. Walden
                                4 December 2014
        """
        from datetime import datetime
        import oct2py
        octave = oct2py.Oct2Py()
        # Add path to octave; python is using sh, NOT bash.
        import sys
        sys.path.append('/opt/local/bin')
        
        # Adds directories containing Matlab M-files to the octave path.
        if self.hostname.rfind('aeolus')>=0 or self.hostname.rfind('compute')>=0:
            octave.addpath('/home/vonw/work/software/ICECAPS/AERI-reprocessing/octave/');
            octave.addpath('/home/vonw/work/software/ICECAPS/AERI-reprocessing/octave/RM/');
            octave.addpath('/home/vonw/work/software/ICECAPS/AERI-reprocessing/octave/QC/');
            octave.addpath('/home/vonw/work/software/ICECAPS/AERI-reprocessing/octave/PC/');
            octave.addpath('/home/vonw/work/software/ICECAPS/AERI-reprocessing/octave/zfli/');
        elif self.hostname.rfind('sila')>=0 or self.hostname.rfind('nuia')>=0:
            octave.addpath('/Users/vonw/work/software/icecaps/AERI-reprocessing/octave/');
            octave.addpath('/Users/vonw/work/software/icecaps/AERI-reprocessing/octave/RM/');
            octave.addpath('/Users/vonw/work/software/icecaps/AERI-reprocessing/octave/QC/');
            octave.addpath('/Users/vonw/work/software/icecaps/AERI-reprocessing/octave/PC/');
            octave.addpath('/Users/vonw/work/software/icecaps/AERI-reprocessing/octave/zfli/');

        # Perform the PCA noise filtering on both AERI channels
        begDate = int(datetime.strftime(self.beginningDate,'%Y%m%d'))
        endDate = int(datetime.strftime(self.endingDate,'%Y%m%d'))
        for ch in [1,2]:
            octave.paeriPC(self.directory['RM'],self.directory['PC'],self.directory['final'],begDate,endDate,ch)
        
        return

    def zfli(self):
        """STEP 6:  Resample the AERI data to the standard wavenumber grids.

                    Written by Von P. Walden
                                4 December 2014
        """
        from datetime import datetime
        import oct2py
        octave = oct2py.Oct2Py()
        # Add path to octave; python is using sh, NOT bash.
        import sys
        sys.path.append('/opt/local/bin')
        
        # Adds directories containing Matlab M-files to the octave path.
        if self.hostname.rfind('aeolus')>=0 or self.hostname.rfind('compute')>=0:
            octave.addpath('/home/vonw/work/software/ICECAPS/AERI-reprocessing/octave/');
            octave.addpath('/home/vonw/work/software/ICECAPS/AERI-reprocessing/octave/RM/');
            octave.addpath('/home/vonw/work/software/ICECAPS/AERI-reprocessing/octave/QC/');
            octave.addpath('/home/vonw/work/software/ICECAPS/AERI-reprocessing/octave/PC/');
            octave.addpath('/home/vonw/work/software/ICECAPS/AERI-reprocessing/octave/zfli/');
        elif self.hostname.rfind('sila')>=0 or self.hostname.rfind('nuia')>=0:
            octave.addpath('/Users/vonw/work/software/icecaps/AERI-reprocessing/octave/');
            octave.addpath('/Users/vonw/work/software/icecaps/AERI-reprocessing/octave/RM/');
            octave.addpath('/Users/vonw/work/software/icecaps/AERI-reprocessing/octave/QC/');
            octave.addpath('/Users/vonw/work/software/icecaps/AERI-reprocessing/octave/PC/');
            octave.addpath('/Users/vonw/work/software/icecaps/AERI-reprocessing/octave/zfli/');
        
        # Resample the data for both channels in both the PC and RM directories.
        begDate = int(datetime.strftime(self.beginningDate,'%Y%m%d'))
        endDate = int(datetime.strftime(self.endingDate,'%Y%m%d'))
        # PC directory
        octave.paeriZfli(self.directory['PC'],self.directory['final'],begDate,endDate,1,'rm_pc',self.LASERwavenumberCh1)
        octave.paeriZfli(self.directory['PC'],self.directory['final'],begDate,endDate,2,'rm_pc',self.LASERwavenumberCh2)
        # RM directory
        octave.paeriZfli(self.directory['RM'],self.directory['final'],begDate,endDate,1,'rm_qc',self.LASERwavenumberCh1)
        octave.paeriZfli(self.directory['RM'],self.directory['final'],begDate,endDate,2,'rm_qc',self.LASERwavenumberCh2)
        
        return

    def zfliHPC(self, hpc=True, hpcType='amd'):
        """final is a routine that combines the PC noise filtering and zero
            filling into a single step, plus adds cluster capability.

                    Written by Von P. Walden
                                19 Dec 2014
        """
        from subprocess import check_output

        def qsubScript(yymmdd):
            f = open(yymmdd+'.sh','w')
            f.write('#!/bin/bash\n')
            f.write('#PBS -k o\n')
            f.write('#PBS -l nodes=1:'+hpcType+':ppn=1,walltime=02:00:00\n')
            f.write('#PBS -M v.walden@wsu.edu\n')
            f.write('#PBS -m abe\n')
            f.write('#PBS -N paeri_'+yymmdd+'\n')
            f.write('#PBS -j oe\n')
            f.write('#PBS -d /data/lar/users/vonw/fieldExperiments/summit/data/paeri/logs\n')
            f.write('#PBS -o /data/lar/usersvonw/fieldExperiments/summit/data/paeri/logs\n')
            f.write('date\n')
            f.write('cd /home/vonw/work/software/ICECAPS/AERI-reprocessing/\n')
            f.write('''python -c 'import aeri; paeri = aeri.REPROCESS("'''+yymmdd+'''","''' + yymmdd +'''"); paeri.zfli()' \n''' )
            f.write('date\n')
            f.close()
            
            return
        
        # Create final AERI spectra.
        ids  = []
        date = self.beginningDate
        while (date <= self.endingDate):
            self.setDirectories(date)
            yymmdd = self.directory['aeri'][2:8]            
            if hpc:
                print('Submitting final processing job: ' + yymmdd)
                qsubScript(yymmdd)
                idstr  = check_output(['qsub', yymmdd+'.sh'])
                ids.append(idstr[0:6])
            else:
                print('Skipping final processing; must use hpc: ' + yymmdd)
                pass
            
            date+=self.delta
                
        return
    
    def finalCheck(self):
        """Creates plots of the spectra as a final check.  This makes 
            sure that the Principal Components noise filtering and zero
            filling has worked properly.
					
					Written by Von P. Walden
                                     22 Dec 2014
        """
        import matplotlib
        # Force matplotlib to not use any Xwindows backend.
        matplotlib.use('AGG')
        from glob import glob
        import scipy.io.netcdf as netcdf
        import matplotlib.pyplot as plt
        from numpy import transpose, where
        
        date = self.beginningDate
        while (date <= self.endingDate):
            yymmdd          = date.strftime('%Y%m%d')
            print('Creating final check figure: AE' + yymmdd[2:])
            f1              = glob(self.directory['final']+'smtaerich1nf1turnX1.c1.'+yymmdd+'*.cdf')[0]
            f2              = glob(self.directory['final']+'smtaerich2nf1turnX1.c1.'+yymmdd+'*.cdf')[0]
            f2              = glob(self.directory['RM']+'smtaerich2.rm_qc.'+yymmdd+'*.cdf')[0]
            c1              = netcdf.netcdf_file(f1)
            c2              = netcdf.netcdf_file(f2)
            missingDataFlag = c1.variables['missingDataFlag'][:]
            good            = where(missingDataFlag==0)[0]
            
            plt.figure()
            plt.subplot(211)
            wnum            = c1.variables['wnum'][:]
            mean_rad        = transpose(c1.variables['mean_rad'][:])
            plt.plot(wnum,mean_rad[:,good])
            plt.axis([400, 2000, -20, 150])
            plt.grid()
            plt.ylabel('Ch1 Radiance')
            plt.title(f1[-42:])
            plt.subplot(212)
            wnum            = c2.variables['wnum'][:]
            mean_rad        = transpose(c2.variables['mean_rad'][:])
            plt.plot(wnum,mean_rad[:,good])
            plt.axis([1800, 3000, -0.5, 5.])
            plt.grid()
            plt.xlabel('Wavenumber (cm-1)')
            plt.ylabel('Ch2 Radiance')
                
            plt.savefig(self.directory['jpgs']+f1[-42:]+'_final.jpg')
            plt.close('all')
            
            date+=self.delta
        
        return
    
class AERI:
    def __init__(self, directory):
        if directory:
            if directory[-1] != '/':
                directory = directory + '/'
        self.directory = directory

    def C1(self):
        self.filec1 = self.directory + self.directory[-7:-1] + 'c1.nc'
        import scipy.io.netcdf as netcdf
        from numpy import transpose
        tmp            = netcdf.netcdf_file(self.fileC1,'r')                
        self.Time        = tmp.variables['Time'][:]
        self.wnum1       = tmp.variables['wnum1'][:]
        self.mean_rad    = transpose(tmp.variables['mean_rad'])

    def C2(self):
        self.fileC2 = self.directory + self.directory[-7:-1] + 'c2.nc'
        import scipy.io.netcdf as netcdf
        from numpy import transpose
        tmp              = netcdf.netcdf_file(self.fileC2)                
        self.Time        = tmp.variables['Time'][:]
        self.wnum1       = tmp.variables['wnum1'][:]
        self.mean_rad    = transpose(tmp.variables['mean_rad'])

    def SUM(self):
        self.fileSM = self.directory + self.directory[-7:-1] + '.nc'
        import scipy.io.netcdf as netcdf
        tmp                        = netcdf.netcdf_file(self.fileSM)
        self.Time                  = tmp.variables['Time'][:]
        self.LWresponsivity        = tmp.variables['LWresponsivity'][:]
        self.SWresponsivity        = tmp.variables['SWresponsivity'][:]
        self.LW_HBB_NEN            = tmp.variables['LW_HBB_NEN'][:]
        self.SW_HBB_NEN            = tmp.variables['SW_HBB_NEN'][:]
        self.calibrationHBBtemp    = tmp.variables['calibrationHBBtemp'][:]
        self.calibrationCBBtemp    = tmp.variables['calibrationCBBtemp'][:]
        self.detectorTemp          = tmp.variables['detectorTemp'][:]
        self.coolerCurrent         = tmp.variables['coolerCurrent'][:]
        self.rainSensorIntensity   = tmp.variables['rainSensorIntensity'][:]

    def PAR(self):
        """
        Examples:
            1) To read a PAR file:
                import aeri
                # Creates an AERI object.
                paeri = aeri.AERI('/data/vonw/fieldExperiments/summit/data/paeri/raw/AE140921/')
                # Reads the PAR file and assigns attributes to paeri.
                paeri.PAR()
        """
        self.filePAR = self.directory + self.directory[-7:-1] + '.PAR'
        import re
        import numpy as np
        
        f = open(self.filePAR)
        par = f.read()
        f.close()
        
        ##############################################################################
        # Decode abcvals.num
        bi      = par.find('@@@@@ BEGIN SECTION - abcvals.num @@@@@') + 41
        ei      = bi + (49*6) + 4
        values  = par[bi:ei].replace('\r\n',', ').split(',')
        abcvals = np.array([])
        for value in values:
            abcvals = np.append(abcvals, float(value))
        self.abcvals = np.reshape(abcvals,(6,3))
        
        ##############################################################################
        # Decode aeri.pos
        bi        = par.find('LAT') + 6
        self.lat = float(par[bi:bi+par[bi:].find('\r\n')])
        bi        = par.find('LON') + 6
        self.lon = float(par[bi:bi+par[bi:].find('\r\n')])
        bi        = par.find('ALT') + 6
        self.alt = float(par[bi:bi+par[bi:].find('\r\n')])
        
        ##############################################################################
        # Decode aeriemis.asc
        bi       = par.find('@@@@@ BEGIN SECTION - aeriemis.asc @@@@@') + 101
        aeriemis = np.array([])
        line     = par[bi:bi+par[bi:].find('\r\n')] 
        while line.find('@')<0:
            values = line.split()
            aeriemis = np.append(aeriemis, float(values[0]))
            aeriemis = np.append(aeriemis, float(values[1]))
            bi       = bi + len(line) + 2
            line     = par[bi:bi+par[bi:].find('\r\n')]
        self.aeriemis = np.reshape(aeriemis,(40,2))
                
        ##############################################################################
        # Decode rwaspc.sip
        bi        = par.find('LASERwavenumberCh1') + 20
        self.laserWavenumberCh1 = float(par[bi:bi+par[bi:].find('\r\n')])
        bi        = par.find('LASERwavenumberCh2') + 20
        self.laserWavenumberCh2 = float(par[bi:bi+par[bi:].find('\r\n')])
        
        ##############################################################################
        # Decode aericalv.sip
        bi        = par.find('ratioTestFactor') + 25
        self.ratioTestFactor = float(par[bi:bi+par[bi:].find('\r\n')])
        bi        = par.find('BBcavityFactor') + 24
        self.bbCavityFactor = float(par[bi:bi+par[bi:].find('\r\n')])
        #  OVERRIDE THIS PARTICULAR VALUE BASED ON RECOMMENDATION FROM DAVE TURNER
        self.bbCavityFactor = 39.
        
        ##############################################################################
        # Decode ffovcmr.si1
        bsct    = par.find('@@@@@ BEGIN SECTION - ffovcmr.si1 @@@@@')
        esct    = par.find('@@@@@ END OF SECTION - ffovcmr.si1 @@@@@')
        section = par[bsct:esct] 
        bi      = section.find('rolloffPointLow') + 22
        self.rolloffPointLowCh1 = float(section[bi:bi+section[bi:].find('\r\n')])
        bi      = section.find('rolloffPointHigh') + 23
        self.rolloffPointHighCh1 = float(section[bi:bi+section[bi:].find('\r\n')])
        
        ##############################################################################
        # Decode ffovcmr.si2
        bsct    = par.find('@@@@@ BEGIN SECTION - ffovcmr.si2 @@@@@')
        esct    = par.find('@@@@@ END OF SECTION - ffovcmr.si2 @@@@@')
        section = par[bsct:esct] 
        bi      = section.find('rolloffPointLow') + 22
        self.rolloffPointLowCh2 = float(section[bi:bi+section[bi:].find('\r\n')])
        bi      = section.find('rolloffPointHigh') + 23
        self.rolloffPointHighCh2 = float(section[bi:bi+section[bi:].find('\r\n')])
        
        ##############################################################################
        # Decode zfli.si1
        bsct    = par.find('@@@@@ BEGIN SECTION - zfli.si1 @@@@@')
        esct    = par.find('@@@@@ END OF SECTION - zfli.si1 @@@@@')
        section = par[bsct:esct] 
        bi      = section.find('originalLaserWavenumber') + 34
        self.originalLaserWavenumberCh1 = float(section[bi:bi+section[bi:].find('\r\n')])
        bi      = section.find('outputLaserWavenumber') + 32
        self.outputLaserWavenumberCh1 = float(section[bi:bi+section[bi:].find('\r\n')])
        bi      = section.find('firstWavenumberOut') + 27
        self.firstWavenumberOutCh1 = float(section[bi:bi+section[bi:].find('\r\n')])
        bi      = section.find('lastWavenumberOut') + 26
        self.lastWavenumberOutCh1 = float(section[bi:bi+section[bi:].find('\r\n')])
        
        ##############################################################################
        # Decode zfli.si2
        bsct    = par.find('@@@@@ BEGIN SECTION - zfli.si2 @@@@@')
        esct    = par.find('@@@@@ END OF SECTION - zfli.si2 @@@@@')
        section = par[bsct:esct] 
        bi      = section.find('originalLaserWavenumber') + 34
        self.originalLaserWavenumberCh2 = float(section[bi:bi+section[bi:].find('\r\n')])
        bi      = section.find('outputLaserWavenumber') + 32
        self.outputLaserWavenumberCh2 = float(section[bi:bi+section[bi:].find('\r\n')])
        bi      = section.find('firstWavenumberOut') + 27
        self.firstWavenumberOutCh2 = float(section[bi:bi+section[bi:].find('\r\n')])
        bi      = section.find('lastWavenumberOut') + 26
        self.lastWavenumberOutCh2 = float(section[bi:bi+section[bi:].find('\r\n')])
        
        ##############################################################################
        # Decode AESITTER.SCR
        def findEntries(bindex,eindex):
            lines = section[bindex:eindex].splitlines()
            while '' in lines:
                lines.remove('')
            if len(lines)<=1:
                lines = None
            return lines
        
        bsct     = par.find('@@@@@ BEGIN SECTION - AESITTER.SCR @@@@@')
        esct     = par.find('@@@@@ END OF SECTION - AESITTER.SCR @@@@@')
        section  = par[bsct:esct] 
        bi       = section.rfind('metadata FILE is') + 18
        ei       = section[bi:].find('end') + bi
        aesitter = {} 
        entries  = findEntries(bi,ei)
        while entries:       
            # Last entry is always the variable name; use as dictionary key.
            k = entries[len(entries)-1]
            if k[0]!='#': aesitter[k] = {}       # Rejects commented entries starting with #
            # Now cycle through the entries and extract values based on entry type
            for i,entry in enumerate(entries):
                typ = entry[:entry.find(' is')]
                if (typ=='yellow') or (typ=='green'):
                    m           = re.search('is (.*?);',entry)
                    values      = m.group(1).split()
                    aesitter[k][typ] = (float(values[0]),float(values[1]))
                else:
                    continue
            bi = ei + 6
            ei = section[bi:].find('end') + bi
            entries = findEntries(bi,ei)
        
        self.aesitter = aesitter
        
        return

    def SUMMIT(summit=None):
        summit.nuLoCh1       =   492.
        summit.nuHiCh1       =  1800.
        summit.nuLoCh2       =  1780.
        summit.nuHiCh2       =  3000.
        summit.nuLaserCh1    = 15799.31
        summit.nuLaserCh2    = 15799.39
        summit.cavFactor     =    39.       # As recommended by Dave Turner.
        summit.ffovHalfAngle =     0.0230
        return summit
    

