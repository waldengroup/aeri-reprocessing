function lights = paeriQC_Flags(directory,fin,ftype)
%
%       function createPaeriQCfigures(filename,mail_flag)
%
%   ....This Matlab function reads in the desired PAERI files
%       and creates health and status figures based on the aesitter
%       status lights (green, yellow, red, and blue) and Summary files.
%
%       Input
%       -----
%       directory - directory where summary file resides
%       filename  - filename (w/out extension) of the summary (nc) file (i.e., '001230').
%
%       mail_flag - if set, then createPaeriQCfigures calls a routine that sends
%                    a mail message to certain, interested people.
%
%       Output
%       ------
%       creates jpeg figures of the PAERI health and status
%
%       Written by Von P. Walden
%                  10 August 2010
%                      (Adapted from createPaeriQCfigures.m used at Eureka.)
%
%       Update by C Cox Dec 2011 and Jan 23 2012
%           Checks and updates to this script for use with Summit.  Check
%           for consistency in thresholds with createPaeriQCfigures.py. 
%
%       Update by C Cox Feb 2012
%           Adds a check for dRepsonsivity/dTime in LW and SW resp.
%
%       Update by C Cox April 27, 2012
%           Ability to look in either the smtaerisum.qc or raw summariy
%           files.

pkg load netcdf
import_netcdf

%
%   ....Converts the PAERI files to netcdf.
%

if strcmp(ftype,'SUM')
    fpsum = netcdf.open([directory,'smtaerisummaryX1.b1.',fin],'NC_write');
    Time  = netcdf.getVar(fpsum,netcdf.inqVarID(fpsum,'Time'));
elseif strcmp(ftype,'RAW')
    fpsum = netcdf.open([directory,'/',fin,'.nc'],'NC_write');
    try
    	Time  = netcdf.getVar(fpsum,netcdf.inqVarID(fpsum,'Time'));
    catch
        lights = -1;
        Time   = -1;
        return
    end
end

aesitter = {'dateYYMMDD','timeHHMMSS','Latitude','Longitude','sceneMirrorAngle','calibrationCBBtemp','outsideAirTemp','rackAmbientTemp','rainSensorIntensity','ABBmaxTempDiff','BBsupportStructureTemp','computerTemp','calibrationHBBtemp','airNearBBsTemp','SCEtemp','HBBmaxTempDiff','BBcontroller1temp','HBBtempDrift','mirrorMotorTemp','BBcontroller2temp','atmosphericPressure','fixed2500ohmResistor','coolerPowerSupplyTemp','fixed12KohmResistor','motorDriverTemp','fixed97KohmResistor','interferometerWindowTemp','maxSampleStdDev','interferometerSecondPortTemp','sceneMirPosEncoderDrift','LW_HBB_NEN','airNearInterferometerTemp','SW_HBB_NEN','interferometerEnclosureRelativeHumidity','LWresponsivity','coolerCompressorTemp','coolerCurrent','SWresponsivity','coolerExpanderTemp','detectorTemp'};
nvar     = length(aesitter);

%
%   ....Sets up background (white) and default (green) status lights.
%
lights = ones(nvar,length(Time))*5;                                    % white

%
%   ....Determines yellow and red states for each variable.
%
for k = 6:nvar
    
%    k        % For testing
    
%    eval(['dvar = sum.',char(aesitter(k)),';']);
    try
        dvar = netcdf.getVar(fpsum,netcdf.inqVarID(fpsum,char(aesitter(k))));
        lights(k,isnan(dvar)) = 1;
    catch
        fid1 = fopen('missing_rlcvar_logfile','a');
        fprintf(fid1,[fin ' doesnt have ',char(aesitter(k)),' \n']);
        fclose(fid1);
    end
    
    switch k
        case 6    % calibrationCBBtemp % low red from 233.15 to 213.15! (see documentation).
            ylw = union(find(dvar>=213.15 & dvar<238.15),find(dvar>308.15 & dvar<=313.15));
            red = find(dvar<213.15 | dvar > 313.15);
        case 7    % outsideAirTemp
            ylw = union(find(dvar>=208.15 & dvar<213.15),find(dvar>313.15 & dvar<=323.15));
            red = find(dvar<208.15 | dvar > 323.15);
        case 8    % rackAmbientTemp
            ylw = union(find(dvar>=277.15 & dvar<282.15),find(dvar>314.15 & dvar<=319.15));
            red = find(dvar<277.15 | dvar > 319.15);
        case 9    % rainSensonIntensity
            ylw = union(find(dvar>=6000 & dvar<9400),find(dvar>16500 & dvar<=17000));
            red = find(dvar<6000 | dvar > 17000);
        case 10   % ABBmaxTempDiff
            ylw = find(dvar>0.05 & dvar<=0.1);
            red = find(dvar<0 | dvar > 0.1);
        case 11   % BBsupportStructureTemp
            ylw = union(find(dvar>=208.15 & dvar<213.15),find(dvar>313.15 & dvar<=323.15));
            red = find(dvar<208.15 | dvar > 323.15);
        case 12   % computerTemp
            ylw = union(find(dvar>=277.15 & dvar<282.15),find(dvar>314.15 & dvar<=319.15));
            red = find(dvar<277.15 | dvar > 319.15);
        case 13   % calibrationHBBtemp
            ylw = union(find(dvar>=263.15 & dvar<332.5),find(dvar>333.5 & dvar<=353.15));
            red = find(dvar<263.15 | dvar > 353.15);
        case 14   % airNearBBsTemp
            ylw = union(find(dvar>=208.15 & dvar<213.15),find(dvar>338.15 & dvar<=343.15));
            red = find(dvar<208.15 | dvar > 343.15);
        case 15   % SCEtemp
            ylw = union(find(dvar>=273.15 & dvar<278.15),find(dvar>328.15 & dvar<=333.15));
            red = find(dvar<273.15 | dvar > 333.15);
        case 16   % HBBmaxTempDiff
            ylw = find(dvar>=0.4 & dvar<0.7);
            red = find(dvar<0 | dvar > 0.7);
        case 17   % BBcontroller1temp
            ylw = union(find(dvar>=273.15 & dvar<278.15),find(dvar>318.15 & dvar<=323.15));
            red = find(dvar<273.15 | dvar > 323.15);
        case 18   % HBBtempDrift
            ylw = union(find(dvar>=-0.5 & dvar<-0.05),find(dvar>0.05 & dvar<=0.5));
            red = find(dvar<-0.5 | dvar > 0.5);
        case 19   % mirrorMotorTemp
            ylw = union(find(dvar>=233.15 & dvar<238.15),find(dvar>333.15 & dvar<=363.15));
            red = find(dvar<233.15 | dvar > 363.15);
        case 20   % BBcontroller2temp
            ylw = union(find(dvar>=273.15 & dvar<278.15),find(dvar>318.15 & dvar<=323.15));
            red = find(dvar<273.15 | dvar > 323.15);
        case 21   % atmosphericPressure
            ylw = union(find(dvar>=600 & dvar<650),find(dvar>750 & dvar<=800));
            red = find(dvar<600 | dvar > 800);
        case 22   % fixed2500ohmResistor
            ylw = union(find(dvar>=336.064 & dvar<336.094),find(dvar>336.154 & dvar<=336.184));
            red = find(dvar<336.064 | dvar > 336.184);
        case 23   % coolerPowerSupplyTemp
            ylw = union(find(dvar>=281.15 & dvar<288.15),find(dvar>328.15 & dvar<=333.15));
            red = find(dvar<281.15 | dvar > 333.15);
        case 24   % fixed12KohmResistor
            ylw = union(find(dvar>=293.612 & dvar<293.642),find(dvar>293.702 & dvar<=293.732));
            red = find(dvar<293.612 | dvar > 293.732);
        case 25   % motorDriverTemp
            ylw = union(find(dvar>=273.15 & dvar<278.15),find(dvar>318.15 & dvar<=323.15));
            red = find(dvar<273.15 | dvar > 323.15);
        case 26   % fixed97KohmResistor
            ylw = union(find(dvar>=249.190 & dvar<249.220),find(dvar>249.280 & dvar<=249.310));
            red = find(dvar<249.190 | dvar > 249.310);
        case 27   % interferometerWindowTemp
            ylw = union(find(dvar>=281.15 & dvar<288.15),find(dvar>306.15 & dvar<=311.15));
            red = find(dvar<281.15 | dvar > 311.15);
        case 28   % maxSampleStdDev
            ylw = find(dvar>=0.137 & dvar<0.177);
            red = find(dvar<0 | dvar > 0.177);
        case 29   % interferometerSecondPortTemp
            ylw = union(find(dvar>=281.15 & dvar<288.15),find(dvar>306.15 & dvar<=311.15));
            red = find(dvar<281.15 | dvar > 311.15);
        case 30   % sceneMirPosEncoderDrift
            ylw = union(find(dvar>=281.15 & dvar<288.15),find(dvar>306.15 & dvar<=311.15));
            red = find(dvar<281.15 | dvar > 311.15);
        case 31   % LW_HBB_NEN
            ylw = find(dvar>=0.2 & dvar<1);
            red = find(dvar<0 | dvar > 1);
        case 32   % airNearInterferometerTemp
            ylw = union(find(dvar>=281.15 & dvar<288.15),find(dvar>303.15 & dvar<=308.15));
            red = find(dvar<281.15 | dvar > 308.15);
        case 33   % SW_HBB_NEN
            ylw = find(dvar>=0.015 & dvar<0.1);
            red = find(dvar<0 | dvar > 0.1);
        case 34   % interferometerEnclosureRelativeHumidity
            ylw = find(dvar>12 & dvar<=20);
            red = find(dvar<0 | dvar > 20);
        case 35   % LWresponsivity
            %ylw = union(find(dvar>=0.643 & dvar<0.679),find(dvar>5 & dvar<=5.5));
            %red = find(dvar<0.643 | dvar > 5.5);
            ylw = union(find(dvar>=0.643 & dvar<0.679),find(dvar>4 & dvar<=5));
            red = find(dvar<0.643 | dvar > 5);
            % dR/dt
            drdt = diff(dvar)./diff(Time);
            red  = [red; find(drdt < -0.25 | drdt > 0.25)];
        case 36   % coolerCompressorTemp
            ylw = union(find(dvar>=281.15 & dvar<288.15),find(dvar>308.15 & dvar<=313.15));
            red = find(dvar<281.15 | dvar > 313.15);
        case 37   % coolerCurrent
            ylw = union(find(dvar>=0.2 & dvar<0.3),find(dvar>1.15 & dvar<=1.2));
            red = find(dvar<0.2 | dvar > 1.2);
        case 38   % SWresponsivity
            %ylw = union(find(dvar>=7.72 & dvar<8.15),find(dvar>20 & dvar<=21));
            %red = find(dvar<7.72 | dvar > 21);
            ylw = union(find(dvar>=7 & dvar<7.5),find(dvar>10 & dvar<=11));
            red = find(dvar<7 | dvar > 11);
            % dR/dt
            drdt = diff(dvar)./diff(Time);
            red  = [red; find(drdt < -5 | drdt > 5)];
        case 39   % coolerExpanderTemp
            ylw = union(find(dvar>=281.15 & dvar<288.15),find(dvar>310.15 & dvar<=313.15));
            red = find(dvar<281.15 | dvar > 313.15);
        case 41   % detectorTemp
            ylw = union(find(dvar>=74 & dvar<75),find(dvar>79 & dvar<=85));
            red = find(dvar<74 | dvar > 85);
        otherwise
    end
    
    % Replaces green lights with yellow and red, if necessary.
    lights(k,ylw) = 4;
    lights(k,red) = 1;

end