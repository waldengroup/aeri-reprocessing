function cdfNewDim(inFile,outFile,nulo,nuhi,minL,FFOVhalfAngle,Version)
% C Cox
% 12/14/2010
%
% If the PAERI on-board processing does not do the wavenumber cutoffs,
% the .nc files must be rewritten with the corrected wnum1, wnum, and 
% mean_rad variables. The offending variables dimensions cannot be
% modified, and they cannot be removed due to the limitations of low-level
% netCDF design.

% inFile  = '100702C1.RLC.nc'; % testing!
% outFile = 'smtaerich1.qc.20100702.000309.cdf';
% nulo = 492;
% nuhi = 1800;

pkg load netcdf
import_netcdf    % for octave

ext = 'rlc';
file = rd_netcdf(inFile);
atts = attribute_list(inFile);

% Change the vars!
[nulop,i1]                        = min(abs(file.wnum1-nulo));
[nuhip,i2]                        = min(abs(file.wnum1-nuhi));
if strcmp(ext,'rlc')
    file.averageRadiance          = file.averageRadiance(i1:i2,1:minL);
elseif strcmp(ext,'rnc')
    file.mean_rad                 = file.mean_rad(i1:i2,1:minL);
elseif strcmp(ext,'rfc')
    file.Radiance                 = file.Radiance(i1:i2,1:minL);
end

file.wnum1                        = file.wnum1(i1:i2);
file.maxRoll(isinf(file.maxRoll)) = single(1 / 0); % this is a technicality in the netcdf build...

% create the new one...
ncid = netcdf.create(outFile,'NC_SHARE');

TimeID = netcdf.defDim(ncid,'time',minL);
WnumID = netcdf.defDim(ncid,'wnum',length(file.wnum1));

netcdf.endDef(ncid);
try cdfPut(file.date,'date','int',[],[],ncid);catch;wrterr(inFile,'date');end;     
try cdfPut(file.base_time,'base_time','int',atts.base_time,[],ncid);catch;wrterr(inFile,'base_time');end;    
try cdfPut(file.wnum1,'wnum','double',atts.wnum1,WnumID,ncid);catch;wrterr(inFile,'wnum1');end;   
try cdfPut(file.time_offset(1:minL),'time_offset','double',atts.time_offset,TimeID,ncid);catch;wrterr(inFile,'time_offset');end;   
try cdfPut(file.missingDataFlag(1:minL),'missingDataFlag','float',atts.missingDataFlag,TimeID,ncid);catch;wrterr(inFile,'missingDataFlag');end; 
try cdfPut(file.sceneMirPosEncoderMaxDrift(1:minL),'sceneMirPosEncoderMaxDrift','float',atts.sceneMirPosEncoderMaxDrift,TimeID,ncid);catch;wrterr(inFile,'sceneMirPosEncoderMaxDrift');end; 
try cdfPut(file.BBcavityFactor(1:minL),'BBcavityFactor','float',atts.BBcavityFactor,TimeID,ncid);catch;wrterr(inFile,'BBcavityFactor');end; 
try cdfPut(file.HBBtempOffset(1:minL),'HBBtempOffset','float',atts.HBBtempOffset,TimeID,ncid);catch;wrterr(inFile,'HBBtempOffset');end; 
try cdfPut(file.ABBtempOffset(1:minL),'ABBtempOffset','float',atts.ABBtempOffset,TimeID,ncid);catch;wrterr(inFile,'ABBtempOffset');end; 
try cdfPut(file.HBBbottomTempWeight(1:minL),'HBBbottomTempWeight','float',atts.HBBbottomTempWeight,TimeID,ncid);catch;wrterr(inFile,'HBBbottomTempWeight');end; 
try cdfPut(file.HBBapexTempWeight(1:minL),'HBBapexTempWeight','float',atts.HBBapexTempWeight,TimeID,ncid);catch;wrterr(inFile,'HBBapexTempWeight');end; 
try cdfPut(file.HBBtopTempWeight(1:minL),'HBBtopTempWeight','float',atts.HBBtopTempWeight,TimeID,ncid);catch;wrterr(inFile,'HBBtopTempWeight');end; 
try cdfPut(file.ABBbottomTempWeight(1:minL),'ABBbottomTempWeight','float',atts.ABBbottomTempWeight,TimeID,ncid);catch;wrterr(inFile,'ABBbottomTempWeight');end; 
try cdfPut(file.ABBapexTempWeight(1:minL),'ABBapexTempWeight','float',atts.ABBapexTempWeight,TimeID,ncid);catch;wrterr(inFile,'ABBapexTempWeight');end; 
try cdfPut(file.ABBtopTempWeight(1:minL),'ABBtopTempWeight','float',atts.ABBtopTempWeight,TimeID,ncid);catch;wrterr(inFile,'ABBtopTempWeight');end; 
try cdfPut(file.calibratedSceneID(1:minL),'calibratedSceneID','float',atts.calibratedSceneID,TimeID,ncid);catch;wrterr(inFile,'calibratedSceneID');end; 
try cdfPut(double(file.calibrationHBBtemp(1:minL)),'calibrationHBBtemp','double',atts.calibrationHBBtemp,TimeID,ncid);catch;wrterr(inFile,'calibrationHBBtemp');end; 
try cdfPut(double(file.calibrationCBBtemp(1:minL)),'calibrationCBBtemp','double',atts.calibrationCBBtemp,TimeID,ncid);catch;wrterr(inFile,'calibrationCBBtemp');end; 
try cdfPut(double(file.calibrationAmbientTemp(1:minL)),'calibrationAmbientTemp','double',atts.calibrationAmbientTemp,TimeID,ncid);catch;wrterr(inFile,'calibrationAmbientTemp');end; 
try cdfPut(file.channelNumber(1:minL),'channelNumber','float',atts.channelNumber,TimeID,ncid);catch;wrterr(inFile,'channelNumber');end; 
try cdfPut(file.sceneMirPosEncoderDrift(1:minL),'sceneMirPosEncoderDrift','float',atts.sceneMirPosEncoderDrift,TimeID,ncid);catch;wrterr(inFile,'sceneMirPosEncoderDrift');end; 
try cdfPut(file.HBBmaxTempDiff(1:minL),'HBBmaxTempDiff','float',atts.HBBmaxTempDiff,TimeID,ncid);catch;wrterr(inFile,'HBBmaxTempDiff');end; 
try cdfPut(file.HBBmaxTempDiff(1:minL),'ABBmaxTempDiff','float',atts.ABBmaxTempDiff,TimeID,ncid);catch;wrterr(inFile,'ABBmaxTempDiff');end; 
try cdfPut(file.maxRoll(1:minL),'maxRoll','float',atts.maxRoll,TimeID,ncid);catch;wrterr(inFile,'maxRoll');end; 
try cdfPut(file.maxPitch(1:minL),'maxPitch','float',atts.maxPitch,TimeID,ncid);catch;wrterr(inFile,'maxPitch');end; 
try cdfPut(file.opticsCompartmentRelativeHumidity(1:minL),'opticsCompartmentRelativeHumidity','float',atts.opticsCompartmentRelativeHumidity,TimeID,ncid);catch;wrterr(inFile,'opticsCompartmentRelativeHumidity');end; 
try cdfPut(file.sceneMirrorMotorStep(1:minL),'sceneMirrorMotorStep','float',atts.sceneMirrorMotorStep,TimeID,ncid);catch;wrterr(inFile,'sceneMirrorMotorStep');end; 
try cdfPut(file.sceneMirrorAngle(1:minL),'sceneMirrorAngle','float',atts.sceneMirrorAngle,TimeID,ncid);catch;wrterr(inFile,'sceneMirrorAngle');end; 
try cdfPut(file.maxSampleStdDev(1:minL),'maxSampleStdDev','float',atts.maxSampleStdDev,TimeID,ncid);catch;wrterr(inFile,'maxSampleStdDev');end; 
try cdfPut(file.atmosphericPressure(1:minL),'atmosphericPressure','float',atts.atmosphericPressure,TimeID,ncid);catch;wrterr(inFile,'atmosphericPressure');end; 
try cdfPut(file.interferometerEnclosureRelativeHumidity(1:minL),'interferometerEnclosureRelativeHumidity','float',atts.interferometerEnclosureRelativeHumidity,TimeID,ncid);catch;wrterr(inFile,'interferometerEnclosureRelativeHumidity');end; 
try cdfPut(file.atmosphericRelativeHumidity(1:minL),'atmosphericRelativeHumidity','float',atts.atmosphericRelativeHumidity,TimeID,ncid);catch;wrterr(inFile,'atmosphericRelativeHumidity');end; 
try cdfPut(file.interferometerWindowTemp(1:minL),'interferometerWindowTemp','float',atts.interferometerWindowTemp,TimeID,ncid);catch;wrterr(inFile,'interferometerWindowTemp');end; 
try cdfPut(file.rainSensorIntensity(1:minL),'rainSensorIntensity','float',atts.rainSensorIntensity,TimeID,ncid);catch;wrterr(inFile,'rainSensorIntensity');end; 
try cdfPut(file.detectorTemp(1:minL),'detectorTemp','float',atts.detectorTemp,TimeID,ncid);catch;wrterr(inFile,'detectorTemp');end; 
try cdfPut(file.coolerCurrent(1:minL),'coolerCurrent','float',atts.coolerCurrent,TimeID,ncid);catch;wrterr(inFile,'coolerCurrent');end; 
try cdfPut(file.SCEtemp(1:minL),'SCEtemp','float',atts.SCEtemp,TimeID,ncid);catch;wrterr(inFile,'SCEtemp');end; 
try cdfPut(file.motorDriverTemp(1:minL),'motorDriverTemp','float',atts.motorDriverTemp,TimeID,ncid);catch;wrterr(inFile,'motorDriverTemp');end; 
try cdfPut(file.computerTemp(1:minL),'computerTemp','float',atts.computerTemp,TimeID,ncid);catch;wrterr(inFile,'computerTemp');end; 
try cdfPut(file.rackAmbientTemp(1:minL),'rackAmbientTemp','float',atts.rackAmbientTemp,TimeID,ncid);catch;wrterr(inFile,'rackAmbientTemp');end; 
try cdfPut(file.coolerPowerSupplyTemp(1:minL),'coolerPowerSupplyTemp','float',atts.coolerPowerSupplyTemp,TimeID,ncid);catch;wrterr(inFile,'coolerPowerSupplyTemp');end; 
try cdfPut(file.coolerExpanderTemp(1:minL),'coolerExpanderTemp','float',atts.coolerExpanderTemp,TimeID,ncid);catch;wrterr(inFile,'coolerExpanderTemp');end; 
try cdfPut(file.coolerCompressorTemp(1:minL),'coolerCompressorTemp','float',atts.coolerCompressorTemp,TimeID,ncid);catch;wrterr(inFile,'coolerCompressorTemp');end; 
try cdfPut(file.BBcontroller2temp(1:minL),'BBcontroller2temp','float',atts.BBcontroller2temp,TimeID,ncid);catch;wrterr(inFile,'BBcontroller2temp');end; 
try cdfPut(file.BBcontroller1temp(1:minL),'BBcontroller1temp','float',atts.BBcontroller1temp,TimeID,ncid);catch;wrterr(inFile,'BBcontroller1temp');end; 
try cdfPut(file.fixed12KohmResistor(1:minL),'fixed12KohmResistor','float',atts.fixed12KohmResistor,TimeID,ncid);catch;wrterr(inFile,'fixed12KohmResistor');end; 
try cdfPut(file.mirrorMotorTemp(1:minL),'mirrorMotorTemp','float',atts.mirrorMotorTemp,TimeID,ncid);catch;wrterr(inFile,'mirrorMotorTemp');end; 
try cdfPut(file.airNearBBsTemp(1:minL),'airNearBBsTemp','float',atts.airNearBBsTemp,TimeID,ncid);catch;wrterr(inFile,'airNearBBsTemp');end; 
try cdfPut(file.BBsupportStructureTemp(1:minL),'BBsupportStructureTemp','float',atts.BBsupportStructureTemp,TimeID,ncid);catch;wrterr(inFile,'BBsupportStructureTemp');end; 
try cdfPut(file.interferometerSecondPortTemp(1:minL),'interferometerSecondPortTemp','float',atts.interferometerSecondPortTemp,TimeID,ncid);catch;wrterr(inFile,'interferometerSecondPortTemp');end; 
try cdfPut(file.airNearInterferometerTemp(1:minL),'airNearInterferometerTemp','float',atts.airNearInterferometerTemp,TimeID,ncid);catch;wrterr(inFile,'airNearInterferometerTemp');end; 
try cdfPut(file.outsideAirTemp(1:minL),'outsideAirTemp','float',atts.outsideAirTemp,TimeID,ncid);catch;wrterr(inFile,'outsideAirTemp');end; 
try cdfPut(file.fixed97KohmResistor(1:minL),'fixed97KohmResistor','float',atts.fixed97KohmResistor,TimeID,ncid);catch;wrterr(inFile,'fixed97KohmResistor');end; 
try cdfPut(file.fixed2500ohmResistor(1:minL),'fixed2500ohmResistor','float',atts.fixed2500ohmResistor,TimeID,ncid);catch;wrterr(inFile,'fixed2500ohmResistor');end; 
try cdfPut(double(file.HBBbottomTemp(1:minL)),'HBBbottomTemp','double',atts.HBBbottomTemp,TimeID,ncid);catch;wrterr(inFile,'HBBbottomTemp');end; 
try cdfPut(double(file.HBBapexTemp(1:minL)),'HBBapexTemp','double',atts.HBBapexTemp,TimeID,ncid);catch;wrterr(inFile,'HBBapexTemp');end; 
try cdfPut(double(file.HBBtopTemp(1:minL)),'HBBtopTemp','double',atts.HBBtopTemp,TimeID,ncid);catch;wrterr(inFile,'HBBtopTemp');end; 
try cdfPut(double(file.ABBbottomTemp(1:minL)),'ABBbottomTemp','double',atts.ABBbottomTemp,TimeID,ncid);catch;wrterr(inFile,'ABBbottomTemp');end; 
try cdfPut(double(file.ABBapexTemp(1:minL)),'ABBapexTemp','double',atts.ABBapexTemp,TimeID,ncid);catch;wrterr(inFile,'ABBapexTemp');end; 
try cdfPut(double(file.ABBtopTemp(1:minL)),'ABBtopTemp','double',atts.ABBtopTemp,TimeID,ncid);catch;wrterr(inFile,'ABBtopTemp');end; 
try cdfPut(file.JulianDay(1:minL),'JulianDay','float',atts.JulianDay,TimeID,ncid);catch;wrterr(inFile,'JulianDay');end; 
try cdfPut(file.sceneMirPosEncoder(1:minL),'sceneMirPosEncoder','float',atts.sceneMirPosEncoder,TimeID,ncid);catch;wrterr(inFile,'sceneMirPosEncoder');end;   
try cdfPut(file.sceneMirPosCount(1:minL),'sceneMirPosCount','float',atts.sceneMirPosCount,TimeID,ncid);catch;wrterr(inFile,'sceneMirPosCount');end; 
try cdfPut(file.sceneMirrorPosition(1:minL),'sceneMirrorPosition','float',atts.sceneMirrorPosition,TimeID,ncid);catch;wrterr(inFile,'sceneMirrorPosition');end; 
try cdfPut(file.coadditionsCount(1:minL),'coadditionsCount','float',atts.coadditionsCount,TimeID,ncid);catch;wrterr(inFile,'coadditionsCount');end; 
try cdfPut(file.sceneViewDuration(1:minL),'sceneViewDuration','float',atts.sceneViewDuration,TimeID,ncid);catch;wrterr(inFile,'sceneViewDuration');end; 
try cdfPut(file.systemReleaseNumber(1:minL),'systemReleaseNumber','float',atts.systemReleaseNumber,TimeID,ncid);catch;wrterr(inFile,'systemReleaseNumber');end; 
try cdfPut(file.Altitude(1:minL),'Altitude','float',atts.Altitude,TimeID,ncid);catch;wrterr(inFile,'Altitude');end; 
try cdfPut(file.Longitude(1:minL),'Longitude','float',atts.Longitude,TimeID,ncid);catch;wrterr(inFile,'Longitude');end; 
try cdfPut(file.Latitude(1:minL),'Latitude','float',atts.Latitude,TimeID,ncid);catch;wrterr(inFile,'Latitude');end; 
try cdfPut(file.timeHHMMSS(1:minL),'timeHHMMSS','float',atts.timeHHMMSS,TimeID,ncid);catch;wrterr(inFile,'timeHHMMSS');end; 
try cdfPut(file.dateYYMMDD(1:minL),'dateYYMMDD','float',atts.dateYYMMDD,TimeID,ncid);catch;wrterr(inFile,'dateYYMMDD');end; 
try cdfPut(file.instrumentUnitNumber(1:minL),'instrumentUnitNumber','float',atts.instrumentUnitNumber,TimeID,ncid);catch;wrterr(inFile,'instrumentUnitNumber');end; 
try cdfPut(file.AERIunitNumber(1:minL),'AERIunitNumber','float',atts.AERIunitNumber,TimeID,ncid);catch;wrterr(inFile,'AERIunitNumber');end; 
try cdfPut(file.Time(1:minL),'Time','float',atts.Time,TimeID,ncid);catch;wrterr(inFile,'Time');end; 
if strcmp(ext,'rlc')
    try cdfPut(double(file.averageRadiance(:,1:minL)'),'averageRadiance','double',atts.averageRadiance,[WnumID TimeID],ncid);catch;wrterr(inFile,'averageRadiance');end; 
   %try cdfPut((file.averageRadiance(:,1:minL)'),'averageRadiance','float',atts.averageRadiance,[WnumID TimeID],ncid);catch;wrterr(inFile,'averageRadiance');end; 

elseif strcmp(ext,'rnc')
    try cdfPut(double(file.mean_rad(:,1:minL)'),'mean_rad','double',atts.mean_rad,[WnumID TimeID],ncid);catch;wrterr(inFile,'mean_rad');end; 
    %try cdfPut(double(file.mean_rad(:,1:minL)'),'mean_rad','float',atts.mean_rad,[WnumID TimeID],ncid);catch;wrterr(inFile,'mean_rad');end; 

elseif strcmp(ext,'rfc')
    try cdfPut(double(file.Radiance(:,1:minL)'),'Radiance','double',atts.Radiance,[WnumID TimeID],ncid);catch;wrterr(inFile,'Radiance');end; 
  %  try cdfPut((file.Radiance(:,1:minL)'),'Radiance','float',atts.Radiance,[WnumID TimeID],ncid);catch;wrterr(inFile,'Radiance');end; 

end

% add globals...
attPut('Comments',atts.globals.Comments,ncid)
attPut('FileHistory',atts.globals.FileHistory,ncid)

% add new globals...
Experiment = 'Integrated Characterization of Energy, Clouds, Atmospheric State, and Precipitation at Summit (ICECAPS), PIs are Ralf Bennartz, Matthew Shupe, David Turner and Von P. Walden';
Contact    = 'Von P. Walden (v.walden@wsu.edu)';
Reference  = 'Shupe et al. (2013), Bull. Amer. Meteor. Soc., doi: 10.1175/BAMS-D-11-00249.1.';
Date_Created = num2str(date);
Missing_data_flag = '-9999';
attPut('experiment',Experiment,ncid)
attPut('contact',Contact,ncid)  
attPut('reference_project',Reference,ncid)
attPut('date_created',Date_Created,ncid)
attPut('processing_version',Version,ncid)
attPut('missing_data_flag',Missing_data_flag,ncid)
attPut('site_id','smt',ncid)
attPut('facility_id','X1: Summit, Greenland',ncid)
attPut('FFOVhalfAngle',[num2str(FFOVhalfAngle,'%1.4f') ' radians'],ncid)
attPut('FFOVhalfAngle_description','Field of view half angle used in finite FOV correction',ncid)
attPut('description','This dataset contains Atmospheric Emitted Radiance Interferometer (AERI) summary data.',ncid)   
if str2num(inFile(end-14:end-9)) >= 100624 && str2num(inFile(end-14:end-9)) <= 110711
    Notes = 'For all dates from 24 June 2010 through 11 July 2011, the software for the Polar AERI at Summit Station, Greenland was missing the electronic calibration coefficients in the mandb configuration file.  Therefore, the resistance-to-temperature conversion for all six thermistors in the cold and hot calibration sources were erroneous.  The temperatures for both the calibrated sources were corrected through post-processing for each spectral measurement during the affected time period.  The largest corrections occurred at longer wavelengths (smaller wavenumbers) and during periods of extreme cold weather in March 2011.  The corrections were also largest under clear-sky conditions. The largest corrections were approximately +0.25 mW(m^2 sr cm^-1)^-1 in the "window regions" of the spectra.  The residual (remaining uncorrected) error is expected to be no larger than 10^-3 mW(m^2 sr cm^-1)^-1.  If you have any questions regarding this correction, contact the instrument PI, Von P. Walden (vonw@uidaho.edu).';
    attPut('notes',Notes,ncid)
end
netcdf.close(ncid)

function cdfPut(Var,thename,prec,Att,dimid,ncid) 
pkg load netcdf
import_netcdf
netcdf.reDef(ncid);
varid = netcdf.defVar(ncid,thename,prec,dimid);
netcdf.endDef(ncid);
netcdf.putVar(ncid,varid,Var');
netcdf.reDef(ncid);
if ~isempty(Att);
    Attv = struct2cell(Att);
    Attn = fieldnames(Att);
    for h = 1:length(Attn)
        netcdf.putAtt(ncid,varid,char(Attn(h)),char(Attv(h)));
    end
end
netcdf.endDef(ncid);


% -------------------------------------------------------------------------
function attPut(thename,Att,ncid)
pkg load netcdf
import_netcdf
netcdf.reDef(ncid);
varid = netcdf.getConstant('NC_GLOBAL');
netcdf.putAtt(ncid,varid,thename,Att);
netcdf.endDef(ncid);



% -------------------------------------------------------------------------
function wrterr(inFile,v)
fid1 = fopen('missing_smped_logfile','a');
fprintf(fid1,[inFile ' doesnt have ',v,' \n']);
fclose(fid1);
