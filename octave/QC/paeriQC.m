function [good] = paeriQC(filename,directory,QCdirectory,DirFinal,logDirectory,BBEflag,apodflag,nuloCh1,nuhiCh1,nuloCh2,nuhiCh2,FFOVhalfAngle,Version)
%
%     function good = paeriQC(directory,filename,QCdirectory,figureFlag)
%
% ... This function reads in netcdf files for channel 1 (c1),  channel
%     2 (c2) and summary (sum) for the PAERI and performs quality control
%     (QC) on the spectral radiances.  It writes out new versions of the
%     netcdf files with 1)rd_netcdf bad radiances replaced with missing values
%     (-9999) and 2) a new variable, acceptableRecords, that contains the
%     indices of good records.
%
%     Input
%     -----
%     directory   - directory in which the original paeri data reside
%     filename    - 'yymmdd' filename of paeri data
%     QCdirectory - directory where you want the QC files to be written
%     figureFlag  - flag for creating status figures (0=>no, non-zero+>yes)
%     ext         - type of file to use: 'c' or 'c1', 'rfc' for rfc, etc
%
%     Output
%     ------
%     good     - indices of good records (also written to new netcdf files)
% 
%     Example call to 'paeriQC.m'
%     ---------------------------
%     paeriQC('/data/dataman/paeri/data/FromEureka','070101',...
%          '/data/dataman/paeri/data/QC/QC_checked');
%
%     Written by Von P. Walden
%       31 Aug 2006
%     Modified
%       8 Feb 2008 - Now working on amundsen but without
%          truncation of wnum1 and mean_rad arrays.
%       10 Feb 2008 - Now trying to get array truncation to work.
%          Aug. 2008  C Cox: line 297 added isempty to check Sequence 1
%       25 Feb. 2009 C Cox: update to netcdf toolbox 2R008b
%       26 Feb. 2009 PMR: finish update for matlab R2008b netcdf toolbox
%       27 Feb. 2009 PMR: Change variables as needed for MIXCRA
%       18 Mar. 2009 PMR: Uses rfc files instead of final c1/c2.nc files
%           the rfc files have the FFOV correction but are not on the final
%           wavenumber grid (corresponding to 15799 cm-1).
%       20 Mar. 2009 PMR: Added option for blackbody emissivity correction
%                         And apodization.
%       -- Nov. 2010 C Cox: General updates for Summit. Added hatchOpen
%           flag to output. New bad data flag routine since the Summit .QC
%           files are corrupted.
%       -- Feb. 2012 C Cox: Skip Bad Hatch Flags May and June 2011.   
%       -- 10/02/12  C Cox: 'wnum1' to 'wnum'
% -------------------------------------------------------------------------
%
% NOTES FOR THE CURRENT VERSION OF THE CODE!! -----------------------------
%
%         Feb 2012 - Code paeriQC_ext was modified to this: paeriQC_ext_v2
%         to simplifiy, shorten, and fix some issues.
%         1) Assumes we are using only .RLC files!
%         2) No more FigureFlag! 
%         
%         -update 704-711 standard_dev_mean_rad isky inds for mismatched
%         time vectors.
%
%         21 Aug 2014 - VPW changed the name back to paeriQC.m.  The history 
%                       of this codebase is given above.  Plus I liked the 
%                       original name !!
%               

pkg load netcdf
import_netcdf


% EXCEPTIONS!!! -----------------------------------------------------------
% 1) From 5/20/11 - 6/12/11 the rainSensorIntensity (hatchflag) was
%    was erroneously thrown. These dates (listed below) shall ignore the 
%    rainsensorintensity and will have hatchFlags = [];.
igHF = cellstr(num2str([[110520:110531]' ; [110601:110612]']));
% -------------------------------------------------------------------------

if directory(length(directory)) ~= '/'
    directory = [directory,'/'];
end
if QCdirectory(length(QCdirectory)) ~= '/'
    QCdirectory = [QCdirectory,'/'];
end

% Determine file extension and set
% Watch for upper/lower cases
if exist([directory,filename,'C1.RLC.nc'],'file')
    fileExt1 = 'C1.RLC.nc';
elseif exist([directory,filename,'c1.rlc.nc'],'file')
    fileExt1 = 'c1.rlc.nc';
elseif exist([directory,filename,'c1.RLC.nc'],'file')
    fileExt1 = 'c1.RLC.nc';
else
    error(['No input file ' [directory,filename] 'c1.rlc found']);
end
if exist([directory,filename,'C2.RLC.nc'],'file')
    fileExt2 = 'C2.RLC.nc';
elseif exist([directory,filename,'c2.rlc.nc'],'file')
    fileExt2 = 'c2.rlc.nc';
elseif exist([directory,filename,'c2.RLC.nc'],'file')
    fileExt2 = 'c2.RLC.nc';
else
    error(['No input file ' [directory,filename] 'c2.rlc found']);
end
meanRadStr = 'averageRadiance'; set_m_radFlag = 1 ;


%   ....Determines date and time to build qc filenames.
fname = [directory,filename];

dch1 = rd_netcdf([fname,fileExt1],'Time');
dch2 = rd_netcdf([fname,fileExt2],'Time');
dsum = rd_netcdf([fname,'.SUM.nc'],'Time');

time1   = dch1.Time(1);
strdate = ['20',filename];
strtime = datestr(datenum(strdate,'yyyymmdd')+double(time1./24),'HHMMSS');
fin   = [strdate,'.',strtime,'.cdf'];  % end of filename

% Copy original files over to a directory and fix dimensions for two cases:
%   1) Truncated wavenumber range for the intended PAERI bandwidth.
%   2) In cases where PAERI end-of-day restart causes differences in the
%      lengths of time vectors between ch1, ch2, sum, the time vectors
%      are rewritten to be the minimum length.
if any(diff([length(dch1.Time) length(dch2.Time) length(dsum.Time)])) ~= 0
    fid1 = fopen([logDirectory,'dim_time_length_logfile'],'a');
    fprintf(fid1,['Dimensions for ch1 ch2 sum, ' fname ': '  [num2str(length(dch1.Time))  ' ' num2str(length(dch2.Time)) ' ' num2str(length(dsum.Time))] '\n']);
    fclose(fid1);
end

minL = min([length(dch1.Time) length(dch2.Time) length(dsum.Time)]);
cdfNewDim([fname,fileExt1],[QCdirectory,'smtaerich1.qc.',fin],nuloCh1,nuhiCh1,minL,FFOVhalfAngle,Version); % wnum cutoffs didnt come thru in onboard processing.  new dimensions means these files have to be entirely rewritten. this replaces the copyfiles of the next two lines. 
cdfNewDim([fname,fileExt2],[QCdirectory,'smtaerich2.qc.',fin],nuloCh2,nuhiCh2,minL,FFOVhalfAngle,Version);
cdfNewDimSum([fname,'.SUM.nc']  ,[DirFinal,'smtaerisummaryX1.b1.',fin],minL,FFOVhalfAngle,Version); % ccox 11/22/2010: see comment line 59

% ... Open new netcdf files with write permission
fpc1 = netcdf.open([QCdirectory,'smtaerich1.qc.',fin],'WRITE');
fpc2 = netcdf.open([QCdirectory,'smtaerich2.qc.',fin],'WRITE');
fpsm = netcdf.open([DirFinal,'smtaerisummaryX1.b1.',fin],'WRITE');

% Rename "Radiance" or "averageRadiance" to "mean_rad" in netcdf file
if set_m_radFlag
    id.c1.m_rad = netcdf.inqVarID(fpc1, meanRadStr)          ;
    netcdf.reDef(fpc1) ;
    netcdf.renameVar(fpc1,id.c1.m_rad,'mean_rad') ;
    netcdf.endDef(fpc1) ;
    
    netcdf.reDef(fpc2) ;
    id.c2.m_rad = netcdf.inqVarID(fpc2, meanRadStr)          ;
    netcdf.renameVar(fpc2,id.c2.m_rad,'mean_rad') ;
    netcdf.endDef(fpc2) ;
end
    
% ... Get all the dimension and variable ids we are going to need.
did.c1.wnum  = netcdf.inqDimID(fpc1,'wnum') ;
did.c1.time  = netcdf.inqDimID(fpc1,'time') ;
did.c2.time  = netcdf.inqDimID(fpc2,'time') ;
did.sm.wnum1 = netcdf.inqDimID(fpsm,'wnum1') ;
did.sm.wnum2 = netcdf.inqDimID(fpsm,'wnum2') ;
did.sm.time  = netcdf.inqDimID(fpsm,'time') ;

id.c1.Time  = netcdf.inqVarID(fpc1,'Time') ;
id.c1.YYMMD = netcdf.inqVarID(fpc1,'dateYYMMDD');
id.c1.HHMMS = netcdf.inqVarID(fpc1,'timeHHMMSS');
id.c1.wnum  = netcdf.inqVarID(fpc1,'wnum') ;
id.c1.m_rad = netcdf.inqVarID(fpc1,'mean_rad') ;
id.c1.missD = netcdf.inqVarID(fpc1,'missingDataFlag')   ;
id.c1.hatch = netcdf.inqVarID(fpc1,'time_offset')         ;
id.c1.btime = netcdf.inqVarID(fpc1,'base_time') ;
id.c1.t_off = netcdf.inqVarID(fpc1,'time_offset') ;

id.c2.wnum  = netcdf.inqVarID(fpc2,'wnum')             ;
id.c2.m_rad = netcdf.inqVarID(fpc2,'mean_rad')          ;
id.c2.missD = netcdf.inqVarID(fpc2,'missingDataFlag')   ;
id.c2.m_rad = netcdf.inqVarID(fpc2,'mean_rad')          ;
id.c2.hatch = netcdf.inqVarID(fpc2,'time_offset')         ;
id.c2.btime = netcdf.inqVarID(fpc2,'base_time')         ;
id.c2.t_off = netcdf.inqVarID(fpc2,'time_offset') ;

id.sm.missD = netcdf.inqVarID(fpsm,'missingDataFlag')   ;
id.sm.hatch = netcdf.inqVarID(fpsm,'time_offset')         ;
id.sm.btime = netcdf.inqVarID(fpsm, 'base_time') ;
id.sm.t_off = netcdf.inqVarID(fpsm, 'time_offset') ;  

%   ....Sets up some time variables.
Time                = netcdf.getVar(fpc1, id.c1.Time);
dateYYMMDD          = netcdf.getVar(fpc1, id.c1.YYMMD);
timeHHMMSS          = netcdf.getVar(fpc1, id.c1.HHMMS);

%   time_offset
c1_time_offset      = floor((Time - Time(1))*3600);
c2_time_offset      = c1_time_offset;
sm_time_offset      = c1_time_offset;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   ....Determine sequences that contain "bad data", defined to be 
%       sequences with RED QC flags.
%
%%% 
% %% Returns flags for each sensor at each timestamp. %% %
% %% 5 = white, 4 = yellow, 1 = red        %% %
% 06 - calibrationCBBtemp
% 07 - outsideAirTemp
% 08 - rackAmbientTemp
% 09 - rainSensorIntensity
% 10 - ABBmaxTempDiff
% 11 - BBsupportStructureTemp
% 12 - computerTemp
% 13 - calibrationHBBtemp
% 14 - airNearBBsTemp
% 15 - SCEtemp
% 16 - HBBmaxTempDiff
% 17 - BBcontroller1temp
% 18 - HBBtempDrift
% 19 - mirrorMotorTemp
% 20 - BBcontroller2temp
% 21 - atmosphericPressure
% 22 - fixed2500ohmResistor
% 23 - coolerPowerSupplyTemp
% 24 - fixed12KohmResistor
% 25 - motorDriverTemp
% 26 - fixed97KohmResistor
% 27 - interferometerWindowTemp
% 28 - maxSampleStdDev
% 29 - interferometerSecondPortTemp
% 30 - sceneMirPosEncoderDrift
% 31 - LW_HBB_NEN
% 32 - airNearInterferometerTemp
% 33 - SW_HBB_NEN
% 34 - interferometerEnclosureRelativeHumidity
% 35 - LWresponsivity
% 36 - coolerCompressorTemp
% 37 - coolerCurrent
% 38 - SWresponsivity
% 39 - coolerExpanderTemp
% 40 - detectorTemp
lights = paeriQC_Flags(DirFinal,fin,'SUM');

% Set indices of flags to check...
RedFlagParams = [6 10 13 16 18 28 31 33 35 38];

if any(strcmp(filename,igHF)) % see exceptions at beginning of script!
    hatchFlags = [];
else
    hatchFlags = find(lights(9,:) == 1);
end

redFlags = [];
for k1 = 1:length(RedFlagParams)
    redFlags = [redFlags find(lights(RedFlagParams(k1),:) == 1)];
end

numberOfSpectraPerSequence = 8;    % NOTE: Currently hard-wired for Summit data!!

icheck = setdiff(hatchFlags,redFlags) ;

badSpectra   = unique([redFlags hatchFlags]);
badSequences = unique(floor(badSpectra/(numberOfSpectraPerSequence + 0.000001)));

%   ... Throw out sequences before and after hatch closings.
%       Actually this throws out spectra before and after red flags(?) PMR
badSequences = unique([badSequences-1 badSequences badSequences+1]) ;
ind = find(badSequences>0 & badSequences<(length(Time)./numberOfSpectraPerSequence) );
badSequences = badSequences(ind);
%

%   ....Determine which spectra to throw out based on bad sequences.
b = (badSequences * 8) + 1;
e = (badSequences + 1) * 8;
bad = [];
if isempty(badSequences) == 0 % if there are inds in badS. and the first badS. is
    if(badSequences(1) == 1)  % one, fill it with 1:8 then continue. The old version
        bad = 1:8;            % would start with [].  b and e do not allow for 1:8
    end                       % to be included by default. This adds 1:8 if they are 
end                           % bad.
for k = 1:length(b)
    bad = [bad b(k):e(k)];
end
    
%   ....Determine good indices.
good = setxor(1:length(Time),bad);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%   

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   ... Replaces bad data in variables with missing values.
%       Notes: 
%           1) not going to resize because to do that you must rename
%              variables, making the netcdf file much bigger.
%           2) switch order of arrays to account for opposite convention.
%
c1_wn                    = netcdf.getVar(fpc1, id.c1.wnum)   ;
c1_lwn                   = length(c1_wn) ;

c1_missingValue          = netcdf.getAtt(fpc1,id.c1.m_rad,'missing_value');
% c1_missingValue is a variable sometimes and a string at other times. ??
if(ischar(c1_missingValue))
    c1_missingValue = str2num(c1_missingValue);
end

c1_missingDataFlag       = netcdf.getVar(fpc1,id.c1.missD)         ;
c1_mean_rad              = netcdf.getVar(fpc1,id.c1.m_rad)'        ;
c1_hatchOpen             = ones(size(Time))                ;
c1_hatchOpen(hatchFlags) = 0                                       ;

netcdf.reDef(fpc1)
id.c1.hatch = netcdf.defDim(fpc1,'hatch_dim',length(Time));
id.c1.hatch = netcdf.defVar(fpc1,'hatchOpen','double',id.c1.hatch);
netcdf.putAtt(fpc1,id.c1.hatch,  'long_name','hatch open flag. 1 = open') ;
netcdf.putAtt(fpc1,id.c1.hatch,  'units',    'unitless') ;
netcdf.putAtt(fpc1,id.c1.hatch,  'valid_min', 0) ;
netcdf.putAtt(fpc1,id.c1.hatch,  'valid_max', 1) ;
netcdf.endDef(fpc1)

c2_wn                    = netcdf.getVar(fpc2,id.c2.wnum)         ;
c2_lwn                   = length(c2_wn);

c2_missingValue          = netcdf.getAtt(fpc2,id.c2.m_rad,'missing_value');
% c2_missingValue is a variable sometimes and a string at other times. ??
if(ischar(c2_missingValue))
    c2_missingValue = str2num(c2_missingValue)                       ;
end


c2_missingDataFlag       = netcdf.getVar(fpc2,id.c2.missD)  ;
c2_mean_rad              = netcdf.getVar(fpc2,id.c2.m_rad)'        ;
c2_hatchOpen             = ones(size(Time))                  ;
c2_hatchOpen(hatchFlags) = 0                                         ;

netcdf.reDef(fpc2)
id.c2.hatch = netcdf.defDim(fpc2,'hatch_dim',length(Time));
id.c2.hatch = netcdf.defVar(fpc2,'hatchOpen','double',id.c2.hatch);
netcdf.putAtt(fpc2,id.c2.hatch,  'long_name','hatch open flag. 1 = open') ;
netcdf.putAtt(fpc2,id.c2.hatch,  'units',    'unitless') ;
netcdf.putAtt(fpc2,id.c2.hatch,  'valid_min', 0) ;
netcdf.putAtt(fpc2,id.c2.hatch,  'valid_max', 1) ;
netcdf.endDef(fpc2)

sm_missingDataFlag       = netcdf.getVar(fpsm,id.sm.missD)  ;
sm_hatchOpen             = ones(size(Time))                  ;
sm_hatchOpen(hatchFlags) = 0                                         ;

netcdf.reDef(fpsm)
id.sm.hatch = netcdf.defDim(fpsm,'hatch_dim',length(Time));
id.sm.hatch = netcdf.defVar(fpsm,'hatchOpen','double',id.sm.hatch);
netcdf.putAtt(fpsm,id.sm.hatch,  'long_name','hatch open flag. 1 = open') ;
netcdf.putAtt(fpsm,id.sm.hatch,  'units',    'unitless') ;
netcdf.putAtt(fpsm,id.sm.hatch,  'valid_min', 0) ;
netcdf.putAtt(fpsm,id.sm.hatch,  'valid_max', 1) ;
netcdf.endDef(fpsm)


for k = bad

    %sprintf(['Processing bad record #',num2str(k)])
    
    c1_missingDataFlag(k) = 1;
    c1_mean_rad(k,:)      = ones(1,c1_lwn) .* c1_missingValue;

    c2_missingDataFlag(k) = 1;
    c2_mean_rad(k,:)      = ones(1,c2_lwn) .* c2_missingValue;

    sm_missingDataFlag(k) = 1;

end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%    ... Corrects Blackbody emissivity and fixes the calibration
%        (do this before setting bad spectra to missingValue
%
if BBEflag && isempty(good)==0
    fp    = fpc1;
    cFac  = netcdf.getVar(fp,netcdf.inqVarID(fp,'BBcavityFactor'));
    HBBT  = netcdf.getVar(fp,netcdf.inqVarID(fp,'calibrationHBBtemp'));
    CBBT  = netcdf.getVar(fp,netcdf.inqVarID(fp,'calibrationCBBtemp'));
    AmbT  = netcdf.getVar(fp,netcdf.inqVarID(fp,'calibrationAmbientTemp'));
    c1_mean_rad(good,:) = BBemissCorr(c1_wn,c1_mean_rad(good,:),...
        cFac(good),HBBT(good),CBBT(good),AmbT(good)) ;
    
    fp    = fpc2 ;
    cFac  = netcdf.getVar(fp,netcdf.inqVarID(fp,'BBcavityFactor'));
    HBBT  = netcdf.getVar(fp,netcdf.inqVarID(fp,'calibrationHBBtemp'));
    CBBT  = netcdf.getVar(fp,netcdf.inqVarID(fp,'calibrationCBBtemp')); 
    AmbT  = netcdf.getVar(fp,netcdf.inqVarID(fp,'calibrationAmbientTemp'));
    c2_mean_rad(good,:) = BBemissCorr(c2_wn,c2_mean_rad(good,:),...
        cFac(good),HBBT(good),CBBT(good),AmbT(good)) ;
    
end
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%    ... Apodizes if apodflag not 0
%        (only for good spectra)
%
if apodflag
    for k=1:length(good)
        
        % Fill in with Planck function back to zero
        wn = c1_wn ;
        dnu = diff(wn(1:2));
        nyquist = dnu*16384/2 ;
        nu  = [1:dnu:wn(1)-dnu wn(:)' max(wn)+dnu:dnu:nyquist];
        inds = find(wn>680 & wn<=705);
        T   = mean(brightnessT(wn(inds),c1_mean_rad(good(k),inds)));
        rin = 1e3*plancknu(nu,T)' ;
        
        inits = find(nu>=min(wn) & nu<=max(wn));
        rin(inits) = interp1(wn,c1_mean_rad(good(k),:),nu(inits)) ;
        
        rout1 = apodizer(rin,2) ;
        rin1  = rin ;
        nu1   = nu ;

        % Fill in with Planck function back to zero
        wn = c2_wn ;
        dnu = diff(wn(1:2));
        nyquist = dnu*16384/2 ;
        nu  = [1:dnu:wn(1)-dnu wn(:)' max(wn)+dnu:dnu:nyquist];
        rin = 1e3*plancknu(nu,T)' ;
        
        inits = find(nu>=min(wn) & nu<=max(wn));
        rin(inits) = interp1(wn,c2_mean_rad(good(k),:),nu(inits)) ;
        
        rout2 = apodizer(rin,2) ;
        rin2  = rin ;
        nu2   = nu ;

        if figureFlag
            subplot(211)
            plot(nu1,rin1,nu1,real(rout1)) ;
            legend('original','apodized');
            
            subplot(212)
            plot(nu2,rin2,nu2,real(rout2)) ;
            legend('original','apodized');
            pause
        end
        
        % Replace unapodized spectra with apodized spectra
        
        c1_mean_rad(good(k),:) = interp1(nu1,rout1,c1_wn') ;
        c2_mean_rad(good(k),:) = interp1(nu2,rout2,c2_wn') ;
    end
 
    
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                     Corrects base_time and time_offset 
% ... base_time
%Time         = Time(1:ns);
%dateYYMMDD   = dateYYMMDD(1:ns);
YY           = double(2000 + floor(dateYYMMDD(1)/10000));
MM           = double(floor((dateYYMMDD(1)-(floor(dateYYMMDD(1)/10000)*10000))/100));
DD           = double(rem((dateYYMMDD(1)-(floor(dateYYMMDD(1)/10000)*10000)),100));
c1_base_time = (datenum(YY,MM,DD)-datenum(1970,1,1))*24*3600 + Time(1)*3600;
c2_base_time = c1_base_time ;
sm_base_time = c1_base_time ;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   ....Writes corrected variables back to netCDF QC files
% Note: matlab convention is opposite what you get in the ncdump, e.g.
% c1_mean_rad: ncdump says time x wnum1, matlab says nu x time
% So size(c1_mean_rad) should be wnum1 x time.  If you get it wrong it
% still works, but gives you junk!

netcdf.putVar(fpc1, id.c1.btime,       c1_base_time);
netcdf.putVar(fpc1, id.c1.wnum,               c1_wn);
netcdf.putVar(fpc1, id.c1.m_rad,        c1_mean_rad'); % (wnum1 x time)
netcdf.putVar(fpc1, id.c1.missD, c1_missingDataFlag);
netcdf.putVar(fpc1, id.c1.hatch,       c1_hatchOpen);
netcdf.putVar(fpc1, id.c1.t_off,     c1_time_offset);

netcdf.putVar(fpc2, id.c2.btime,       c2_base_time);
netcdf.putVar(fpc2, id.c2.wnum,               c2_wn);
netcdf.putVar(fpc2, id.c2.m_rad,        c2_mean_rad'); % (wnum1 x time)
netcdf.putVar(fpc2, id.c2.missD, c2_missingDataFlag);
netcdf.putVar(fpc2, id.c2.hatch,       c2_hatchOpen);
netcdf.putVar(fpc2, id.c2.t_off,     c2_time_offset);    

netcdf.putVar( fpsm, id.sm.btime,       sm_base_time);
netcdf.putVar( fpsm, id.sm.missD, sm_missingDataFlag);
netcdf.putVar( fpsm, id.sm.hatch,       sm_hatchOpen);
netcdf.putVar( fpsm, id.sm.t_off,     sm_time_offset);    


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%   ....Creates AERI-like variables to make PAERI files compatible with
%       those generated by DOE ARM program.
%
time           = floor(Time*3600);
qc_time        = -9999*ones(size(time));     % qc_time = -9999.*size(time);
Time_UTC_hours = Time;
%
%   ....Coordinates and altitude were obtained from the Integrated Global
%       Radiosonde Archive (IGRA) at ftp://ftp.ncdc.noaa.gov/pub/data/igra.
%
lat = 72.59622;                % North
lon = -38.4297;               % Negative, since defined as 'east longitude'
alt = 3255;                    % above MSL

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%..........................  Write variables back to netCDF file .........

% Defines global variables.

netcdf.reDef(fpc1)
netcdf.reDef(fpc2)
netcdf.reDef(fpsm)

% Note: getting the id from an assignment is ~70 times faster than getting
% it from the netcdf file each time
[ns,nw1]  = size(c1_mean_rad);
did.c1.scalr = netcdf.defDim(fpc1,'scalar',      1       ) ;
did.c1.timeN = netcdf.defDim(fpc1,'time1',       ns      ) ;
did.c1.waveN = netcdf.defDim(fpc1,'wavenumber1', c1_lwn ) ;
did.c2.scalr = netcdf.defDim(fpc2,'scalar',      1       ) ;
did.c2.timeN = netcdf.defDim(fpc2,'time2',       ns      ) ;
did.c2.waveN = netcdf.defDim(fpc2,'wavenumber2', c2_lwn ) ;
did.sm.scalr = netcdf.defDim(fpsm,'scalar',      1       ) ;
did.sm.timeN = netcdf.defDim(fpsm,'times',       ns      ) ;


% define wnumsum5/6 for sum file 
id.sm.wsum5 = netcdf.defVar(fpsm,'wnumsum5','float', did.sm.wnum1)  ;
id.sm.wsum6 = netcdf.defVar(fpsm,'wnumsum6','float', did.sm.wnum2) ;
id.sm.skyn1 = netcdf.defVar(fpsm,'SkyNENCh1','float',[did.sm.wnum1,did.sm.time]);
id.sm.skyn2 = netcdf.defVar(fpsm,'SkyNENCh2','float',[did.sm.wnum2,did.sm.time]);

netcdf.endDef(fpsm);

wnumsum5  = netcdf.getVar(fpsm,netcdf.inqVarID(fpsm,'wnum1')) ; 
wnumsum6  = netcdf.getVar(fpsm,netcdf.inqVarID(fpsm,'wnum2')) ; 
SkyNENCh1 = netcdf.getVar(fpsm,netcdf.inqVarID(fpsm,'SkyNENch1')) ;
SkyNENCh2 = netcdf.getVar(fpsm,netcdf.inqVarID(fpsm,'SkyNENch2')) ;

netcdf.putVar(fpsm, id.sm.wsum5, wnumsum5  ) ;
netcdf.putVar(fpsm, id.sm.wsum6, wnumsum6  ) ;
netcdf.putVar(fpsm, id.sm.skyn1, SkyNENCh1 ) ;
netcdf.putVar(fpsm, id.sm.skyn2, SkyNENCh2 ) ;

netcdf.reDef(fpsm);

datStr = [datestr(datenum(YY,MM,DD),29),' 00:00:00 0:00 GMT'] ;

fpnames     = [fpc1 fpc2 fpsm];
strnames      = {'c1','c2','sm'};
for icase=1:3
    
 d = fpnames(icase) ;  % fpc1, fpc2, or fpsm
 s = char(strnames(icase)) ;  % fpc1, fpc2, or fpsm
    
 id.(s).time  = netcdf.defVar(d,'time',           'double', did.(s).timeN);
 id.(s).qc_tm = netcdf.defVar(d,'qc_time',        'double', did.(s).timeN);
 id.(s).TmUTC = netcdf.defVar(d,'Time_UTC_hours', 'double', did.(s).time );
 id.(s).lat   = netcdf.defVar(d,'lat',            'float',  did.(s).scalr);
 id.(s).lon   = netcdf.defVar(d,'lon',            'float',  did.(s).scalr);
 id.(s).alt   = netcdf.defVar(d,'alt',            'float',  did.(s).scalr);

 % Put new variables
 netcdf.endDef( d )
 netcdf.putVar( d, id.(s).time,  time);
 netcdf.putVar( d, id.(s).qc_tm, qc_time);
 netcdf.putVar( d, id.(s).TmUTC, Time_UTC_hours);
 netcdf.putVar( d, id.(s).lat,   lat);
 netcdf.putVar( d, id.(s).lon,   lon);
 netcdf.putVar( d, id.(s).alt,   alt);

 % Re-enter define mode.
 netcdf.reDef(d);

 % Create an attribute associated with the variable.
 netcdf.putAtt(d,id.(s).time, 'long_name','Time offset from midnight');
 netcdf.putAtt(d,id.(s).time, 'units',    ['seconds since ',datStr]);
 netcdf.putAtt(d,id.(s).qc_tm,'long_name','Dummy variable for compatibility with ARM AERI files');
 netcdf.putAtt(d,id.(s).qc_tm,'units',    'unitless') ;
 netcdf.putAtt(d,id.(s).TmUTC,'long_name','Time at center of AERI sky observation period') ;
 netcdf.putAtt(d,id.(s).TmUTC,'units',    ['hours since ',datStr]) ;
 netcdf.putAtt(d,id.(s).TmUTC,'missing_value', -9999) ;
 netcdf.putAtt(d,id.(s).lat,  'long_name','north latitude') ;
 netcdf.putAtt(d,id.(s).lat,  'units',    'degrees') ;
 netcdf.putAtt(d,id.(s).lat,  'valid_min', -90) ;
 netcdf.putAtt(d,id.(s).lat,  'valid_max', 90) ;
 netcdf.putAtt(d,id.(s).lon,  'long_name','east longitude') ;
 netcdf.putAtt(d,id.(s).lon,  'units',    'degrees') ;
 netcdf.putAtt(d,id.(s).lon,  'valid_min', -180) ;
 netcdf.putAtt(d,id.(s).lon,  'valid_max', 180) ;
 netcdf.putAtt(d,id.(s).alt,  'long_name','altitude') ;
 netcdf.putAtt(d,id.(s).alt,  'units',    'meters above Mean Sea Level');

 % Only do this for fpc1 and fpc2
 if icase==1 || icase==2
 
  if icase==1
     cno_wnum  = c1_wn ;
  elseif icase==2
     cno_wnum  = c2_wn ;
  end

  % wnum needs to be type float
  %id.(s).wnum = netcdf.defVar(d,'wnum','double',did.(s).waveN) ;    
  nurangeStr  = ['[',num2str(min(cno_wnum)),'  ',num2str(max(cno_wnum)),']']  ; 

  %netcdf.endDef(d); % out of define mode to put var
  %netcdf.putVar(d,id.(s).wnum, cno_wnum);

  %netcdf.reDef(d);  % into define mode to put att
  netcdf.putAtt(d,id.(s).wnum,'long_name',          'Wave number in reciprocal centimeters' );
  netcdf.putAtt(d,id.(s).wnum,'units',              'cm^-1' );
  netcdf.putAtt(d,id.(s).wnum,'range_of_values',     nurangeStr);
  netcdf.putAtt(d,id.(s).wnum,'independent_interval',num2str(cno_wnum(2)-cno_wnum(1)));
  netcdf.endDef(d); % out of define mode

 end
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


for ich=1:2
    ichS = num2str(ich) ;
    d    = fpnames(ich) ;  % fpc1, fpc2, or fpsm
    
    % Define values that are different for the two channels
    if ich==1
        did_time = netcdf.inqDimID(d,'time1') ;
    elseif ich==2
        did_time = netcdf.inqDimID(d,'time2') ;
    end
    
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   ....Create standard_dev_mean_rad variable for ARM file compatibility.

nu     = 798:4:1402 ;
wnum2  = nu(1:length(nu)-1)+diff(nu)/2 ;  % as suggested by Dave Turner

% Channels 1 and 2
if exist([fname,'B' ichS '.CVS.nc'],'file') % ccox 11/23/2010 changed the names slightly to get the file...
    %sprintf('!! USING BACKWARD SCAN DIRECTION FOR VARIANCE !!')
    fp = rd_netcdf([fname,'B',ichS,'.CVS.nc']);
    
    %wnum2                 = fp.wnum1 ; % wnum1 for both channel 1 and 2!
    sceneMirrorAngle      = fp.sceneMirrorAngle ;
    VarianceOfRadiance    = fp.VarianceOfRadiance' ;
    hbb                   = find(sceneMirrorAngle == 60);  
    
    % INDEX TO HBB VIEWS
    timelen = size(netcdf.getVar(d,netcdf.inqVarID(d,'mean_rad')),2) ;
    standard_dev_mean_rad = zeros(timelen,length(wnum2));
    
elseif exist([fname,'f' ichS '_cvs.nc'],'file') 
    %sprintf('!! USING FORWARD SCAN DIRECTION FOR VARIANCE !!')
    fp = rd_netcdf([fname,'f',ichS,'_cvs.nc']);
        
    %wnum2                 = fp.wnum1 ;
    sceneMirrorAngle      = fp.sceneMirrorAngle ;
    VarianceOfRadiance    = fp.VarianceOfRadiance' ;
    hbb                   = find(sceneMirrorAngle == 60);  
    
    % INDEX TO HBB VIEWS
    timelen = size(netcdf.getVar(d,netcdf.inqVarID(d,'mean_rad')),2) ;
    standard_dev_mean_rad = zeros(timelen,length(wnum2));
    
else
   % error(['No cvs or uvs file found for channel ' ichS '.']);
   disp(['No cvs or uvs file found for channel ' ichS '. I cannot do std_dev_mean_rad!']); % lets not have a crash here for now... ccox 1/24/2011
end


% NOTES:
%   THE "VARIANCE" IS ACTUALLY A STANDARD DEVIATION SPECTRUM (SEE
%   AERICALCV.FOR)  BUT, ALSO FROM AERICALCV.FOR:
%C     DESCRIPTION:
%C          This program reads the AERI complex-spectra files,
%C          and converts the raw sky-view data values into radiance units.
%C          This is accomplished by using spectra obtained while viewing
%C          2 calibration blackbodies, one warm (heated) and the other
%C          cooler (at ambient temperature).  The "variance" files, which
%C          are actually "sums-of-squares" of the uncalibrated data values,
%C          are also converted to radiance units by using the responsivity
%C          determined from the blackbody views.
%
% Notes:
%   See metadata for an ARM AERI file called 
%       nsaaerich1C1.b1.20070901.000232.cdf 
%   on amundsen at 
%       /data/dataman/paeri/docs/Mfiles/QC/SSECtoARM/
%   'long_name' in the metadata states that: 
%      'standard_dev_mean_rad' is "Difference between the sky view''s 
%       radiance variance and the hot black body view''s variance"

% SHOULD WORK AS LONG AS WE HAVE A HBB SPECTRUM
% BEFORE THE FIRST SKY SPECTRUM
if exist('sceneMirrorAngle','var')
isky = find(sceneMirrorAngle == 0);  

% Pads isky in case PAERI software was terminated in the middle of
% sequence.
% added elseif for the opposite condition - ccox 2/3/12
if length(isky) < timelen 
    isky(length(isky)+1:timelen) = isky(length(isky));
elseif length(isky) > timelen 
   isky = isky(1:timelen);
end

% Change this from commented out lines below so that 
%   1) it has units of radiance and
%   2) it is the mean over bins of 4 cm-1 as requested by Dave Turner
% But note we need to ask Dave Turner about this if he starts using it.
% (He doesn't currently)
VarianceOfRadiance = VarianceOfRadiance';
sdev   = sqrt(VarianceOfRadiance(:,isky))' ; % will bomb if c1 and c2 differ?
for ind=1:length(wnum2)
    standard_dev_mean_rad(:,ind) = mean(sdev(:,(fp.wnum1>=nu(ind) & ...
        fp.wnum1<nu(ind+1)) ),2)';    % time x wnum2
end


 % NOW MAKE THE DEFINITIONS AND THE ASSIGNMENTS
 sdev_meanrad_longname = ['Difference between the sky view''s radiance' ...
                       ' variance and the hot black body view''s variance'];
 range_wnum2     = ['[',num2str(min(wnum2)),'  ',num2str(max(wnum2)),']'] ;

 % Set up variables for standard deviations
 netcdf.reDef(d);  % into define mode to put att
 did_w2  = netcdf.defDim(d,'wnum2', length(wnum2) ) ; 

 id_w2   = netcdf.defVar(d,'wnum2','float',netcdf.inqDimID(d,'wnum2')) ;
 id_sdev = netcdf.defVar(d,'standard_dev_mean_rad','float',[did_w2; did_time]) ; 

 netcdf.endDef(d)
 netcdf.putVar(d,id_w2,   wnum2);
 netcdf.putVar(d,id_sdev, standard_dev_mean_rad');  % wnum2 x time

 netcdf.reDef(d);  % into define mode to put att
 netcdf.putAtt(d,id_w2,'longname','Wave number in reciprocal centimeters');
 netcdf.putAtt(d,id_w2,'units','cm^-1');
 netcdf.putAtt(d,id_w2,'range_of_values', range_wnum2);
 netcdf.putAtt(d,id_w2,'independent_interval',num2str(wnum2(2)-wnum2(1)));

 netcdf.putAtt(d,id_sdev, 'long_name',sdev_meanrad_longname) ;
 netcdf.putAtt(d,id_sdev, 'units','(mW/(m^2 sr cm^-1))^2');
end
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   ....Save the netcdf files.
%
% close up
netcdf.close(fpc1)
netcdf.close(fpc2)
netcdf.close(fpsm)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

