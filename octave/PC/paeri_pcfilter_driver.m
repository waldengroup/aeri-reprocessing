function paeri_pcfilter_driver(DirQC,DirPCout,DirQCsum,begDate,endDate,chNo)
%
%       function paeri_pcfilter_driver(directory,PCdirectory)
%
%   ....This function reads in netcdf files for channel 1 (c1) for the PAERI and performs 
%       PC noise filtering on the spectra.  It uses the method describe
%       by:
%
%         Antonelli et al, 2004: A principal component noise filter for
%           high spectral resolution infrared measurements, J. Geophys. Res.,
%           109, D23102, doi:10.1029/2004JD00482.
%
%       and
%
%         Turner et al, 2006:  Noise reduction of Atmospheric Emitted
%         Radiance Interferometer (AERI) observations using principal
%         component analysis, J. Atmos. Ocean. Tech., 23, 1223-1238.
%
%       Input
%       -----
%       directory     - directory in which the PAERI QC data reside
%       PCdirectory  - directory where you want the PC filtered files to be written
%       filenamebeg  - beginning of file names to process, e.g. 'euraerich1.qc.07'
%
%       Example call to 'paeri_pcfilter_driver.m'
%       ---------------------------
%         paeri_pcfilter_driver('/data/dataman/paeri/data/QC/QC_checked/','/data/dataman/paeri/data/PC/');
%       
%       Written by Von P. Walden
%                  3 May 2007
%       Updated    5 May 2007 (Removed dependence on month; now processes everthing in the QC directory.)
%                 21 May 2007 (Now calculates how many files to read on the fly using nfreq.)
%		Updated   23 May 2009 (Modified by PMR, see initials within for specific changes)
%       Revised for use on data with RM applied (PMR), see details in "readme"
%       Updated 9 Dec: for use on either channel (previously on channel 1) 
%       Updated 9 Dec: Now skips missing files and reports the files skipped in a logfile
%
%       Updated 29 Dec: Modified for use with Summit data - ccox
%

pkg load netcdf
import_netcdf
addpath('/home/vonw/work/software/ICECAPS/AERI-reprocessing/octave/')
addpath('/home/vonw/work/software/ICECAPS/AERI-reprocessing/octave/PC/')

% Filenames
logfile = 'PCAlog';


% PCA start from 20060903 for channel 1
% PCA start from 20060622 for channel 2 

filenamebeg = 'smtaerisummaryC1.b1.';
% begDate = 20061230;
% endDate = 20090601;
% chNo    = 1   ;   % The desired channel number

% Open logfile for appending:
cd(DirPCout) ;
logfid = fopen(logfile,'a') ;
fprintf(logfid,['\nPerforming PCA for ' num2str(begDate) ' - ' num2str(endDate) '.\n']);

%   ....Determines the number of spectra to use in the PC noise filter based on Turner et al's 
%       recommendation of using at least twice the number of spectral points (about 2600).
%       The spectral limits are 490 to 1800 cm-1, and the spectral resolution is 0.5 cm-1.
%
%       Dave Turner actually recommended we use about 10,000 spectra instead of double the spectral limits.
%nspectra_limit = (1800-490)./0.5 * 2;
nspectra_limit = 10000;


%
%   ....Determines which files to open.  
%
d = dir([DirQCsum, filenamebeg '*.cdf']);

% Get only files between begDate and endDate
filesum = char(zeros(length(d),39));
filechn = char(zeros(length(d),36));%7+2+1+4+11+4)); 
%filechn = char(zeros(length(d),7+2+1+4+13+6)); % ccox 12/15/11 filenames should be YYYYMMDD.TTTTTT, not YYMMDD.TTTT 
fileout = char(zeros(length(d),36));%7+2+1+7+19));
count=0; irem = 0 ; removeme=[];
for id = 1:length(d)
    if ( (str2num(d(id).name(length(filenamebeg)+1:length(filenamebeg)+1+7)) >= begDate) && (str2num(d(id).name(length(filenamebeg)+1:length(filenamebeg)+1+7)) <= endDate) )
        count=count+1;
        filesum(count,1:39) = char(d(id).name);
        filechn(count,:) = [filesum(count,1:7) 'ch' num2str(chNo) '.rm_qc.'    filesum(count,21:35) '.cdf'];
        %filechn(count,:) = [filesum(count,1:7) 'ch' num2str(chNo) '.qc.'    filesum(count,15:29) '.cdf']; % ccox 12/15/11 filenames should be YYYYMMDD.TTTTTT, not YYMMDD.TTTT 
        fileout(count,:) = [filesum(count,1:7) 'ch' num2str(chNo) '.rm_pc.' filesum(count,21:35) '.cdf'];
        if exist([DirQC,filechn(count,:)],'file')~=2
            irem=irem+1;
            fprintf(logfid,[DirQC,filechn(count,:) ' not found, skipping.\n']);
            removeme(irem) = count ;
        endif
    endif
endfor
iends = count+1:size(filesum,1) ;
filesum([removeme iends],:)=[];
filechn([removeme iends],:)=[];
fileout([removeme iends],:)=[];

clear d

wnumCh   = ['wnum' num2str(chNo)];
SkyNENch = ['SkyNENCh' num2str(chNo)];

l = 1;

while l <= size(filesum,1)
    
    f            = l;   % File index used below to copy filtered data into files.

    c1t.Time     = [];
    c1t.wnum    = [];
    c1t.mean_rad = [];
    c1t.wnumChs   = [];
    c1t.SkyNEN   = [];
   
    nspectra = 0;
    sprintf('READING PAERI SPECTRA TO FILTER: \n') 
    fprintf(logfid,'READING PAERI SPECTRA TO FILTER: \n') ;
    try
        while nspectra < nspectra_limit
    
            % Change the filename (PMR)
            sm           = rd_netcdf([DirQCsum,filesum(l,:)]);
            c1           = rd_netcdf([DirQC,filechn(l,:)]);
            good         = find(c1.missingDataFlag == 0);
    
            if(length(good) > 1)
                c1t.wnum    = c1.wnum;
                c1t.Time     = [c1t.Time;     c1.Time(good)];
                c1t.mean_rad = [c1t.mean_rad; c1.mean_rad(:,good)'];
                c1t.wnumChs   = sm.(wnumCh) ;
                c1t.SkyNEN   = [c1t.SkyNEN;   sm.(SkyNENch)(:,good)'];
            endif

            nspectra     = nspectra + length(good);
            fprintf(logfid,'File read: %36s, # of good spectra: %6d, Total # of spectra read: %6d\n',filechn(l,:),length(good),nspectra);
            sprintf('File read: %36s, # of good spectra: %6d, Total # of spectra read: %6d\n',filechn(l,:),length(good),nspectra)

            l            = l + 1;
        
        endwhile
    
        % For testing...
        %c1p = c1t;
        
        
        %
        %   ....Perform the PC noise filtering.
        %
        sprintf(['Starting PCA NOISE FILTERING at ' datestr(now) '.\n'])
    
        [c1p.mean_rad,c1p.numberOfPCs] = paeri_pcfilter(c1t.wnum,c1t.mean_rad,c1t.wnumChs,c1t.SkyNEN);    
    
        sprintf(['Done at ' datestr(now) '.\n'])
    
        %
        %   ....Write PC noise-filtered data to new YYMMDD_qc_pc.nc files using .
        %
        sprintf('CREATING PC NOISE-FILTERED DATA FILES:\n')
        start       = 0;
        c1_mean_rad = [];
        for k = f:l-1
            %   ....Open file pointers to original netcdf files, but makes copies first!
            disp(fileout(k,:));

            % copy original QC file for channel 1 to output PC file name.
            copyfile([DirQC,filechn(k,:)],[DirPCout,fileout(k,:)]);
                
            fpc1 = netcdf.open( [DirPCout,fileout(k,:)],'NC_WRITE');

            c1                  = rd_netcdf([DirQC,filechn(k,:)]);
            c1_mean_rad         = c1.mean_rad';
            good                = find(c1.missingDataFlag == 0);

            if (length(good) > 0)
                c1_mean_rad(good,:) = c1p.mean_rad(start+(1:length(good)),:);
                start = start + length(good);
            endif

            varid = netcdf.inqVarID(fpc1,'mean_rad');
            netcdf.putVar(fpc1,varid,c1_mean_rad');
            netcdf.reDef(fpc1);   % Must enter define mode to add new variable.
            dimid = netcdf.inqDimID(fpc1,'scalar');
            varid = netcdf.defVar(fpc1,'numberOfPCs','double',dimid);
            netcdf.endDef(fpc1);  % Goes back to data mode to putVar.  
            netcdf.putVar(fpc1,varid,c1p.numberOfPCs);
            netcdf.reDef(fpc1);   % Must enter define mode to add new variable.
            netcdf.putAtt(fpc1,varid,  'long_name','Number of principal components (PC) in PC noise filtering') ;
            netcdf.endDef(fpc1);  % Goes back to data mode to putVar.                                   
            netcdf.close(fpc1);
    	
        endfor
    catch
    end_try_catch
    
    f = l; 

endwhile

fclose(logfid);



