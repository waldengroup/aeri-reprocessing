function out = dmvtocdf_any(dataDirectory,YYMMDD,filetypes)
%
%       function s = dmvtocdf_sum(dataDirectory,YYMMDD,filetypes)
%
%   ....This function drops into the PAERI directory structure and
%       converts the files indicated in the input to netcdf
%
%       Input
%       -----
%       dataDirectory  - string containing the data directory for the PAERI data.
%       YYMMDD         - string containing the YYMMDD, which is used to construct the data filenames.
%	filetypes      - cell array of file endings for desired output to be converted via dmvtocdf
%			 e.g. {'B1.CVS','B1.RLC','B2.RLC','C1.RFC'}
%
%       Output 
%       ------------------------
%	netcdf files with extension (filetypes(k)).nc in outDirectory
%
%
%       Written by Von P. Walden
%                  19 Aug 2003
%	Updated by Penny M. Rowe
%
%       Modified:
%                  19 Mar 2006 - Modified for use with data from Eureka.
%                  11 Sep 2006 - Adapted from dmvtocdf.m for only SUM files.
%                  30 Jan 2007 - Modified to use with new Eureka directory structure.
%	           03 Mar 2009 - Modified to do any desired type of file, as specified in input. (PMR)
%		   03 Mar 2009 - removed output (PMR)
%
%	Notes/Warnings:
%		Case must match that of files!
%		Currently no check that files exist, no error messages, it will just bomb if it doesn't work
%		Results will be put into data directory!
%               You must run this code from its location
%
%
%   ....Creates the paeri filename from YYMMDD.
%  
%

pkg load netcdf
import_netcdf

out = 0 ;
filename = YYMMDD;

%
%   ....Go to the appropriate data directory.
%
thisDir = pwd; thisDir(length(thisDir)+1)='/';


cd(dataDirectory);
%
%    Convert datafiles to netcdf using the Linux executable, dmvtocdf.
%
for k = 1:length(filetypes) ;
   fp = fopen('dmvtocdf.input','w');
   fprintf(fp,[filename,char(filetypes(k)),'\n']);    % input file
   fprintf(fp,'1\n');                                 % first input rec no
   fprintf(fp,'9999\n');                              % last input rec no
   fprintf(fp,'0\n') ;                                % scene types, 0=>all
   fprintf(fp,[filename,char(filetypes(k)),'.nc\n']); % output file
   fprintf(fp,[thisDir 'commentfile.txt\n']);         % comment file
   fprintf(fp,[thisDir 'def.txt\n']);                 % metadata confg file
   fprintf(fp,[thisDir 'log.txt\n']);                 % log file
   fclose(fp);
   eval(['!' thisDir 'dmvtocdf < dmvtocdf.input']);
   %   ....Clean up
   %!rm dmvtocdf.input
end

%
%   ....Go back to original directory.
%
cd(thisDir);

out = 1 ; 


