function [wnout] = getWN_ELW(wnin,WNLASR)
%
% ... Purpose: To correct the wavenumber vector in the
% intermediate PAERI data from the AERI processing, which has an error.
%
% Approximate runtime: fast
%
% Inputs:
%  wnin:    Input wavenumber vector (erroneous)
%  WNLASR: Effective laser wavenumber of the instrument
%
% Outputs:
%  wnout:   Output wavenumber vector (corrected)
%
% Notes: Based on zfli_mat.m, which is based on fortran code zfli.
%  The lines given in the comments refer to line numbers in zfli.
%
% Function calls: none
%
% P.M. Rowe
% Nov. 2010

% make sure inputs are double precision so outputs will be
wnin = double(wnin) ;
WNLASR = double(WNLASR) ;

% Go ...
npts  = length(wnin);

WNAc  = wnin(1) ;               % line 345: as in the code, but wrong here.
WNBc  = wnin(npts) ;            % line 346
dwnc  = (WNBc-WNAc)/(npts-1) ;    % line 347

% Note: the next three lines are not in the code:
% Note: I can't find
WNA  = wnin(1)*WNLASR/16384/2 / dwnc ; %xin(1)*(1+eps) ;    % 491.80356 ; %
WNB  = wnin(npts)*WNLASR/16384/2 / dwnc ; %xin(npts)*(1+eps) ;    % 1799.90454; %

% Go on
dwn  = (WNB-WNA)/(npts-1) ;   % line 347


wnout = WNA:dwn:WNB ;

