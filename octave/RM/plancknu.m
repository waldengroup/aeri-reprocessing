function f = plancknu(nu_icm,T);

% function f = plancknu(nu_icm,T);
%
% spectral Planck function as function of wavenumbers (cm-1)
%
% [h]    = J*s
% [c]    = m/s
% [cbar] = cm/s
% [k]    = J*K-1
%
%
%    Note: LBLRTM uses ...
%c    Constants from NIST 01/11/2002
%
%      DATA PLANCK / 6.62606876E-27 /, BOLTZ  / 1.3806503E-16 /,
%     *     CLIGHT / 2.99792458E+10 /, 
%
%    Update 5-31-12: new constants, CODATA 2010 -ccox

% old constants, see updates
%h    = 6.62606896e-34;				% J s;  CODATA 2006
%c    = 2.99792458e8;				% m/s;  NIST
%k    = 1.3806504e-23;				% J K-1; CODATA 2006

h    = 6.62606957e-34;				% J s;  CODATA 2010
c    = 2.99792458e8;				% m/s;  NIST
k    = 1.3806488e-23;				% J K-1; CODATA 2010

cbar = 100*c;			  		% cm/s

top    = 2 * h * cbar^3 .* nu_icm.^3;
bottom =  c^2*  ( exp(h*cbar*nu_icm./(k*T))-1 );
f      = cbar*top./bottom;


% now set f to zero for wavenumbers less than or equal to 0
indzero = find(nu_icm<=0);
f(indzero)=0;


%[B]= cm*s-1*J*s*cm3*s-3*cm-3*m-2*s2
%[B]= W m-2 cm



	