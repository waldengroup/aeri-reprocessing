function AERI_process_amun_short(logDirectory,begDate,chNo,nulo,nuhi,LASERwavenumberCh1,LASERwavenumberCh2,cavfactor,FFOVhalfAngle)

% Script: AERI_process_amun.m
%
% Purpose: To repeat the AERI processing in matlab, starting from the
%   CXV (for channel 1, nlapp for detector nonlinearity is corrected)
%   or CXS files (for channel 2, no nlap).  The code is set up to do
%   aericalv, ffovcmr, and scndirav, but not all are currently "on",
%   since the immediate purpose is to get the instrument response.
%
% Notes:
%   - The final QC'd and PCA'd PAERI data is also loaded, mainly to get
%     BB temperatures, which are not recalculated here.
%   - Two errors can be turned on or off here: the errors in the FFOV
%     correction and the uncalibrated-blackbody-spectrum weighting
%   - instrument response saved as cdf file
%   - ratio of uncalibrated spectral differences saved as cdf file
%   - channel 2 not yet implemented but some code in place
%   - Will skip any days with directories that are not in form AEYYMMDD
%     future version will include these.
%   - calibrated forward and backward spectra calculated but not saved
%   - SCNDIRAV not performed: see code after "return" statement
%   - FFOVCMR not performed: see code after "return" statement
%
%
% Input variables:
%   begDate: beginning date, in form 20090308
%            (endDate will be same)
%   chNo:    The desired channel number
%
% Input Files and Directories (hardwired below)
%   DirQC:        Dir of "QC" or template files. Beginning given by qcStr
%   DirRawPaeri:  The CXV (ch. 1) and CXS (ch. 2) files *must* be
%                 in this dir within subdirs of the form AEYYYYMMDD/,
%                 otherwise you need to modify the code.
%   locStr/qcStr: Specifiy the beginning of the quality control (QC)
%                 filename. The QC file is used as a template for the
%                 output file and thus should have the desired fields,
%                 otherwise those fields will need to be added manually.
%                 For example *c1.nc and *c2.nc files created by standard
%                 processing.
%
%
% Output Files and where they are saved:
%   DirR/fileNCout_ratio   : ratio of uncal spec
%   DirR/fileNCout_r       : reponsivity
%   DirRlc/fileNCout_rlcFB : cal sky spec, for/back
%   DirRlc/fileNCout_rlc   : same, but aved over for/back
%   DirR/fileNCout_RM      : after RM
%   DirNus/fileNu          : wavenums where accurate calibration not
%                            possible, id by responsivity criterion
%   DirT/fileTa            : ambient Temp, determined from ch. 1 rad,
%                            needed for ch. 2
%   DirRM/fileNCout_bbE    : Final File! after calib, ave, RM, ffov,
%                            and BBemissCorr
%
% Penny M. Rowe
% 23 Sept. 2009
%
% Updates:
%  Various, by Chris Cox (see initials within)
%  Cleaned up comments, etc, Penny M. Rowe, 23 Dec. 2011
%  Shortened by removing unneeded options, to make this a streamlined
%    version for processing spectra only, not for comparing to standard
%    AERI processing output or exploring errors (for that, see the code
%    AERI_process_amun1_new.m) Penny M. Rowe, 2 Jan 2012
%    - flagAERIcalv removed: will always calibrate spectra
%    - flagScnDirAve removed: will always average mirror direction-spectra
%    - flagCompRlc removed: will not compare calib sky spec. to "c#.RLC"
%    - flagStandRlc removed: applies responsivity criterion to
%      rlc-like spectra created here
%    - flagRM removed: will apply responsivity criterion
%    - flagFFOVcmr removed: Do FFOVCMR: apply FFOV correction
%    - flagFFOVerr removed: errors in FFOV calculation not included
%    - flagBBwtErr removed: errors in blackbody weighting not included
%    - uses "continue" rather than "skipIt=1" with if statements generally
%    - changed variable dimid_wnum to did_nu and dimid_time to did_t
%    - fileInRLC is no longer needed

pkg load netcdf
import_netcdf

% ... Paths for the code needed here and for dmvtocdf
%addpath /data2/fieldExperiments/summit/Mfiles/MatlabProcessingCode/RM
%addpath /data/dataman/paeri/docs/Software/dmvtocdf/


% ... Input Directories
DirBeg = '/data/vonw/fieldExperiments/summit/data/paeri/';
DirQC       = [DirBeg 'QC/'];
DirRawPaeri = [DirBeg 'reprocessed/'];


% ... Output Directories
DirRM       = [DirBeg 'QC/'];
DirR        = [DirRM 'instrumentR/'];
DirRlc      = [DirRM 'rlc_rm/'];
DirT        = [DirRM 'Tamb/'];
DirNus      = [DirRM 'nus/'];


% ... Log file
cd(DirRM)
fid1 = fopen([logDirectory,'aeri_proc_amun_short_logfile'],'a');
fprintf(fid1,['Working on channel number ' num2str(chNo) '\n']);


% ... Parameters for naming convention
locStr  = 'smtaerich';
qcStr   = [locStr num2str(chNo) '.qc'];


% ... Adjustable parameters (SIP files parameters)
% These value are now passed to this function from driver -ccox (oct'12)
%nuloCh1            = 492 ;
%nuhiCh1            = 1800;
%nuloCh2            = 1780;
%nuhiCh2            = 3000;
%FFOVhalfAngle      = FFOVhalfAngle;
%cavfactor          = cavfactor;
%LASERwavenumberCh1 = LASERwavenumberCh1 ;
%LASERwavenumberCh2 = LASERwavenumberCh2 ;

% ... Adjustable Parameters to set for RM (instead of ratfac or ratio test)
nuloRMch1 = [400  460  ] ;  % The wavenumber ranges to get the noise in r
nuhiRMch1 = [1845 1920 ] ;
nuloRMch2 = [1600 1636];
nuhiRMch2 = [2900 3000];
%RespAveTime = 0.5 ; % (hr; time for getting st dev of responsivity)
Nstands = 6 ;

% ... Flags for codes to run
flagBBE       = 0 ;  % 1=> Do BB Emissivity correction
imodelBBE     = 0 ;  % 0=> get BB emissivity the old way (fixed later) :: 0 = scaler cav factor, 9 = wnum dep cav factor. D. Turner recommends scaler of 39 on 5/29/12. -ccox

% ... Flags for files to save and log info
flagSaveRLCfb = 0 ;  % 1 => Save RLC forward and backward files
flagSaveRLC   = 0 ;  % 1 => Save RLC averaged files
flagSaveResp  = 0 ;  % 1 => Save instrument response to directory DirR
flagSaveRatio = 0 ;  % 1 => Save ratio of uncal. spectra to dir. DirR
flagSaveRM    = 0 ;  % 1 => Save spectra after r criterion (before ffov)


endDate = begDate ;


% ... Channel-dependent variables
if chNo==1
  WNB  = 2000;
  XMAX = 1.04;
  filetype = 'CXV';
 % nulo = nuloCh1 ;
 % nuhi = nuhiCh1 ;
  WNLASR_sip = LASERwavenumberCh1 ;
elseif chNo==2
  filetype = 'CXS';
  warning('I hope I do not need WNB or XMAX');
  WNB=NaN;
  XMAX=NaN;
 % nulo = nuloCh2 ;
 % nuhi = nuhiCh2 ;
  WNLASR_sip = LASERwavenumberCh2 ;
end

bStr = [locStr num2str(chNo) '.'];

% ... Get all filenames in the QC folder
cd(DirQC)
dirFiles=dir('*.cdf') ;

% ... Loop over all filenames to get the PAERI QC files for the given year
count=0; clear filenames
for ifile=1:length(dirFiles)
  
  fname = char(dirFiles(ifile).name)  ;
  if length(fname)>30 && strcmpi(fname(1:length(bStr)),bStr) && ...
      str2double(fname(length(bStr)+4:length(bStr)+4+7)) >= begDate && ...
      str2double(fname(length(bStr)+4:length(bStr)+4+7)) <= endDate
    % changed final "8" to "7" to avoid the "." at the end, PMR 6 Jan 2012
    count = count+1;
    filenames(count)={fname};
  end
end





% ... BEGIN CODE
fprintf(fid1,['Repeat AERI processing for ' num2str(begDate) '.\n']);


% ... Loop over all files for given year
for ifile=1:length(filenames)
  fname = char(filenames(ifile));
  
  % Day and time
  i1 = strfind(fname,'20')  ;  % changed findstr to strfind 2012 Jan 18 PMR
  YYYYMMDD = fname(i1(1):i1(1)+7) ;
  YYMMDD   = YYYYMMDD(3:8);
  hhmmss   = fname(i1(1)+7+2:i1(1)+7+2+5) ;
  hhmm     = hhmmss(1:4);
  
  i2 = find(fname=='.');
  if ~strcmpi(fname(1:i2(1)),bStr)
    fprintf(fid1,'Error:Is the naming correction right? \n');
    error('Is the naming correction right?');
  end
  
  % ... Set all the input file names
  fileInQC        = [qcStr '.' YYYYMMDD '.' hhmmss '.cdf'] ;
  
  % ... Set all the output file names (whether they are saved is
  %     controlled by flags)
  fileNCout_r     = [bStr 'r.'      YYYYMMDD '.' hhmmss '.nc'] ;
  fileNCout_rlcFB = [bStr 'rlcFB.'  YYYYMMDD '.' hhmmss '.nc'] ;
  fileNCout_rlc   = [bStr 'rlc.'  YYYYMMDD '.' hhmmss '.nc'] ;
  fileNCout_RM    = [bStr 'rlc_rm.' YYYYMMDD '.' hhmmss '.nc'] ;
  fileNCout_bbE   = [bStr 'rm_qc.'     YYYYMMDD '.' hhmmss '.cdf'] ;
  fileNu          = [bStr 'nus.'    YYYYMMDD '.' hhmmss '.txt'];
  fileTa          = [locStr '1.Tamb.'   YYYYMMDD '.' hhmmss '.txt'];
  
  
  
  % Previously, this loop encompassed all the code down to just before the
  % last end (before the fprintf statement, ~to line 718)
  % i.e. the "continue; end" was an "else".  PMR 2 Jan 2012
  if ~strcmp(fileInQC,fname)
    fprintf(fid1,['Skipping ' fname ' because it is not in the format '...
      'expected (' fileInQC '). \n']);
    continue
  end
  
  % ... Load the QC so we can find the index to the good spectra, ikeep
  %     and the first time
  cd(DirQC)
  qc    = rd_netcdf(fileInQC);
  ikeep = find(qc.missingDataFlag==0);
  %if isempty(ikeep)
  % No good records for this day, but do it anyway so we will have a file.
  %fprintf(fid1,['All records have been qc''d out for ' fname ...
  %  ', so skipping. \n']);
  % continue
  %end
  
  fprintf(fid1,['Performing AERICALV on ' fname ' \n']);
  
  % ... Get the wavenumber and temperature vectors
  % nu = qc.wnum1;  % This is never used (PMR 2 Jan 2012)
  Th = qc.calibrationHBBtemp ;
  Tc = qc.calibrationCBBtemp ;
  Ta = qc.calibrationAmbientTemp ;
  
  % Make a correction to these temperatures, if necessary (24 June 2010
  % through 11 July 2011 taken at Summit Station, Greenland when the mandb 
  % parameter file was filled with zeros).  Uses Tcorrection.m, which is
  % based on python code written by D. Hackel.
  if str2num(YYYYMMDD) >= 20100624 && str2num(YYYYMMDD) <= 20110711
      % Make structure of thermistor temps
      Tabb.top    = qc.ABBtopTemp;
      Tabb.apex   = qc.ABBapexTemp;
      Tabb.bottom = qc.ABBbottomTemp;
      Thbb.top    = qc.HBBtopTemp;
      Thbb.apex   = qc.HBBapexTemp;
      Thbb.bottom = qc.HBBbottomTemp;
      % Apply correction to the temps
      [Tc1, Th1] = Tcorrection(Tabb,Thbb);
      % Subtract off the uncorrected temps to get the correction
      Tc1.top    = Tc1.top-qc.ABBtopTemp;
      Tc1.apex   = Tc1.apex-qc.ABBapexTemp;
      Tc1.bottom = Tc1.bottom-qc.ABBbottomTemp;
      Th1.top    = Th1.top-qc.HBBtopTemp;
      Th1.apex   = Th1.apex-qc.HBBapexTemp;
      Th1.bottom = Th1.bottom-qc.HBBbottomTemp;
      % Get the calibrationBB corrections using the weighted mean
      calibTcCorr = Tc1.bottom.*qc.ABBbottomTempWeight + ...
                    Tc1.top.*qc.ABBtopTempWeight + ...
                    Tc1.apex.*qc.ABBapexTempWeight;  
      calibThCorr = Th1.bottom.*qc.HBBbottomTempWeight + ...
                    Th1.top.*qc.HBBtopTempWeight + ...
                    Th1.apex.*qc.HBBapexTempWeight;
      % Add calibrated correction back to calibration temps
      Tc = Tc + calibTcCorr;
      Th = Th + calibThCorr;
  end
  
  DirCXV = [DirRawPaeri 'AE' YYMMDD '/'];
  
  % ... Try getting the CXV files
  try
    fprintf(fid1,'Looking for CXV files. \n');
    % ... try to cd to directory, check for file, and use dmvtocdf
    
    if exist(DirCXV,'dir')==7
      cd(DirCXV)
      if strcmpi(filetype,'cxv')
        % ... Will need to create cxv nc file if it is not there
        fileCXVf = [YYMMDD 'F' num2str(chNo) '.CXV.nc'];
        fileCXVb = [YYMMDD 'B' num2str(chNo) '.CXV.nc'];
        if exist(fileCXVf,'file')~=2
          dmvtocdf_any(DirCXV,YYMMDD,{'B1.CXV','F1.CXV'});
        end
        if exist(fileCXVb,'file')~=2
          dmvtocdf_any(DirCXV,YYMMDD,{'B1.CXV'});
        end
        
        cxvf = rd_netcdf(fileCXVf);
        cxvb = rd_netcdf(fileCXVb);
      elseif strcmpi(filetype,'cxs')
        % ... Will need to create cxs nc file if it is not there
        fileCXVf = [YYMMDD 'F' num2str(chNo) '.CXS.nc'];
        fileCXVb = [YYMMDD 'B' num2str(chNo) '.CXS.nc'];
        if exist(fileCXVf,'file')~=2
          dmvtocdf_any(DirCXV,YYMMDD,{['B' num2str(chNo) '.CXS'],...
            ['F' num2str(chNo) '.CXS']});
        end
        if exist(fileCXVb,'file')~=2
          dmvtocdf_any(DirCXV,YYMMDD,{['B' num2str(chNo) '.CXS']});
        end
        
        cxvf = rd_netcdf(fileCXVf);
        cxvb = rd_netcdf(fileCXVb);
      end
      
    end
    
    % ... If the directory was not found above, or
    % ... Check the times on the QC file and the CXV file to make sure
    %     they match.  If not, try a different directory
    if exist(DirCXV,'file')~=7 || cxvf.Time(3) ~= qc.Time(1)
      % Get all possible directory names
      dirnamesCXV = dir([DirCXV(1:length(DirCXV)-1) '*']);
      % Loop over them
      timeisnotright=1;
      itry = 0 ; maxtries=length(dirnamesCXV);
      skipIt = 0 ;  % PMR 2 Jan 2012
      while timeisnotright && itry<maxtries
        itry = itry+1;
        DirCXV = [DirRawPaeri char(dirnamesCXV(itry).name)] ;
        
        cd(DirCXV);
        if strcmpi(filetype,'cxv')
          % ... Will need to create cxv file if it is not there
          fileCXVf = [YYMMDD 'F' num2str(chNo) '.CXV.nc'];
          fileCXVb = [YYMMDD 'B' num2str(chNo) '.CXV.nc'];
          if exist(fileCXVf,'file')~=2
            dmvtocdf_any(DirCXV,YYMMDD,{'B1.CXV','F1.CXV'});
          end
          if exist(fileCXVb,'file')~=2
            dmvtocdf_any(DirCXV,YYMMDD,{'B1.CXV'});
          end
          
          cxvf = rd_netcdf(fileCXVf);
          cxvb = rd_netcdf(fileCXVb);
        elseif strcmpi(filetype,'cxs')
          error('Modify code to include options for channel 2');
          %cxvf = rd_netcdf([YYMMDD 'f' num2str(chNo) '.cxs.nc']);
          %cxvb = rd_netcdf([YYMMDD 'b' num2str(chNo) '.cxs.nc']);
        end
        
        if cxvf.Time(3) == qc.Time(1)
          timeisnotright=0;  % got it, quit
        elseif itry==maxtries
          fprintf(['I have tried every directory for this day ' ...
            'but I cannot find a time matching the first in the ' ...
            'qc file, so I am skipping this day.']);
          skipIt = 1 ;
        end
        
      end % of while loop over checking possible day directores
      if skipIt; continue; end % (PMR 2 Jan 2012)
    end % of loop over checking if qc file and cxv file times match
    
    
  catch
    % ... If we fail in any of the attempts:
    %     to cd to directory, check for file, or use dmvtocdf
    fprintf(fid1,'Skipping because trouble finding/reading CXV files.');
    continue   % instead of setting skipIt to 1 (PMR 2 Jan 2012)
  end           % of try/catch loop
  
  
  fprintf(fid1,'Getting the instrument response and ratio.\n');
    
    % Make important cxv vars double
  if chNo == 1
    cxvf.RealPartCounts = double(cxvf.RealPartCounts);
    cxvf.ImagPartCounts = double(cxvf.ImagPartCounts);
    cxvb.RealPartCounts = double(cxvb.RealPartCounts);
    cxvb.ImagPartCounts = double(cxvb.ImagPartCounts); 
  elseif chNo == 2
    cxvf.Ch2ForwardScanRealPartCounts = double(cxvf.Ch2ForwardScanRealPartCounts);
    cxvf.Ch2ForwardScanImagPartCounts = double(cxvf.Ch2ForwardScanImagPartCounts);
    cxvb.Ch2BackwardScanRealPartCounts = double(cxvb.Ch2BackwardScanRealPartCounts);
    cxvb.Ch2BackwardScanImagPartCounts = double(cxvb.Ch2BackwardScanImagPartCounts);
  end
  cxvf.Time           = double(cxvf.Time);
  cxvb.Time           = double(cxvb.Time);
  
  % ... Fix wavenumber vector in cxvf.wnum1
  cxvf.wnum1 = getWN_ELW(cxvf.wnum1,WNLASR_sip)' ;
  % ccox transposed output 12/14/11
  cxvb.wnum1 = getWN_ELW(cxvb.wnum1,WNLASR_sip)' ;
  % added the above line for consistency (PMR 2 Jan 2012)
  
  % ... Do the AERI calibration and get ratio and r
  [L,r,ratio,wtH1,wtC1,wtH2,wtC2,TimeL] = doAericalv(cxvf,cxvb,...
    chNo,filetype,cavfactor,Th,Tc,Ta,0,imodelBBE,1);
  
  % All following instances of cxvf.wnum1 changed to nuL, PMR 2 Jan 2012
  nuL   = cxvf.wnum1 ;
  nuR   = cxvf.wnum1 ;
  Nnus  = length(nuL);
  Ntime = size(r.f,2) ;
  
  
  % ... Note: it takes 20 s to save the response file as a '.mat'
  %     file but only 2 s to save it as a netcdf file!
  
  
  % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %
  if flagSaveResp
    % ... Save the instrument response
    fprintf(fid1,'Saving the instrument response.\n');
    
    cd(DirR)
    ncid = netcdf.create(fileNCout_r,'NC_WRITE');    
    
    did_t = netcdf.defDim(ncid,'Time',Ntime) ;
    did_nu = netcdf.defDim(ncid,'wnum',Nnus) ;
    
    % Define a new variable in the file.
    varid1 =netcdf.defVar(ncid,'wnum','double',did_nu) ;
    varid2 =netcdf.defVar(ncid,'forwardResponse','double',[did_nu,did_t]);
    varid3 =netcdf.defVar(ncid,'backwardResponse','double',[did_nu,did_t]);
    
    % Leave define mode and enter data mode to write data.
    netcdf.endDef(ncid) ;
    
    % Write data to variable.
    netcdf.putVar(ncid,varid1,nuR) ;
    netcdf.putVar(ncid,varid2,r.f) ;
    netcdf.putVar(ncid,varid3,r.b) ;
    
    netcdf.close(ncid);
  end
  
  
  
  % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %
  if flagSaveRatio
    % ... Save the ratio
    fprintf(fid1,'Saving the instrument ratio.\n');
    
    cd(DirR)
    ncid = netcdf.create(fileNCout_ratio,'NC_WRITE');
    
    did_t = netcdf.defDim(ncid,'Time',Ntime) ;
    did_nu = netcdf.defDim(ncid,'wnum',Nnus) ;
    
    % Define a new variable in the file.
    varid1 = netcdf.defVar(ncid,'wnum','double',did_nu) ;
    varid2 = netcdf.defVar(ncid,'forwardRatio','double',[did_nu,did_t]) ;
    varid3 = netcdf.defVar(ncid,'backwardRatio','double',[did_nu,did_t]) ;
    
    % Leave define mode and enter data mode to write data.
    netcdf.endDef(ncid) ;
    
    % Write data to variable.
    netcdf.putVar(ncid,varid1,nuR) ;
    netcdf.putVar(ncid,varid2,ratio.f) ;
    netcdf.putVar(ncid,varid3,ratio.b) ;
    
    netcdf.close(ncid);
  end
  
  
  
  % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %
  if flagSaveRLCfb
    % ... Save the forward and backward spectra with no ratfac or RM
    %     correction
    ncid = netcdf.create(fileNCout_rlcFB,'NC_WRITE');
    
    did_t = netcdf.defDim(ncid,'Time',Ntime) ;
    did_nu = netcdf.defDim(ncid,'wnum',Nnus) ;
    
    % Define a new variable in the file.
    varid1 = netcdf.defVar(ncid,'wnum','double',did_nu) ;
    varid2 = netcdf.defVar(ncid,'forwardAtmosphericRadiance',...
      'double',[did_nu,did_t]) ;
    varid3 = netcdf.defVar(ncid,'backwardAtmosphericRadiance',...
      'double',[did_nu,did_t]) ;
    varid4 = netcdf.defVar(ncid,'weight1HBB','double',did_t) ;
    varid5 = netcdf.defVar(ncid,'weight2HBB','double',did_t) ;
    varid6 = netcdf.defVar(ncid,'weight1CBB','double',did_t) ;
    varid7 = netcdf.defVar(ncid,'weight2CBB','double',did_t) ;
    
    % Leave define mode and enter data mode to write data.
    netcdf.endDef(ncid) ;
    
    % Write data to variable.
    netcdf.putVar(ncid,varid1,nuL) ;
    netcdf.putVar(ncid,varid2,L.f) ;
    netcdf.putVar(ncid,varid3,L.b) ;
    
    netcdf.putVar(ncid,varid4,wtH1);
    netcdf.putVar(ncid,varid5,wtC1);
    netcdf.putVar(ncid,varid6,wtH2);
    netcdf.putVar(ncid,varid7,wtC2);
    
    netcdf.close(ncid);
  end
  
  
  
  % ... SCNDIRAV: average forward and backward calibrated spectra
  radL = .5*real(L.f) + .5*real(L.b) ;   % Calibrated radiance
  
  % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %
  
  
  % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %
  if flagSaveRLC
    % ... Save the scndiraved spectra with no ratfac or RM correction
    cd(DirRlc) ;
    ncid = netcdf.create(fileNCout_rlc,'NC_WRITE');
    
    did_t = netcdf.defDim(ncid,'Time',Ntime) ;
    did_nu = netcdf.defDim(ncid,'wnum',Nnus) ;
    
    % Define a new variable in the file.
    varid1 = netcdf.defVar(ncid,'wnum','double',did_nu) ;
    varid2 = netcdf.defVar(ncid,'averageRadiance','double',[did_nu,did_t]);
    varid4 = netcdf.defVar(ncid,'weight1HBB','double',did_t) ;
    varid5 = netcdf.defVar(ncid,'weight2HBB','double',did_t) ;
    varid6 = netcdf.defVar(ncid,'weight1CBB','double',did_t) ;
    varid7 = netcdf.defVar(ncid,'weight2CBB','double',did_t) ;
    
    % Leave define mode and enter data mode to write data.
    netcdf.endDef(ncid) ;
    
    % Write data to variable.
    netcdf.putVar(ncid,varid1,nuL) ;
    netcdf.putVar(ncid,varid2,radL) ;
    
    netcdf.putVar(ncid,varid4,wtH1);
    netcdf.putVar(ncid,varid5,wtC1);
    netcdf.putVar(ncid,varid6,wtH2);
    netcdf.putVar(ncid,varid7,wtC2);
    
    netcdf.close(ncid);
  end
  
  % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %
  
  
  
  % ... Apply responsivity criterion and replace frequencies where
  %     calibration is impossible
  fprintf(fid1,['Performing RM on ' YYYYMMDD ' at ' hhmmss '.\n']);
    
  if chNo==1
    
      % ccox commented out this error 1/2/12. i dont think it causes any
      % problems not to have it here. it does cause problems to have it for
      % cases where the time dimension was changed in paeriQC_ext_v2 to
      % make c1, c2, and sum files consistent when the end-of-day restart
      % resulted in multiple file time vector lenghts. 
    %  Quality control
%     if length(qc.Time)~=length(TimeL) || sum(qc.Time-TimeL)~=0
%       error(['Times in template or QC file do not match ' ...
%         'times in cxvf & cxvb files.']);
%       % ccox fixed typos in the variables 12/14/11
%     end
%     
    % Perform the RM on the rlc-type data (created here)!
%     [radL,nubads,Tamb] = applyRMv2( nuR,r.f,r.b,...
%       nuL,TimeL,radL, ikeep,RespAveTime,Nstands) ;
    [radL,nubads,Tamb] = applyRM( nuR,r.f,r.b,...
      nuL,TimeL,radL,ikeep,nuloRMch1,nuhiRMch1,Nstands) ;
    % ccox changed "nup" to "rlc.wnum1" in 2nd nu spot
    % like Ch2 below 12/14/11.
    %
    % Note: Here nuR is cxvf.wnum1, the wavenumber corresponding to
    % the responsivities r.f and r.b, determined from aericalv, using cxvf
    % (cxvf.wnum1 was modified by getWN_ELW).
    % The second nu (rlc.wnum1) was changed ot nuL, also cxvf.wnum1.  It 
    % corresponds to radL; rlc.wnum1 does not necessarily, while for this
    % shortened version of AERI_process_amun, radL should be a function of
    % cxvf.wnum1. Likewise, changed rlc.Time to TimeL. (PMR 2 Jan 2012)
    
    
    % Save Tamb
    cd(DirT);
    save(fileTa,'-ascii','Tamb');
    
  elseif chNo==2
    % Tamb must already be there, so check and if so, load
    cd(DirT)
    if exist(fileTa,'file')~=2
      % No channel 2, check if this is because no QC for channel 1
      cd(DirQC)
      fileInQC1 = fileInQC; ich = strfind(fileInQC,'ch2.') ;
      % changed findstr to strfind 2012 Jan 18 PMR
      fileInQC1(ich:ich+3) = 'ch1.' ;
      if exist(fileInQC1,'file')
        fprintf(fid1,['Skipping ' fileInQC '. Do channel 1 to ' ...
          'get Tamb first. ' fileInQC1 ' exists.']);
      else
        fprintf(fid1,['Skipping ' fileInQC ' because '...
          fileInQC1 ' does not exist.']);
      end
      continue ;  % skip this day and move on
      % instead of setting skipIt to 1 (PMR 2 Jan 2012)
    else
      
      Tamb = load(fileTa);
      
      % Apply the responsivity criterion to the new paeri output
      % rlc files (created here)!
   %   [radL,nubads] = applyRMch2v2( nuR,r.f,r.b, nuL,TimeL,radL, ...
   %     ikeep,RespAveTime,Nstands,Tamb) ;
      [radL,nubads] = applyRMch2( nuR,r.f,r.b, nuL,TimeL,radL, ...
         ikeep,nuloRMch2,nuhiRMch2,Nstands,Tamb) ;
      % nuR and nuL are both set equal to cxvf.wnum1 in this short version
      % Changed rlc.wnum1 to nuL, because this needs to correspond
      % to radL and rlc.wnum1 does not necessarily.  Likewise,
      % change rlc.Time to TimeL (PMR 2 Jan 2012)
      
      
    end % for if Tamb exists
  end % of loop over channel number
  
  
  % Save nubads
  cd(DirNus);
  save(fileNu,'-ascii','nubads')
  
  
  % Chop the ends off the radiances
  [nulop,i1] = min(abs(nuL-nulo));
  [nuhip,i2] = min(abs(nuL-nuhi));
  
  Lsky   = radL(i1:i2,:);
  nuchop = nuL(i1:i2);
  
  % QC
  if length(nuchop)~=length(qc.wnum) % || sum(nuchop-qc.wnum1)~=0
    error('My wavenumber vector disagrees with that in qc file!');
    % ccox commented out "sum" part because nuL is now changed
    % in getWN_ELW.m 12/14/11
  end
  if size(Lsky)~=size(qc.mean_rad)
    error('My radiance array dims do not agree with those in qc file!');
  end
  

  % % % % % % % % % % % % % % % % % % %  % % % %% % % % % % % %
  if flagSaveRM
  % ... Save the radiances after RM

  % Copy the netcdf file to the new location/filename
  cd(DirQC);
  %copyfile(fileInQC,[DirR fileNCout_RM],'f');  % QC file is template
  %eval(['! cp -f ',fileInQC,' ',[DirR fileNCout_RM]])
  system(['cp -f ',fileInQC,' ',[DirR fileNCout_RM]]) ; % For compatibility with octave, vpw, 140911

  % Open the new RM file
  cd(DirR)
  ncid = netcdf.open(fileNCout_RM,'NC_WRITE');
  did_t = netcdf.inqDimID(ncid,'time') ;
  
  % Replace paeri.(varnameRad) with rad
  varid = netcdf.inqVarID(ncid,'mean_rad');
  
  % Enter define mode and enter data mode to write data.
  
  % Define Tamb
  netcdf.reDef(ncid) ;
  varid_time = netcdf.defVar(ncid,'BTambient','double',did_t) ;
  
  
  % Leave define mode and enter data mode to write data.
  netcdf.endDef(ncid) ;
  
  [timename,timelen] = netcdf.inqDim(ncid,did_t) ;
  
  % Write data to variable.
  netcdf.putVar(ncid,varid,Lsky) ;
  netcdf.putVar(ncid,varid_time,Tamb(1:timelen));
  
  % Close it
  netcdf.close(ncid);
  
  end
  % % % % % % % % % % % % % % % % % % %  % % % %% % % % % % % %
  
  
  % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %
  % ... Get FFOV correction, *without* error 2
  
  [Lffov,wnp,deltaC1,deltaC2] = ffovcmr_v2(...
    nuchop,Lsky(:,ikeep),FFOVhalfAngle,0,XMAX,WNB);
  Lsky(:,ikeep)=Lffov ;
  
  fprintf(fid1,'I did ffovcmr, but I am not saving the result.\n');
    
  
  % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %
  % ... Blackbody emissivity correction
  % This should be removed, and instead imodelBBE should be set
  % to option 9.
  
  if flagBBE
    
    Lsky = BBemissCorr(nuchop, Lsky, cavfactor,...
      qc.calibrationHBBtemp,...
      qc.calibrationCBBtemp,...
      qc.calibrationAmbientTemp)';
  end    % of if flagBBE statement  
 
  % Replace the QC'd out times with -9999
  % % % % % % % % % % % % % % % % % % %  % % % % % % % % % % % %
  % ... Save the rlc, scndirav, rm, rfc, ffov, bbe files
  iout = setdiff(1:size(Lsky,2),ikeep) ;
  Lsky(:,iout) = -9999 ; 

  % Copy the netcdf file to the new location/filename
  %copyfile([DirQC fileInQC],[DirRM fileNCout_bbE],'f');
  %eval(['! cp -f ',[DirQC fileInQC],' ',[DirRM fileNCout_bbE]])
  system(['cp -f ',[DirQC fileInQC],' ',[DirRM fileNCout_bbE]]) ;  % For compatibility with octave, vpw, 140911

  % Open the new RM file
  cd(DirRM)
  ncid = netcdf.open(fileNCout_bbE,'NC_WRITE');

fprintf(fid1, 'Got here - Opened rm_qc file.\n')
fclose(fid1)
fid1 = fopen([logDirectory,'aeri_proc_amun_short_logfile'],'a');

  % Replace paeri.(varnameRad) with rad and the corrected wnum
  varid  = netcdf.inqVarID(ncid,'mean_rad');
  varid2 = netcdf.inqVarID(ncid,'wnum');

fprintf(fid1, 'Got here - Before writing wnum and mean_rad\n')
fclose(fid1)
fid1 = fopen([logDirectory,'aeri_proc_amun_short_logfile'],'a');

  % Write data to variable.
  netcdf.putVar(ncid,varid,real(Lsky)) ;
  netcdf.putVar(ncid,varid2,nuchop) ;

fprintf(fid1, 'Got here - After writing wnum and mean_rad\n')
fclose(fid1)
fid1 = fopen([logDirectory,'aeri_proc_amun_short_logfile'],'a');

  % Close it
  netcdf.close(ncid);
    
    

  % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %
  
  
  fprintf(fid1,'\n');
end          % of loop over ifile (day)

fprintf(fid1, 'Got here - END !\n')
fclose(fid1)
