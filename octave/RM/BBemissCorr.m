function rad = BBemissCorr_nuDependent(nu, rad, BBcavityFactor, ...
    calibrationHBBtemp, calibrationCBBtemp, calibrationAmbientTemp)
%
% function rad = BBemissCorr(nu, rad, BBcavityFactor, ...
%    calibrationHBBtemp, calibrationCBBtemp, calibrationAmbientTemp)
%
%
%     This script adjusts the calibration on Von Walden's P-AERI to account
%     for the improvement in the BB emissivity model.
%
%     From Dave D. Turner as adjust_von_calibration.pro
%
%     Modified for matlab by Penny M. Rowe
%
% Function Calls:
%   get_aeri_bb_emis.m
%   aeri_recal.m
%
% % % % % % % % % % % % % % % % % % % % % % % % % % % % % %
disp('Running BBemissCorr.  Please check line 47 to make sur that it is doing what you want!!!!')

msg = '    Correction to BB emissivity model.';
fprintf([ msg ' \n']);


[dim1,dim2] = size(rad) ;
dimw        = length(nu) ;

if dim2~=dimw
    if dim1~=dimw
        error('wavenumber and radiance vectors do not agree');
    elseif dim1==dimw
        % radiance is wnum x time, flip to time x wnum
        rad = rad';
    end
end
[dimt,dimw] = size(rad) ;


% quality control
if  dimt < 0 || dimw < 0
    fprintf('Either no times or no frequencies! \n');
end

%[emis,fov_emis,bbemisnew] = get_aeri_bb_emis(nu,9,[]) ;
[emis,fov_emis,bbemisnew] = get_aeri_bb_emis(nu,0,BBcavityFactor) ;

radBBemis = zeros(size(rad));  % dimensions: hour x nu
for i = 1: length(calibrationHBBtemp)
    [emis,fov_emis,bbemisold] = get_aeri_bb_emis(nu,...
        0,BBcavityFactor(i)) ;
    
    radBBemis(i,:) = aeri_recal(nu,rad(i,:)',...
        calibrationHBBtemp(i), ...
        calibrationCBBtemp(i),...
        calibrationAmbientTemp(i),...
        bbemisold,bbemisnew,bbemisold,bbemisnew) ;
end

rad = radBBemis ;

% % % % % % % % % % % % % % % % % % % % % % % % % % % % % %



% $Id: BBemissCorr.m,v 1.1 2012/10/06 20:03:52 dataman Exp $
%+
% Abstract:
%	This routine recalibrates AERI a spectrum based upon the temperature
%   and emissivities of the blackbodies.  It can also be used to estimate
%   the uncertainty in the AERI radiance based upon uncertainties in the
%   measured BB temperatures/emissivities.  Note that it is designed to only
%   process one radiance spectrum at a time.
%
%	Note that a Radiance unit (RU) is mW / (m2 ster cm-1)
%
% Author:
%	Dave Turner
%		Pacific Northwest National Laboratory
%		University of Wisconsin - Madison
%
% Date:
%	October 2002
%
% Original code from Dave Tobin, UW-Madison
% See notebook SSEC/CIMSS pg 55, or email from BobK dated June 19, 2002
%
% Modified by Penny Rowe 
%   4 Dec. 2007
%
% Method:
%
%   rad1 = Re{(Cs-Ca)/(Ch-Ca)}*(B(Th1,Tr1,eh1)-B(Ta1,Tr1,ea1))+B(Ta1,Tr1,ea1)
%   rad2 = Re{(Cs-Ca)/(Ch-Ca)}*(B(Th2,Tr2,eh2)-B(Ta2,Tr2,ea2))+B(Ta2,Tr2,ea2)
%        = ((rad1-B(Ta1,Tr1,ea1)))/(B(Th1,Tr1,ea1)-B(Ta1,Tr1,ea1))*
%          (B(Th2,Tr2,eh2)-B(Ta2,Tr2,ea2)) + B(Ta2,Tr2,ea2)
%    with B(T,Tr,e) = e*planck(T) + (1-e)*planck(Tr)
%
%
% Call:
  function rad2 = aeri_recal(wn,  rad1,  th,  ta, tr, ...
      eh1, eh2, ea1, ea2)
%-
%   function aeri_recal, $	% Newly calibrated radiance spectrum [RU]
%   		wn, $		% Wavenumber array [cm-1]
% 		rad1, $		% Orig radiance spectrum [RU]
% 		th, $		% Orig temperature of hot BB [K]
% 		th2, $		% New temperature of hot BB [K]
% 		ta, $		% Orig temperature of ambient BB [K]
% 		ta2, $		% New temperature of ambient BB [K]
% 		tr, $		% Orig reflected temperature [K]
% 		tr2, $		% New reflected temperature [K]
% 		eh1, $		% Orig emis spectrum for hot BB []
% 		eh2, $		% New emis spectrum for hot BB []
% 		ea1, $		% Orig emis spectrum for ambient BB []
% 		ea2		% New emis spectrum for ambient BB []
% %-

% Start with simple QC
n = length(wn) ;
if (n ~= length(rad1) || n~= length(eh1) || n~=length(eh2) || ...
      n~=length(ea1) || n~=length(ea2) )
  msg=['the wavenumber, radiance, and emissivity arrays ' ...
      'must be same length'];
  error(msg);
end

% Compute the radiances for the various temperatures
% I'm not sure about the original code here ...
b_th = 1e3*plancknu(wn,th); 
b_ta = 1e3*plancknu(wn,ta);
b_tr = 1e3*plancknu(wn,tr);

% Compute pseudo radiances
Bh1 = eh1 .* b_th + (1-eh1) .* b_tr ;
Bh2 = eh2 .* b_th + (1-eh2) .* b_tr ;

Ba1 = ea1 .* b_ta + (1-ea1) .* b_tr ;
Ba2 = ea2 .* b_ta + (1-ea2) .* b_tr ;

% and recalibrate
rad2 = ((rad1 - Ba1) ./ (Bh1 - Ba1)) .* (Bh2 - Ba2) + Ba2 ;




