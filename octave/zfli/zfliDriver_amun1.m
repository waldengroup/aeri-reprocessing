function zfliDriver_amun1(DirRM,DirOut,begDate,endDate,chNo,ext,WNLASR)
% PC done
% 20061110 at 9:53 am (zfli at 11:32 am)
% 20061213 at 9:58 am (zfli at 11:50 am)
% 20061223 at 10:00 am (zfli at 11:54 am)

% Purpose: perform the zero-fill interpolation on PAERI data after QCing,
%   performing the PCA, and performing the responsivity method.
%
% Penny M. Rowe
% Oct. 1, 2009
% 
% Updated 12/29/2010 for use with Summit data - ccox
%

pkg load netcdf
import_netcdf

% Variables describing the PAERI spectra naming convention
locstr  = 'smtaerich';   % String for the location and channel
%chNo    = 2 ;            % Channel number
%ext     = 'rm_pc';    % Extension for file type (qc, pca, and rm done)
datelen = 15 ;          % length of date string => date as YYYYMMDD.hhmmss
% begDate = 20090309;
% endDate = 20090601;

% ... Parameters for zli_mat
% if chNo==1
%   %WNLASR= 15799.43;   % originalLaserWavenumber
%   %WNLASR= 15799.05;   % originalLaserWavenumber
%   WNLASO= 15799.0 ;   % outputLaserWavenumber
%   jexp = 17 ;         % FFTpowerOf2
% elseif chNo==2
%   %WNLASR= 15799.28;   % originalLaserWavenumber
%   %WNLASR= 15798.73;   % originalLaserWavenumber
%   WNLASO= 15799.0 ;   % outputLaserWavenumber
%   jexp = 17 ;         % FFTpowerOf2
% end

WNLASO= 15799.0 ;   % outputLaserWavenumber
jexp = 17 ;         % FFTpowerOf2



% ... Begin

cd(DirRM)
dirnames = dir('smt*.cdf') ;

% ... Get a list of all the PAERI spectra that will be used.
longstr   = [locstr num2str(chNo) '.' ext ];
llen      = length(longstr);
ibads     = zeros(1,5);
filenames = {dirnames.name}; clear dirnames;
count     = 0;
for ifile=1:length(filenames)
    fname = char(filenames(ifile)) ;
    if length(fname)>10 && strcmpi(fname(1:llen),longstr);
        if strcmp(ext,'rm_pc')
            YYYYMMDD = fname(length(locstr)+2+length(ext)+2:length(locstr)+2+length(ext)+9);
        elseif strcmp(ext,'rm_qc')
            YYYYMMDD = fname(length(locstr)+2+length(ext)+2:length(locstr)+2+length(ext)+9);
        end
        if str2num(YYYYMMDD)<begDate || str2num(YYYYMMDD)>endDate
            count=count+1;
            ibads(count)=ifile;
        end
    else
        count=count+1;
        ibads(count)=ifile;
    end
end
filenames(ibads(ibads>0))=[] ;


% ... Loop over all PAERI files (having QC, PCA, and RM performed on them)
tic
sprintf('Putting files on final wavenumber grid.\n')
for ifile=1:length(filenames)
    
    % ... Set up filenames
    fname = char(filenames(ifile));
    datestr = fname(llen+2:llen+1+datelen) ;
    if strcmp(ext,'rm_qc')
        fileout = [locstr num2str(chNo) 'X1.b1.' datestr '.cdf'];
    elseif strcmp(ext,'rm_pc')
        fileout = [locstr num2str(chNo) 'nf1turnX1.c1.' datestr '.cdf'];
    end
    
    disp(fname);
    
    % ... Copy the input cdf file to the output cdf file
    copyfile([DirRM fname],[DirOut fileout]);
    
    % ... Load the input file
    cd(DirRM)
    rm = rd_netcdf(fname);
    
    % ... Get the spectra that were kept during the QC (others are -9999)
    ikeep = find(rm.missingDataFlag==0);
    
    % ... Put on final wavenumber grid
    [wn,rad] = zfli_mat(rm.wnum,rm.mean_rad,ikeep,WNLASR,WNLASO,jexp) ;
    
    
    
    % ... Place wnum, rad, laser nu, and interferogram sizes in final output netcdf file
    
    % ... Open netcdf file
    cd(DirOut);
    ncid = netcdf.open(fileout,'NC_WRITE');
    
    Time = netcdf.getVar(ncid,netcdf.inqVarID(ncid,'Time'));
          
    dimid = netcdf.inqDimID(ncid,'time');
    
%   netcdf.reDef(ncid) ;
%   varid1 = netcdf.defVar(ncid,'expandedInterferogramSize','NC_FLOAT',dimid) ;
%   varid2 = netcdf.defVar(ncid,'originalInterferogramSize','NC_FLOAT',dimid) ;
%   varid3 = netcdf.defVar(ncid,'outputLaserWavenumber','NC_FLOAT',dimid) ;
%   varid4 = netcdf.defVar(ncid,'originalLaserWavenumber','NC_FLOAT',dimid) ;
% 
%   netcdf.endDef(ncid);
%   netcdf.putVar(ncid,varid1,2^jexp * ones(size(Time)));
%   netcdf.putVar(ncid,varid2,8192*ones(size(Time)));
%   netcdf.putVar(ncid,varid3, WNLASO*ones(size(Time))); % ccox 12/15/11 added missing varid3
%   netcdf.putVar(ncid,varid4, WNLASR*ones(size(Time)));
    
    % ... Replace wnum and mean_rad with wn and rad
    netcdf.putVar(ncid,netcdf.inqVarID(ncid,'wnum'),wn);
    %netcdf.putVar(ncid,netcdf.inqVarID(ncid,'wnum1'),wn);
    netcdf.putVar(ncid,netcdf.inqVarID(ncid,'mean_rad'),rad);
    
    % Global Atts
    attPut('originalLaserWavenumber',num2str(WNLASR,'%1.2f'),ncid)
    attPut('originalLaserWavenumber_description','Original laser wavenumber assumed for this instrument',ncid)
    attPut('outputLaserWavenumber',num2str(WNLASO,'%1.2f'),ncid)
    attPut('outputLaserWavenumber_description','Laser wavenumber used in definition of output wavenumber scale',ncid)
    attPut('originalInterferogramSize',num2str(8192,'%1.1f'),ncid)
    attPut('originalInterferogramSize_description','Size of buffer holding initial spectrum',ncid)  
    attPut('expandedInterferogramSize',num2str(2^jexp,'%1.1f'),ncid)
    attPut('expandedInterferogramSize_description','Size of buffer holding expanded spectrum before interpolation',ncid)
    
    if strcmp(ext,'rm_pc')
        RPC = 'Antonelli et al. (2004) J. Geophys. Res., 109, D23102, doi:10.1029/2004JD00482 -and- Turner et al. (2006) J. Atmos. Ocean. Tech., 23, 1223-1238, doi:10.1175/JTECH1906.1.';
        attPut('reference_datastream',RPC,ncid)
        Desc = 'This dataset contains calibrated and quality-controlled Atmospheric Emitted Radiance Interferometer (AERI) infrared spectral radiances [mW(m^2 sr cm^-1)^-1]. These spectra have also been noise-filtered using a Principal Components (PC) technique.  See the reference_datastream global attribute for references. Environment and instrument related metadata are also included.'; 
        attPut('description',Desc,ncid)   
    elseif strcmp(ext,'rm_qc')
        Desc = 'This dataset contains calibrated and quality-controlled Atmospheric Emitted Radiance Interferometer (AERI) infrared spectral radiances [mW(m^2 sr cm^-1)^-1]. Environment and instrument related metadata are also included.'; 
        attPut('description',Desc,ncid)   
    end    
    % ... Done!
    netcdf.close(ncid);
end
toc



% -------------------------------------------------------------------------
function attPut(thename,Att,ncid)
pkg load netcdf
import_netcdf
netcdf.reDef(ncid);
varid = netcdf.getConstant('NC_GLOBAL');
netcdf.putAtt(ncid,varid,thename,Att);
netcdf.endDef(ncid);
