function [wnout,Rout] = zfli_mat(xin,Rin,ikeep,WNLASR,WNLASO,jexp)
%
% ... Purpose: to test the zfli code written by PMR in matlab
%    by running the code on rfc radiances and comparing the result
%    to the zfli radiances output as part of the AERI processing



% parameters
wing = 30 ;
sigma = 10 ;




% Go ...
npts  = length(xin);

WNAc  = xin(1) ;               % line 345: as in the code, but wrong here.
WNBc  = xin(npts) ;            % line 346
dwnc  = (WNBc-WNAc)/(npts-1) ;    % line 347

% Note: the next three lines are not in the code:
% Note: I can't find
WNA  = xin(1)*WNLASR/16384/2 / dwnc ; %xin(1)*(1+eps) ;    % 491.80356 ; %
WNB  = xin(npts)*WNLASR/16384/2 / dwnc ; %xin(npts)*(1+eps) ;    % 1799.90454; %

% Go on
dwn  = (WNB-WNA)/(npts-1) ;   % line 347
nporg = npts;                 % line 348

npiw   = round(wing/dwn) ;   % nint?
nshift = npiw;

dwni = dwn ;


% ... DEFINE WAVENUMBER FOR FIRST POINT IN SHIFTED SPECTRUM *
wnash = WNA- nshift * dwni ;   % line 459
%wnashc = (WNAc- nshift * dwnc) ;   % line 459

ntotl = 2*(nshift+nporg+npiw) ;
iexp   = round(0.9999+log(ntotl)/log(2)) ;
newtot = 2^jexp;

ntotl   = 2^iexp ;
nyqno   = 1+ntotl/2;
newnyq  = 1+newtot/2;
dwnexp  = dwni / 2^(jexp-iexp) ;
%dwnexpc = dwnc / 2^(jexp-iexp) ;


% ... DEFINE WAVENUMBER ARRAY FOR INTERPOLATION *
wn = zeros(1,newnyq) ;
%wnc = zeros(1,newnyq) ;
for I=1:newnyq
    wn(I)  = wnash  + (I-1)*dwnexp ;
    %wnc(I) = wnashc + (I-1)*dwnexpc  ;
end

% The following lines are mine (PMR)
dwno  = WNLASO/16384/2 ;
wnout = xin/dwnc*dwno ;

KPTS = nporg + nshift + npiw ;

% Loop over the spectra
Rout = Rin ;
for it=ikeep(:)'
    
    radin = Rin(:,it) ;
    rad   = zeros(size(radin));
    
    
    % shift the spectrum               % line 707
    rad(1+nshift:nporg+nshift)=radin(1:nporg) ;
    
    
    % ... APPLY GAUSSIAN ROLL-OFF ...  % line 710
    J    = KPTS ;
    K    = nshift +1 ;
    RADK = rad(K) ;
    RADL = rad(KPTS-npiw) ;
    
    
    for I = 1:npiw
        WNI    = wnash + (I-1)*dwni ;
        GDFVAL = exp(-(WNI-WNA)^2/(2*sigma^2)) ;
        
        rad(I) = RADK*GDFVAL ;
        rad(J) = RADL*GDFVAL ;
        J = J-1 ;
    end
    
    C = rad(1:KPTS) ;
    
    
    % START INVERSE FFT
    R = real(ifft(C,2^iexp));
    
    % Get first half
    R = R(1:length(R)/2+1);
    
    
    %MAXRL = 2*length(rfc.Time);
    % * INTERFEROGRAM IS IN ARRAY "R", R(1) = POINT OF SYMMETRY (ZPD)
    % * SET INTERFEROGRAM TO ZERO BEYOND NYQUIST NUMBER *
    %      WRITE(*,*)'ZEROING BEYOND NYQUIST NUMBER'
    %for I=nyqno+1:MAXRL
    %	R(I)=0 ;
    %end
    
    % ... NOW CONSTRUCT "NEGATIVE" PORTION
    IS = newtot ;
    for I = 2:nyqno-1
        R(IS) = R(I) ;
        IS = IS-1 ;
    end
    
    %      WRITE(*,'('' START FORWARD FFT, REC.NO. '',I5)')NR
    % *** COMPUTE SPECTRUM (REAL, FORWARD FFT)
    %      CALL RFFTH(R,JEXP)
    rout = 2*real(fft(R,2^jexp)) ;
    rout = rout(1:length(rout)/2+1);
    
    %radc = interp1(wnc,rout,wnout);
    Rout(:,it)  = interp1(wn, rout,wnout);
    
end
