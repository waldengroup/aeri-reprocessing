% $Id: cdfNewSingleDataType.m,v 1.2 2012/12/20 17:09:10 dataman Exp $
function cdfNewSingleDataType(inFile)
% C Cox
% 8/31/2012
%

pkg load netcdf 
import_netcdf

% Copies nc from RM directory to final_RMcalibHere, chaning the back to  
file = rd_netcdf(inFile);
atts = attribute_list(inFile);
%unix(['mv ',inFile,' ',inFile,'.orig']);

% create the new one...
ncid = netcdf.create(inFile,'NC_SHARE');

TimeID  = netcdf.defDim(ncid,'time',length(file.time));
WnumID  = netcdf.defDim(ncid,'wnum',length(file.wnum));
WnumID2 = netcdf.defDim(ncid,'wnum2',length(file.wnum2));
netcdf.endDef(ncid)

try cdfPut(file.date,'date','int',[],[],ncid);catch;wrterr(inFile,'date');end;     
try cdfPut(file.base_time,'base_time','int',atts.base_time,[],ncid);catch;wrterr(inFile,'base_time');end;    
try cdfPut(file.wnum,'wnum','float',atts.wnum,WnumID,ncid);catch;wrterr(inFile,'wnum');end;   
try cdfPut(file.time_offset,'time_offset','double',atts.time_offset,TimeID,ncid);catch;wrterr(inFile,'time_offset');end;   
try cdfPut(file.missingDataFlag,'missingDataFlag','float',atts.missingDataFlag,TimeID,ncid);catch;wrterr(inFile,'missingDataFlag');end; 
try cdfPut(file.sceneMirPosEncoderMaxDrift,'sceneMirPosEncoderMaxDrift','float',atts.sceneMirPosEncoderMaxDrift,TimeID,ncid);catch;wrterr(inFile,'sceneMirPosEncoderMaxDrift');end; 
try cdfPut(file.BBcavityFactor,'BBcavityFactor','float',atts.BBcavityFactor,TimeID,ncid);catch;wrterr(inFile,'BBcavityFactor');end; 
try cdfPut(file.HBBtempOffset,'HBBtempOffset','float',atts.HBBtempOffset,TimeID,ncid);catch;wrterr(inFile,'HBBtempOffset');end; 
try cdfPut(file.ABBtempOffset,'ABBtempOffset','float',atts.ABBtempOffset,TimeID,ncid);catch;wrterr(inFile,'ABBtempOffset');end; 
try cdfPut(file.HBBbottomTempWeight,'HBBbottomTempWeight','float',atts.HBBbottomTempWeight,TimeID,ncid);catch;wrterr(inFile,'HBBbottomTempWeight');end; 
try cdfPut(file.HBBapexTempWeight,'HBBapexTempWeight','float',atts.HBBapexTempWeight,TimeID,ncid);catch;wrterr(inFile,'HBBapexTempWeight');end; 
try cdfPut(file.HBBtopTempWeight,'HBBtopTempWeight','float',atts.HBBtopTempWeight,TimeID,ncid);catch;wrterr(inFile,'HBBtopTempWeight');end; 
try cdfPut(file.ABBbottomTempWeight,'ABBbottomTempWeight','float',atts.ABBbottomTempWeight,TimeID,ncid);catch;wrterr(inFile,'ABBbottomTempWeight');end; 
try cdfPut(file.ABBapexTempWeight,'ABBapexTempWeight','float',atts.ABBapexTempWeight,TimeID,ncid);catch;wrterr(inFile,'ABBapexTempWeight');end; 
try cdfPut(file.ABBtopTempWeight,'ABBtopTempWeight','float',atts.ABBtopTempWeight,TimeID,ncid);catch;wrterr(inFile,'ABBtopTempWeight');end; 
try cdfPut(file.calibratedSceneID,'calibratedSceneID','float',atts.calibratedSceneID,TimeID,ncid);catch;wrterr(inFile,'calibratedSceneID');end; 
try cdfPut(single(file.calibrationHBBtemp),'calibrationHBBtemp','float',atts.calibrationHBBtemp,TimeID,ncid);catch;wrterr(inFile,'calibrationHBBtemp');end; 
try cdfPut(single(file.calibrationCBBtemp),'calibrationCBBtemp','float',atts.calibrationCBBtemp,TimeID,ncid);catch;wrterr(inFile,'calibrationCBBtemp');end; 
try cdfPut(single(file.calibrationAmbientTemp),'calibrationAmbientTemp','float',atts.calibrationAmbientTemp,TimeID,ncid);catch;wrterr(inFile,'calibrationAmbientTemp');end; 
try cdfPut(file.channelNumber,'channelNumber','float',atts.channelNumber,TimeID,ncid);catch;wrterr(inFile,'channelNumber');end; 
try cdfPut(file.sceneMirPosEncoderDrift,'sceneMirPosEncoderDrift','float',atts.sceneMirPosEncoderDrift,TimeID,ncid);catch;wrterr(inFile,'sceneMirPosEncoderDrift');end; 
try cdfPut(file.HBBmaxTempDiff,'HBBmaxTempDiff','float',atts.HBBmaxTempDiff,TimeID,ncid);catch;wrterr(inFile,'HBBmaxTempDiff');end; 
try cdfPut(file.HBBmaxTempDiff,'ABBmaxTempDiff','float',atts.ABBmaxTempDiff,TimeID,ncid);catch;wrterr(inFile,'ABBmaxTempDiff');end; 
try cdfPut(file.maxRoll,'maxRoll','float',atts.maxRoll,TimeID,ncid);catch;wrterr(inFile,'maxRoll');end; 
try cdfPut(file.maxPitch,'maxPitch','float',atts.maxPitch,TimeID,ncid);catch;wrterr(inFile,'maxPitch');end; 
try cdfPut(file.opticsCompartmentRelativeHumidity,'opticsCompartmentRelativeHumidity','float',atts.opticsCompartmentRelativeHumidity,TimeID,ncid);catch;wrterr(inFile,'opticsCompartmentRelativeHumidity');end; 
try cdfPut(file.sceneMirrorMotorStep,'sceneMirrorMotorStep','float',atts.sceneMirrorMotorStep,TimeID,ncid);catch;wrterr(inFile,'sceneMirrorMotorStep');end; 
try cdfPut(file.sceneMirrorAngle,'sceneMirrorAngle','float',atts.sceneMirrorAngle,TimeID,ncid);catch;wrterr(inFile,'sceneMirrorAngle');end; 
try cdfPut(file.maxSampleStdDev,'maxSampleStdDev','float',atts.maxSampleStdDev,TimeID,ncid);catch;wrterr(inFile,'maxSampleStdDev');end; 
try cdfPut(file.atmosphericPressure,'atmosphericPressure','float',atts.atmosphericPressure,TimeID,ncid);catch;wrterr(inFile,'atmosphericPressure');end; 
try cdfPut(file.interferometerEnclosureRelativeHumidity,'interferometerEnclosureRelativeHumidity','float',atts.interferometerEnclosureRelativeHumidity,TimeID,ncid);catch;wrterr(inFile,'interferometerEnclosureRelativeHumidity');end; 
try cdfPut(file.atmosphericRelativeHumidity,'atmosphericRelativeHumidity','float',atts.atmosphericRelativeHumidity,TimeID,ncid);catch;wrterr(inFile,'atmosphericRelativeHumidity');end; 
try cdfPut(file.interferometerWindowTemp,'interferometerWindowTemp','float',atts.interferometerWindowTemp,TimeID,ncid);catch;wrterr(inFile,'interferometerWindowTemp');end; 
try cdfPut(file.rainSensorIntensity,'rainSensorIntensity','float',atts.rainSensorIntensity,TimeID,ncid);catch;wrterr(inFile,'rainSensorIntensity');end; 
try cdfPut(file.detectorTemp,'detectorTemp','float',atts.detectorTemp,TimeID,ncid);catch;wrterr(inFile,'detectorTemp');end; 
try cdfPut(file.coolerCurrent,'coolerCurrent','float',atts.coolerCurrent,TimeID,ncid);catch;wrterr(inFile,'coolerCurrent');end; 
try cdfPut(file.SCEtemp,'SCEtemp','float',atts.SCEtemp,TimeID,ncid);catch;wrterr(inFile,'SCEtemp');end; 
try cdfPut(file.motorDriverTemp,'motorDriverTemp','float',atts.motorDriverTemp,TimeID,ncid);catch;wrterr(inFile,'motorDriverTemp');end; 
try cdfPut(file.computerTemp,'computerTemp','float',atts.computerTemp,TimeID,ncid);catch;wrterr(inFile,'computerTemp');end; 
try cdfPut(file.rackAmbientTemp,'rackAmbientTemp','float',atts.rackAmbientTemp,TimeID,ncid);catch;wrterr(inFile,'rackAmbientTemp');end; 
try cdfPut(file.coolerPowerSupplyTemp,'coolerPowerSupplyTemp','float',atts.coolerPowerSupplyTemp,TimeID,ncid);catch;wrterr(inFile,'coolerPowerSupplyTemp');end; 
try cdfPut(file.coolerExpanderTemp,'coolerExpanderTemp','float',atts.coolerExpanderTemp,TimeID,ncid);catch;wrterr(inFile,'coolerExpanderTemp');end; 
try cdfPut(file.coolerCompressorTemp,'coolerCompressorTemp','float',atts.coolerCompressorTemp,TimeID,ncid);catch;wrterr(inFile,'coolerCompressorTemp');end; 
try cdfPut(file.BBcontroller2temp,'BBcontroller2temp','float',atts.BBcontroller2temp,TimeID,ncid);catch;wrterr(inFile,'BBcontroller2temp');end; 
try cdfPut(file.BBcontroller1temp,'BBcontroller1temp','float',atts.BBcontroller1temp,TimeID,ncid);catch;wrterr(inFile,'BBcontroller1temp');end; 
try cdfPut(file.fixed12KohmResistor,'fixed12KohmResistor','float',atts.fixed12KohmResistor,TimeID,ncid);catch;wrterr(inFile,'fixed12KohmResistor');end; 
try cdfPut(file.mirrorMotorTemp,'mirrorMotorTemp','float',atts.mirrorMotorTemp,TimeID,ncid);catch;wrterr(inFile,'mirrorMotorTemp');end; 
try cdfPut(file.airNearBBsTemp,'airNearBBsTemp','float',atts.airNearBBsTemp,TimeID,ncid);catch;wrterr(inFile,'airNearBBsTemp');end; 
try cdfPut(file.BBsupportStructureTemp,'BBsupportStructureTemp','float',atts.BBsupportStructureTemp,TimeID,ncid);catch;wrterr(inFile,'BBsupportStructureTemp');end; 
try cdfPut(file.interferometerSecondPortTemp,'interferometerSecondPortTemp','float',atts.interferometerSecondPortTemp,TimeID,ncid);catch;wrterr(inFile,'interferometerSecondPortTemp');end; 
try cdfPut(file.airNearInterferometerTemp,'airNearInterferometerTemp','float',atts.airNearInterferometerTemp,TimeID,ncid);catch;wrterr(inFile,'airNearInterferometerTemp');end; 
try cdfPut(file.outsideAirTemp,'outsideAirTemp','float',atts.outsideAirTemp,TimeID,ncid);catch;wrterr(inFile,'outsideAirTemp');end; 
try cdfPut(file.fixed97KohmResistor,'fixed97KohmResistor','float',atts.fixed97KohmResistor,TimeID,ncid);catch;wrterr(inFile,'fixed97KohmResistor');end; 
try cdfPut(file.fixed2500ohmResistor,'fixed2500ohmResistor','float',atts.fixed2500ohmResistor,TimeID,ncid);catch;wrterr(inFile,'fixed2500ohmResistor');end; 
try cdfPut(single(file.HBBbottomTemp),'HBBbottomTemp','float',atts.HBBbottomTemp,TimeID,ncid);catch;wrterr(inFile,'HBBbottomTemp');end; 
try cdfPut(single(file.HBBapexTemp),'HBBapexTemp','float',atts.HBBapexTemp,TimeID,ncid);catch;wrterr(inFile,'HBBapexTemp');end; 
try cdfPut(single(file.HBBtopTemp),'HBBtopTemp','float',atts.HBBtopTemp,TimeID,ncid);catch;wrterr(inFile,'HBBtopTemp');end; 
try cdfPut(single(file.ABBbottomTemp),'ABBbottomTemp','float',atts.ABBbottomTemp,TimeID,ncid);catch;wrterr(inFile,'ABBbottomTemp');end; 
try cdfPut(single(file.ABBapexTemp),'ABBapexTemp','float',atts.ABBapexTemp,TimeID,ncid);catch;wrterr(inFile,'ABBapexTemp');end; 
try cdfPut(single(file.ABBtopTemp),'ABBtopTemp','float',atts.ABBtopTemp,TimeID,ncid);catch;wrterr(inFile,'ABBtopTemp');end; 
try cdfPut(file.JulianDay,'JulianDay','float',atts.JulianDay,TimeID,ncid);catch;wrterr(inFile,'JulianDay');end; 
try cdfPut(file.sceneMirPosEncoder,'sceneMirPosEncoder','float',atts.sceneMirPosEncoder,TimeID,ncid);catch;wrterr(inFile,'sceneMirPosEncoder');end;   
try cdfPut(file.sceneMirPosCount,'sceneMirPosCount','float',atts.sceneMirPosCount,TimeID,ncid);catch;wrterr(inFile,'sceneMirPosCount');end; 
try cdfPut(file.sceneMirrorPosition,'sceneMirrorPosition','float',atts.sceneMirrorPosition,TimeID,ncid);catch;wrterr(inFile,'sceneMirrorPosition');end; 
try cdfPut(file.coadditionsCount,'coadditionsCount','float',atts.coadditionsCount,TimeID,ncid);catch;wrterr(inFile,'coadditionsCount');end; 
try cdfPut(file.sceneViewDuration,'sceneViewDuration','float',atts.sceneViewDuration,TimeID,ncid);catch;wrterr(inFile,'sceneViewDuration');end; 
try cdfPut(file.systemReleaseNumber,'systemReleaseNumber','float',atts.systemReleaseNumber,TimeID,ncid);catch;wrterr(inFile,'systemReleaseNumber');end; 
try cdfPut(file.Altitude,'Altitude','float',atts.Altitude,TimeID,ncid);catch;wrterr(inFile,'Altitude');end; 
try cdfPut(file.Longitude,'Longitude','float',atts.Longitude,TimeID,ncid);catch;wrterr(inFile,'Longitude');end; 
try cdfPut(file.Latitude,'Latitude','float',atts.Latitude,TimeID,ncid);catch;wrterr(inFile,'Latitude');end; 
try cdfPut(file.timeHHMMSS,'timeHHMMSS','float',atts.timeHHMMSS,TimeID,ncid);catch;wrterr(inFile,'timeHHMMSS');end; 
try cdfPut(file.dateYYMMDD,'dateYYMMDD','float',atts.dateYYMMDD,TimeID,ncid);catch;wrterr(inFile,'dateYYMMDD');end; 
try cdfPut(file.instrumentUnitNumber,'instrumentUnitNumber','float',atts.instrumentUnitNumber,TimeID,ncid);catch;wrterr(inFile,'instrumentUnitNumber');end; 
try cdfPut(file.AERIunitNumber,'AERIunitNumber','float',atts.AERIunitNumber,TimeID,ncid);catch;wrterr(inFile,'AERIunitNumber');end; 
try cdfPut(file.Time,'Time','float',atts.Time,TimeID,ncid);catch;wrterr(inFile,'Time');end; 
try cdfPut(single(file.mean_rad'),'mean_rad','float',atts.mean_rad,[WnumID TimeID],ncid);catch;wrterr(inFile,'mean_rad');end; 
try cdfPut(single(file.hatchOpen),'hatchOpen','float',atts.hatchOpen,TimeID,ncid);catch;wrterr(inFile,'hatchOpen');end; 
try cdfPut(file.time,'time','double',atts.time,TimeID,ncid);catch;wrterr(inFile,'time');end
try cdfPut(single(file.qc_time),'qc_time','float',atts.qc_time,TimeID,ncid);catch;wrterr(inFile,'qc_time');end;
try cdfPut(single(file.Time_UTC_hours),'Time_UTC_hours','float',atts.Time_UTC_hours,TimeID,ncid);catch;wrterr(inFile,'Time_UTC_hours');end; 
try cdfPut(file.lat,'lat','float',atts.lat,[],ncid);catch;wrterr(inFile,'lat');end;
try cdfPut(file.lon,'lon','float',atts.lon,[],ncid);catch;wrterr(inFile,'lon');end;
try cdfPut(file.alt,'alt','float',atts.alt,[],ncid);catch;wrterr(inFile,'alt');end; 
try cdfPut(file.wnum2,'wnum2','float',atts.wnum2,WnumID2,ncid);catch;wrterr(inFile,'wnum2');end; 
try cdfPut(file.standard_dev_mean_rad,'standard_dev_mean_rad','float',atts.standard_dev_mean_rad,[WnumID2 TimeID],ncid);catch;wrterr(inFile,'standard_dev_mean_rad');end; 
try cdfPut(file.numberOfPCs,'numberOfPCs','float',atts.numberOfPCs,[],ncid);catch;wrterr(inFile,'numberOfPCs');end; 
%try cdfPut(file.expandedInterferogramSize,'expandedInterferogramSize','float',[],TimeID,ncid);catch;wrterr(inFile,'expandedInterferogramSize');end; 
%try cdfPut(file.originalInterferogramSize,'originalInterferogramSize','float',[],TimeID,ncid);catch;wrterr(inFile,'originalInterferogramSize');end; 
%try cdfPut(file.outputLaserWavenumber,'outputLaserWavenumber','float',[],TimeID,ncid);catch;wrterr(inFile,'outputLaserWavenumber');end; 
%try cdfPut(file.originalLaserWavenumber,'originalLaserWavenumber','float',[],TimeID,ncid);catch;wrterr(inFile,'originalLaserWavenumber');end

% add globals...
attPut('Comments',atts.globals.Comments,ncid)
attPut('FileHistory',atts.globals.FileHistory,ncid)
attPut('experiment',atts.globals.experiment,ncid)
attPut('contact',atts.globals.contact,ncid)  
attPut('reference_project',atts.globals.reference_project,ncid)
attPut('date_created',atts.globals.date_created,ncid)
attPut('processing_version',atts.globals.processing_version,ncid)
attPut('missing_data_flag',atts.globals.missing_data_flag,ncid)
attPut('site_id',atts.globals.site_id,ncid)
attPut('facility_id',atts.globals.facility_id,ncid)
attPut('FFOVhalfAngle',atts.globals.FFOVhalfAngle,ncid)
attPut('FFOVhalfAngle_description',atts.globals.FFOVhalfAngle_description,ncid)
attPut('originalLaserWavenumber',atts.globals.originalLaserWavenumber,ncid)
attPut('originalLaserWavenumber_description',atts.globals.originalLaserWavenumber_description,ncid)
attPut('outputLaserWavenumber',atts.globals.outputLaserWavenumber,ncid)
attPut('outputLaserWavenumber_description',atts.globals.outputLaserWavenumber_description,ncid)
attPut('originalInterferogramSize',atts.globals.originalInterferogramSize,ncid)
attPut('originalInterferogramSize_description',atts.globals.originalInterferogramSize_description,ncid)  
attPut('expandedInterferogramSize',atts.globals.expandedInterferogramSize,ncid)
attPut('expandedInterferogramSize_description',atts.globals.expandedInterferogramSize_description,ncid)
attPut('description',atts.globals.description,ncid)
try attPut('notes',atts.globals.notes,ncid); catch end  
try attPut('reference_datastream',atts.globals.reference_datastream,ncid); catch end  

netcdf.close(ncid)

function cdfPut(Var,thename,prec,Att,dimid,ncid)
pkg load netcdf
import_netcdf
netcdf.reDef(ncid);
varid = netcdf.defVar(ncid,thename,prec,dimid);
netcdf.endDef(ncid);
netcdf.putVar(ncid,varid,Var');
netcdf.reDef(ncid);
if ~isempty(Att);
   % Attv = struct2cell(Att);
    Attn = fieldnames(Att);
    for h = 1:length(Attn)
        % netcdf.putAtt(ncid,varid,char(Attn(h)),char(Attv(h)));
        netcdf.putAtt(ncid,varid,char(Attn(h)),eval(['Att.',char(Attn(h))]));
    end
end
netcdf.endDef(ncid);


% -------------------------------------------------------------------------
function attPut(thename,Att,ncid)
pkg load netcdf
import_netcdf
netcdf.reDef(ncid);
varid = netcdf.getConstant('NC_GLOBAL');
netcdf.putAtt(ncid,varid,thename,Att);
netcdf.endDef(ncid);



% -------------------------------------------------------------------------
function wrterr(inFile,v)
fid1 = fopen('missing_smped_logfile','a');
fprintf(fid1,[inFile ' doesnt have ',v,' \n']);
fclose(fid1);
